<?php

  /*---------------------------------------------------------------
   * 
   * 	MODULE:		ahiftDetail.php
   * 	AUTHOR:		Gian
   * 	Created:	Unknown
   * 
   * --------------------------------------------------------------
   * 
   * 	
   * 	MODIFICATION HISTORY
   * 	20170521 PRSC	Cleanup paths, code and PHP tabs fixed.
   *    * 
   *---------------------------------------------------------------
   */


    $basepath = "C:\inetpub\wwwroot\VolunteerCalendar";

	require ($basepath . '\init.php');
	require ($basepath . '\includes\adminFunctions.php');
    require ($basepath . '\includes\functions.php');


	$shiftID = $_GET['shiftid'];
	$action = $_GET['action'];
	$postedShiftDate = $_GET['shiftDate'];

	if (empty($action)) $action = "VIEW";

	//print "ACTION ["  . $action . "]<br>";
	
	
	switch($action) {
	case "ADDNEW":
		$pageTitle = "Add New Shift";
		$shiftDate = date("m/d/Y");
		break;
		
	case "EDIT";
		$pageTitle = "Edit Shift";
		$shiftValues = loadShiftData($shiftID);

		//print_r($shiftValues);

		$shiftUserID = $shiftValues[0];
		$shiftDate = date("m/d/Y", strtotime($shiftValues[1]));
		$shiftStart = date("g:i a", strtotime($shiftValues[1]));
		$shiftEnd = date("g:i a", strtotime($shiftValues[2]));
		$shiftComment = str_replace("'", "", strip_tags($shiftValues[3]));
		$shiftCat = $shiftValues[4];
		$shiftRepeatInterval = $shiftValues[5];
		$shiftRepeatYear = $shiftValues[6];
		$shiftRepeatMonth = $shiftValues[7];
		$shiftRepeatDay = $shiftValues[8];
		$shiftRepeatWeek = $shiftValues[9];
		$shiftRepeatWeekday = $shiftValues[10];
		if ($shiftValues[11] != null) {
            $shiftEndDate = date("m/d/Y", strtotime($shiftValues[11]));
        }
        $shiftRepeats = $shiftValues[12];




		break;
	case "VIEW":
		$pageTitle = "View Shift";
	}

	/*...............Theoretically update the APPSHIFT table with the details........................PRSC */
	
	if (array_key_exists('fShiftDate', $_POST) || $action == "DELETE") {
		//form has been posted
		updateShift($action, getShiftFormData(), $shiftID);
		echo "<script>window.opener.location.reload(true);window.close();</script>";
	}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>Shift Detail</title>

	<link rel="stylesheet" href="css/default.css" media="screen,projection" type="text/css" />
	<link rel="stylesheet" href="css/dhtmlgoodies_calendar.css" media="screen,projection" type="text/css" />

    <script language="JavaScript" type="text/javascript" src="js/jquery-1.12.4.min.js"></script>
    <script language="JavaScript" type="text/javascript" src="js/jquery-ui.min.js"></script>
	<script language="JavaScript" type="text/javascript" src="js/main.js"></script>
	<script type="text/javascript" src="js/dhtmlgoodies_calendar.js"></script>
</head>

<body id="entry">

<h1><?= $pageTitle ?></h1>


	<?php 
	
	if ($action == "ADDNEW") { ?>
		<form action="shiftDetail.php?shiftid=<?= $shiftID ?>&action=<?= $action ?>" name="shiftForm" method="post" onsubmit="return validateVolunteerForm(this)">
            <table id="detailTable" cellpadding="0" cellspacing="0" width="98%">
                <tr>
                    <td class="label">Volunteer</td>
                    <td><?php drawUserSelect($currentUserID, "fVolunteer", $currentUserID) ?></td>
                </tr>
                <tr>
                    <td class="label">Start Date</td>
                    <td>
                        <input class="datesel" type="text" id="fShiftDate" name="fShiftDate" readonly value="<?= $shiftDate ?>" onchange="fRepeatEveryChanged()">
                        <img src="images/icon_cal.gif" border="0" title="Calendar"
                             onclick="displayCalendar(document.forms[0].fShiftDate, 'mm/dd/yyyy', this);"
                             onmouseover="this.style.cursor='pointer';"/>
                        <img src="images/refresh.png" border="0" title="Clear Date"
                             onclick="document.forms[0].fShiftDate.value = '';"
                             onmouseover="this.style.cursor='pointer';"/>
                    </td>
                </tr>
                <tr>
                    <td class="label">Start Time</td>
                    <td><?php drawTimeSelect("fStartTime", $shiftStart);
                        echo "Note: <strong>12:00 AM = Midnight | 12:00 PM = Noon</strong>"; ?></td>
                </tr>
                <tr>
                    <td class="label">End Time</td>
                    <td><?php drawTimeSelect("fEndTime", $shiftEnd); ?></td>
                </tr>
                <tr>
                    <td class="label">Repeats</td>
                    <td><input type="checkbox" id="fShiftRepeats" name="fShiftRepeats" onchange="fRepeatsChanged()"/>
                    </td>
                </tr>
                <tr id="trRepeatInterval" style="visibility: collapse">
                    <td class="label">Repeat Every</td>
                    <td><select name="fRepeatEvery" id="fRepeatEvery" onchange="fRepeatEveryChanged()">
                            <option value="Day">Day</option>
                            <option value="Week">Week</option>
                            <option value="Month">Month</option>
                            <option value="Year">Year</option>
                        </select></td>
                </tr>
                <tr id="trRepeatWeekly" style="visibility: collapse">
                    <td class="label">Repeat On</td>
                    <td>
                        <select id="fRepeatOn" name="fRepeatOn">
                            <option value="7">Sunday</option>
                            <option value="1">Monday</option>
                            <option value="2">Tuesday</option>
                            <option value="3">Wednesday</option>
                            <option value="4">Thursday</option>
                            <option value="5">Friday</option>
                            <option value="6">Saturday</option>
                        </select>
                    </td>
                </tr>
                <tr id="trRepeatMonthly" style="visibility: collapse">
                    <td class="label">Repeat</td>
                    <td>
                        <select id="fRepeatMonthly" name="fRepeatMonthly">
                        </select>
                    </td>
                </tr>
                <tr id="trEndDate" style="visibility: collapse">
                    <td class="label">End Date</td>
                    <td>
                        <input class="datesel" type="text" id="fShiftEndDate" name="fShiftEndDate" readonly
                               value="<?= $shiftEndDate ?>">
                        <img src="images/icon_cal.gif" border="0" title="Calendar"
                             onclick="displayCalendar(document.forms[0].fShiftEndDate, 'mm/dd/yyyy', this);"
                             onmouseover="this.style.cursor='pointer';"/>
                        <img src="images/refresh.png" border="0" title="Clear Date"
                             onclick="document.forms[0].fShiftEndDate.value = '';"
                             onmouseover="this.style.cursor='pointer';"/>
                    </td>
                </tr>
                <tr>
                    <td class="label">Section</td>
                    <td><?php printCategories("SELECT", $shiftCat, $currentUserID, false); ?></td>
                </tr>
                <tr>
                    <td class="label" valign="top">Comments</td>
                    <td valign="top"><textarea name="fComments"><?= $shiftComment ?></textarea></td>
                </tr>
            </table>

		<div id="buttonbar">
			<input type="submit" value="Save" />
			<input type="button" onclick="window.close();" value="Cancel" />
		</div>

		<input type="hidden" name="fShiftID" value="<?= $shiftID ?>" />
        <input type="hidden" id="hdnShiftCat" name="hdnRepeats" value="<?= $shiftCat ?>" />
	<?php } elseif ($action == "EDIT") { ?>
		<form action="shiftDetail.php?shiftid=<?= $shiftID ?>&action=<?= $action ?>" name="shiftForm" method="post" onsubmit="return validateVolunteerForm(this)">
		<table id="detailTable" cellpadding="0" cellspacing="0" width="98%">
		<tr>
			<td class="label">Volunteer</td>
			<td><?php drawUserSelect($currentUserID, "fVolunteer", $shiftUserID) ?></td>
		</tr>
		<tr>
			<td class="label">Start Date</td>
			<td>
				<input class="datesel" type="text" id="fShiftDate" name="fShiftDate" readonly value="<?= $shiftDate ?>" onchange="fRepeatEveryChanged()">
				<img src="images/icon_cal.gif" border="0" title="Calendar" onclick="displayCalendar(document.forms[0].fShiftDate, 'mm/dd/yyyy', this);" onmouseover="this.style.cursor='pointer';" />
                <img src="images/refresh.png" border="0" title="Clear Date" onclick="document.forms[0].fShiftDate.value = '';" onmouseover="this.style.cursor='pointer';" />
			</td>
		</tr>
		<tr>
			<td class="label">Start Time</td>
			<td><?php drawTimeSelect("fStartTime", $shiftStart); echo "Note: <strong>12:00 AM = Midnight | 12:00 PM = Noon</strong>"; ?></td>
		</tr>
		<tr>
			<td class="label">End Time</td>
			<td><?php drawTimeSelect("fEndTime", $shiftEnd); ?></td>
		</tr>
        <tr>
            <td class="label">Repeats</td>
            <td><input type="checkbox" id="fShiftRepeats" name="fShiftRepeats" onchange="fRepeatsChanged()" /> </td>
        </tr>
        <tr id="trRepeatInterval" style="visibility: collapse">
            <td class="label">Repeat Every</td>
            <td><select name="fRepeatEvery" id="fRepeatEvery" onchange="fRepeatEveryChanged()">
                    <option value="Day">Day</option>
                    <option value="Week">Week</option>
                    <option value="Month">Month</option>
                    <option value="Year">Year</option>
                </select></td>
        </tr>
        <tr id="trRepeatWeekly" style="visibility: collapse">
            <td class="label">Repeat On</td>
            <td>
                <select id="fRepeatOn" name="fRepeatOn">
                    <option value="7">Sunday</option>
                    <option value="1">Monday</option>
                    <option value="2">Tuesday</option>
                    <option value="3">Wednesday</option>
                    <option value="4">Thursday</option>
                    <option value="5">Friday</option>
                    <option value="6">Saturday</option>
                </select>
            </td>
        </tr>
        <tr id="trRepeatMonthly" style="visibility: collapse">
            <td class="label">Repeat</td>
            <td>
                <select id="fRepeatMonthly" name="fRepeatMonthly">
                </select>
            </td>
        </tr>
        <tr id="trEndDate" style="visibility: collapse">
            <td class="label">End Date</td>
            <td>
                <input class="datesel" type="text" id="fShiftEndDate" name="fShiftEndDate" readonly value="<?= $shiftEndDate ?>">
                <img src="images/icon_cal.gif" border="0" title="Calendar" onclick="displayCalendar(document.forms[0].fShiftEndDate, 'mm/dd/yyyy', this);" onmouseover="this.style.cursor='pointer';" />
                <img src="images/refresh.png" border="0" title="Clear Date" onclick="document.forms[0].fShiftEndDate.value = '';" onmouseover="this.style.cursor='pointer';" />
            </td>
        </tr>
		<tr>
			<td class="label">Section</td>
<!--			<td>--><?php //printCategories("SELECT", $shiftCat, $currentUserID, false); ?><!--</td>-->
            <td><?php printCategories("SELECT", $shiftCat, $shiftUserID, false); ?></td>
		</tr>
		<tr>
			<td class="label" valign="top">Comments</td>
			<td valign="top"><textarea name="fComments"><?= $shiftComment ?></textarea></td>
		</tr>
		</table>

		<div id="buttonbar">
			<input type="submit" value="Save" />
			<input type="button" onclick="window.close();" value="Cancel" />
		</div>

		<input type="hidden" id="fShiftID" name="fShiftID" value="<?= $shiftID ?>" />
            <input type="hidden" id="hdnRepeatInterval" name="hdnRepeatInterval" value="<?= $shiftRepeatInterval ?>" />
            <input type="hidden"id="hdnRepeatYear" name="hdnRepeatYear" value="<?= $shiftRepeatYear ?>" />
            <input type="hidden"id="hdnRepeatMonth" name="hdnRepeatMonth" value="<?= $shiftRepeatMonth ?>" />
            <input type="hidden" id="hdnRepeatDay" name="hdnRepeatDay" value="<?= $shiftRepeatDay ?>" />
            <input type="hidden" id="hdnRepeatWeek" name="hdnRepeatWeek" value="<?= $shiftRepeatWeek ?>" />
            <input type="hidden" id="hdnRepeatWeekday" name="hdnRepeatWeekday" value="<?= $shiftRepeatWeekday ?>" />
            <input type="hidden" id="hdnShiftRepeats" name="hdnRepeats" value="<?= $shiftRepeats ?>" />
            <input type="hidden" id="hdnShiftCat" name="hdnRepeats" value="<?= $shiftCat ?>" />

	<?php } else {
		printShiftDetail($currentUserID, $shiftID, $postedShiftDate);
	} ?>
</form>

</body>
</html>