
<?php
/*---------------------------------------------------------------
 *
 * 	MODULE:		retrievePostBySections.php
 * 	AUTHOR:		Clay Pye
 * 	Created:	2019-06-06
 *
 * --------------------------------------------------------------
 *
 *
 * 	MODIFICATION HISTORY
 *
 *---------------------------------------------------------------
 */
$basepath = "C:\inetpub\wwwroot\VolunteerCalendar";

require ($basepath . '\includes\functions.php');

$userId = $_REQUEST["userId"];
$formattedNewsId = $_REQUEST["newsId"];

        global $db;
        connectDB();

        $userNewsSections = null;

        $sqlQuery = "SELECT * "
            . "FROM AppSection "
            . "INNER JOIN AppAdmin "
            . "ON AppSection.SectionID = AppAdmin.SectionID "
            . "WHERE AppAdmin.UserID = $userId "
            . "ORDER BY AppSection.SectionName ASC";

        $rs = $db->Execute($sqlQuery);

if ($rs->RowCount()) {

    while ($row = $rs->FetchRow()) {

        $sectionName = str_replace(" ","",$row['SectionName']);
        $sectionName = "f".$sectionName;

        $count = 0;
        if ($formattedNewsId != null) {
            $sqlQuery2 = "SELECT SectionName "
                . "FROM vAppNews "
                . "WHERE NewsID in (" . $formattedNewsId . ")";

            $rs2 = $db->Execute($sqlQuery2);

            $count = 0;

            while($row2 = $rs2->FetchRow()) {

                if($row2['SectionName'] == $row['SectionName']) {
                    echo "<tr>\n<td style=\"width:250px;\"><input type=\"checkbox\" class=\"CheckBoxes\" name=\"". $sectionName ."\"  id=\"". $sectionName ."\" value=\"1\" CHECKED /><span>" . $row['SectionName'] . "</span></td>\n";
                    $count = 1;
                }

            }
        }

        if($count == 0){
            echo "<tr>\n<td style=\"width:250px;\"><input type=\"checkbox\" class=\"CheckBoxes\" name=\"". $sectionName ."\"  id=\"". $sectionName ."\" value=\"1\" /><span>" . $row['SectionName'] . "</span></td>\n";
        }

    }
}

    $db->Close();


