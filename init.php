<?php
//session_destroy();
session_start();

error_reporting(E_ERROR);
ini_set('display_errors', '1');

$currentUserID 		= $_SESSION['caluser']['userid'];

// echo $currentUserID;

$currentName 		= $_SESSION['caluser']['name'];

$currentEmail 		= $_SESSION['caluser']['email'];
if(empty($currentEmail))
{
	$currentEmail = "N/A";
}
else
{
	$currentEmail = $currentEmail;
}

$currentPhone 		= $_SESSION['caluser']['phone'];
if(empty($currentPhone))
{
	$currentPhone = "N/A";
}
else
{
	$currentPhone = $currentPhone;
}

$currentUsername 	= $_SESSION['caluser']['username'];
$currentAdmin		= $_SESSION['caluser']['admin'];

$theMonth 		= $_GET['m'];
$theYear 		= $_GET['y'];
$currentCat 	= $_GET['cat'];

$curURL = $_SERVER["PHP_SELF"];

if (empty($currentUserID)) 
{
	if (!strpos($curURL, "login.php")) header('Location: login.php');
} 
else 
{
	if (strpos($curURL, "login.php")) header('Location: index.php');
}
?>

