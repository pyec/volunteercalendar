<?php

  /*---------------------------------------------------------------
   * 
   * 	MODULE:		AdminUserList02.php
   * 	AUTHOR:		Gian
   * 	Created:	Unknown
   * 
   * --------------------------------------------------------------
   * 
   * 	
   * 	MODIFICATION HISTORY
   * 	20170521 PRSC	Cleanup paths, code and PHP tabs fixed.
   *    * 
   *---------------------------------------------------------------
   */


    $basepath = $_SERVER['DOCUMENT_ROOT']."/VolunteerCalendar";

	require($basepath . "/header.php");


$filter 	= $_GET['filter'];
$action 	= $_GET['action'];

$sectionID 	= $_GET['section'];

//echo "<pre>";
//print_r(userBelongsTo($currentUserID));
sectionLinks($currentUserID);
//echo "</pre>";

//printUsers($currentUserID, $filter, $modifiedID, $action);
if(isset($filter)) {
	printUsersByFilter($currentUserID, $filter);

} else if(isset($sectionID)) {
	printUsersBySection($currentUserID, $sectionID);

} else {
	printUsers($currentUserID);
}

require($basepath . "/footer.php");

?>