<?php
  /*---------------------------------------------------------------
   * 
   * 	MODULE:		functions.php
   * 	AUTHOR:		Gian
   * 	Created:	Unknown
   * 
   * --------------------------------------------------------------
   * 
   * 	
   * 	MODIFICATION HISTORY
   * 	20170521 PRSC	Cleanup paths, code and PHP tabs fixed.
   *     
   *---------------------------------------------------------------
   */


//    $basepath = $_SERVER['DOCUMENT_ROOT']."/police/VolunteerCalendar";
    $basepath = "C:\inetpub\wwwroot\VolunteerCalendar";


	$mstr_homeloc = "https://apps.halifax.ca/VolunteerCalendar/login.php?action=FORGOT&status=FAIL";	
	
	
	?>
	
<?php 	

function connectDB() 
{
	global $db;
	require_once($basepath . "/adodb5/adodb.inc.php");

	$dbhost 	= "localhost";
	$dbuname 	= "HRPCalendar";
	$dbpass 	= "v0lunt33r!";
	//$dbname 	= "HRPCalendarTest2011";	// development dbase
	//$dbname 	= "HRPCalendar2008";  		// production dbase

	/*.............Version 2_3 Moved to apps.halifax.ca PRSC.....*/
//	$dbname 	= "hrp_volunteer_calendar";
	$dbname 	= "HRPCalendar2017";
	$dbuname 	= "sqladmin";
	$dbpass 	= "Hal1fax@2012";	
	
	$db = ADONewConnection('odbc_mssql');
	$db->SetFetchMode(ADODB_FETCH_ASSOC);
	$db->debug = false;
	$dsn = "Driver={SQL Server};Server=localhost;Database=$dbname;";
	$result = $db->Connect($dsn, $dbuname, $dbpass) or die("unable to connect to the database");
	// $result = $db->Connect($dsn, $dbuname, $dbpass);

	return $result;
}





function userLogout() 
{
	session_destroy();
}






function userLogin($username, $password) 
{
	global $db;
	connectDB();

//	$pattern = '[a-zA-Z]';
//	echo preg_match($pattern,$username);
	
//	$username = substr($username, 0, 8);

	$illegalCharacters = array("'", "=", ";", ":", "<", ">", ")", "(");

	$username = str_replace($illegalCharacters, "", $username);

	//$password = substr($username, 0, 10);
	//$pattern = '/.\(.[a-zA-Z]+.\)./';
	// preg_match('/regex/', $subject)
	// [a-zA-Z\s]
	//echo $username."<br/>";
	
	
	
		$sqlQuery = "SELECT * "
				  . "FROM vAppUser "
				  . "WHERE Username = '" . $username . "'";

//			$sqlQuery = "SELECT * "
//				  . "FROM Users "
//				  . "WHERE email_address = '" . $username . "'";
	
				  
				  
	   	$rs = $db->Execute($sqlQuery);

		/*..................DBG PRSC...............*/	   	
//	  	echo "QQQ [" . $sqlQuery . "]";
//		 echo "<pre>-------------------------";
//		 print_r($rs);
//		 print_r($rs['fields']);
//		 echo "----------------</pre>";
		
		
		if ($rs->RowCount()) 
		{
			$row = $rs->FetchRow();

			// echo "<pre>";
			// echo $password;
			// // print_r($row);
			// // print_r($row['Password']);
			// echo "</pre>";
			
			if ($row['Password'] != $password) {
				return "<font color=\"red\"><strong>Invalid Password or Username!</strong><br />Please try again.</font>";
				break;
	
			} elseif ($row['IsActive'] != 1) {
				return "<font color=\"red\"><strong>Your account is currently inactive.</strong><br />Please contact the 
				<a href=\"mailto:burnssh@halifax.ca\">administrator</a>
				 to reactivate it!</font>";
				break;
			}
			
			
		} 
		else 
		{
			return "<font color=\"red\"><strong>User Account not Found!</strong><br />Please try again.</font>";
			break;
		}
	
		/*..........Old Version 2_2..............PRSC */
		$_SESSION['caluser']['userid'] 		= $row['UserID'];
		$_SESSION['caluser']['name'] 		= $row['FirstName'] . " " . $row['LastName'];
		$_SESSION['caluser']['email']		= $row['Email1'];
		$_SESSION['caluser']['phone'] 		= $row['Phone1'];
		$_SESSION['caluser']['username'] 	= $row['Username'];

		
		/*...........New Version 2_4.............PRSC */
//		$_SESSION['caluser']['userid'] 		= $row['user_id'];
//		$_SESSION['caluser']['name'] 		= $row['first_name'] . " " . $row['last_name'];
//		$_SESSION['caluser']['email']		= $row['email_address'];
//		$_SESSION['caluser']['phone'] 		= $row['legacy_phone'];
//		$_SESSION['caluser']['username'] 	= $row['email_address'];
		
		
		// echo "<pre>";
		// // echo $password;
		// print_r($_SESSION);
		// echo "</pre>";
		
	
		header("Location: index.php");
}





function userForgetPassword($email)
{
	global $db;
	connectDB();

	$illegalCharacters = array("'", "=", ";", ":", "<", ">", ")", "(");
	$email = str_replace($illegalCharacters, "", $email);

// Old V2_2 Version PRSC	
	$sqlQuery = "SELECT * "
              . "FROM vAppUser "
              . "WHERE (Email1 = '" . $email . "') OR (Email2 = '" . $email . "')";

// V2_3 Version PRSC	
//	$sqlQuery = "SELECT * "
//              . "FROM dbo.Users "
//              . "WHERE (email_address = '" . $email . "')";
	
	$rs = $db->Execute($sqlQuery);
	$row = $rs->FetchRow(); 

	// echo "<pre>";
	// print_r($row);
	// echo "</pre>";
	
	//If Rows are Returned
	if($rs->RowCount())
	{
		$username 	= $row['Username'];
		$fname 		= $row['FirstName'];
		$lname 		= $row['LastName'];
		$active 	= $row['IsActive'];
		$password 	= base64_encode($row['Password']);
		
		//Pass the Users Information including the encrypted password to the form script located on the EServices Server.
		// header("Location: https://eservices.halifax.ca/_private/hrpcalendar/form.php?e=$email&u=$username&fn=$fname&ln=$lname&p=$password&a=$active");

		header("Location: https://apps.halifax.ca/HRPVolunteerCalendar/form.php?e=$email&u=$username&fn=$fname&ln=$lname&p=$password&a=$active");
//		header("Location: " . $mstr_homeloc);	
		
	}
	else //No Rows Returned
	{	
		//If No Rows are Returned the User entered invaild Information so kick them back to the Forgotten Password page
//		header("Location: https://appsdev.halifax.ca/VolunteerCalendar/login.php?action=FORGOT&status=FAIL");	
		
		/* WARNING - change this PRSC */
		header("Location: " . $mstr_homeloc);	

	}
}






function loadShiftData($shiftID) 
{
	global $db;
	connectDB();

	$sqlQuery = "SELECT * "
	          . "FROM vAppShift "
	          . "WHERE ShiftID = $shiftID";
	$rs = $db->Execute($sqlQuery);
	$row = $rs->FetchRow();

	$shiftValues = array($row['UserID'], $row['ShiftFrom'], $row['ShiftTo'], $row['Comment'], $row['SectionID']);
	return $shiftValues;
}






function printShiftDetail($userID, $shiftID) 
{
	global $db;
	connectDB();

	$sqlQuery = "SELECT * "
	          . "FROM vAppShift "
	          . "WHERE ShiftID = " . $shiftID . "";
	$rs = $db->Execute($sqlQuery);
	
	//If Rows are Returned
	if ($rs->RowCount()) 
	{
		$row = $rs->FetchRow();
		
		//If the ShiftDurationHours comes back a negative number, add 24 to offset it
		if(strpbrk($row['ShiftDurationHours'], "-"))
		{	
			$totaltoRemove = $row['ShiftDurationHours'] * 60;
			
			$row['ShiftDurationHours'] = $row['ShiftDurationHours'] + 24;
//			$row['ShiftDurationMinutes'] = $row['ShiftDurationMinutes'] - $totaltoRemove;
		}
		else
		{	
			$totaltoRemove = $row['ShiftDurationHours'] * 60;
			
			$row['ShiftDurationHours'] = $row['ShiftDurationHours'];
//			$row['ShiftDurationMinutes'] = $row['ShiftDurationMinutes'] - $totaltoRemove;
		}
		
		$row['ShiftDurationMinutes'] = $row['ShiftDurationMinutes'] - $totaltoRemove;

		//Check if minutes are negative
		if(strpbrk($row['ShiftDurationMinutes'], "-"))
		{	
			$row['ShiftDurationMinutes'] = $row['ShiftDurationMinutes'] + 60;
			$row['ShiftDurationHours'] = $row['ShiftDurationHours'] - 1;
		}



		$pageBody = "<tr><td class=\"label\">Volunteer</td><td>". $row['FirstName'] . " ". $row['LastName'] ." (" .$row['Phone1']. ")</td></tr>\n"
				  . "<tr><td class=\"label\">Date</td><td>". date("l F d Y", strtotime($row['ShiftFrom'])) ."</td></tr>\n"
				  . "<tr><td class=\"label\">Time</td><td>". date("g:i A", strtotime($row['ShiftFrom'])) ." - ". date("g:i A", strtotime($row['ShiftTo'])) ." (". $row['ShiftDurationHours'] . "h " . $row['ShiftDurationMinutes'] . "m)</td></tr>\n"
				  . "<tr><td class=\"label\">Section</td><td>". $row['SectionName'] . "</td></tr>\n"
				  . "<tr><td class=\"label\" valign=\"top\">Comments</td><td valign=\"top\">". nl2br($row['Comment']) . "</td></tr>\n"
				  . "<tr><td class=\"label\" valign=\"top\">Last Modified By</td><td valign=\"top\">". $row['AddedByName'] . "<br />". date("F d Y", strtotime($row['DateAdded'])) . "</td></tr>\n";
	} 

	$pageStart = "<table id=\"detailTable\" cellpadding=\"0\" cellspacing=\"0\" width=\"98%\">";

	$pageEnd = "</table><br>\n"
		     . "<div id=\"buttonbar\">";
	
	if(userIsAdminForShifts($userID,$row['SectionID']) || $row['UserID'] == $userID)
	{
		$pageEnd .= "<input type=\"button\" onClick=\"window.location.href='shiftDetail.php?shiftid=$shiftID&action=EDIT';\" value=\"Edit Shift\">"
		      	  . "<input type=\"button\" onClick=\"deleteShift('$shiftID');\" value=\"Delete Shift\">";
	}
	 
	$pageEnd .= "<input type=\"button\" onClick=\"window.close();\" value=\"Close\">"
		      . "</div>";

	echo $pageStart . $pageBody . $pageEnd;
}






function printShiftForm($shiftID, $mode) {
	switch ($mode) {
	case "ADDNEW":
		//addnew stuff here
		break;
	case "EDIT":
		//edit stuff here
		break;
	}
}





function printMessages($numItems, $userID) {
	global $db;
	connectDB();

	$sqlQuery = "SELECT TOP $numItems * "
	          . "FROM vAppMessage "
		      . "WHERE SentToID = $userID "
		      . "ORDER BY MessageDate DESC";
	$rs = $db->Execute($sqlQuery);

	echo "<table cellspacing=\"0\" border=\"0\" width=\"100%\" id=\"msgTable\">\n"
		. "<thead><tr><th>From</th><th>Subject</th><th>Date</th></tr></thead>"
		. "<tbody>\n";

	while ($row = $rs->FetchRow()) {
		if ($row['MessageStatus'] == 0) {
			$rowClass = " class=\"unread\" ";
			$bullet = "bullet_new";
		} else {
			$rowClass = "";
			$bullet = "bullet_old";
		}
			echo "<tr $rowClass>"
				. "<td><img src=\"images/$bullet.gif\" border=\"0\">". $row['SentBy'] ."</td>"
				. "<td>". $row['MessageSubject'] ."</td>"
				. "<td width=\"80\">". date("m/d/Y", strtotime($row['MessageDate'])) ."</td>"
				. "</tr>";
		}
		echo "</tbody>\n</table>\n";

	$db->Close();
}





function printNews($numItems, $sectionID, $userID) 
{
	global $db;
	connectDB();
	
	$sqlQuery = "SELECT * "
			  . "FROM vAppAdmin "
			  . "WHERE UserID = " . $userID . " ";
//			  . "AND SectionID = " . $sectionID . " "
//			  . "AND IsAdmin = 1";

	$rs = $db->Execute($sqlQuery);
//	$row = $rs->FetchRow();
	

	
	
	//Added by Gian
	$temp = " WHERE "; 

	while($row = $rs->FetchRow()) {
		$temp = $temp . "(SectionID = " . $row['SectionID'] . ") OR ";

		if($row['IsAdmin'] == 1)
		{
			$currentUser_IsAdmin = 1;
		}

	}
	
	$temp = substr($temp, 0,-3);


	
	//the SQL
	$sqlQuery = "SELECT * "
	          . "FROM vAppNews "
			  . $temp
		      . "ORDER BY PostingDate DESC, SectionName ASC";
			  
	$rs = $db->Execute($sqlQuery);
	
	//If Rows are Returned
	if($rs->RowCount()) 
	{
		$i = 0; //Define $i as 0
		$count = 0; //Define $count as 0
		
		$currentNewsInLoop = array(); //Define $currentNewsInLoop as an Array()
		$sectionNames = ""; //Define $sectionNames as a blank value
		$prevID = 0;
		
		//While Rows are Returned
		while($row = $rs->FetchRow())
		{
			//Fill $currentNewsInLoop Array() with values
			$currentNewsInLoop[$i] = $row['NewsText'];
			
			//Check if the current Rows matches that of the Array - 1
			if($row['NewsText'] != $currentNewsInLoop[$i - 1] && $count > 0) 
			{
				echo "<div class=\"newsItem\" style=\"min-width: 282px\">\n"
				   . "<div class=\"dateContainer\"><div class=\"month\">" . date("M", strtotime($prevMonthName)) . "</div>\n"
				   . "<div class=\"day\">" . date("d", strtotime($prevMonthDate)) . "</div>\n"
				   . "</div>\n"
				   . "<div class=\"slugline\">Posted in " . $sectionNames . " by " . $prevName . "</div>\n"
				   . "<div class=\"newsText\">" . $prevNewsEntry . "</div>\n"
				   . "</div>\n";
					
					if($currentUser_IsAdmin == 1)
					{
/*						echo "<div class='NewsControls'><a href='news.php?action=EDIT&newsid=" . $prevID . "&sectionid=" . $sectionID . "&postedbyid=" . $prevPostedByID . "&postingdate=" . $prevMonthDate . "'>edit</a>"*/
						echo "<div class='NewsControls'><a href='index.php?action=DELETE&newsid=" . base64_encode($prevID) . "'>Delete</a></div>"; 
					}
				 
				$sectionNames = ""; //Reset $sectionNames to a blank value
				$sectionID = 0;
				$prevID = 0;
			}
			
			//Preview News Item
			$prevMonthName = $row['PostingDate'];
			$prevMonthDate = $row['PostingDate'];
			$prevNewsEntry = $row['NewsText'];
			$prevName = $row['FirstName'] ." ". $row['LastName'];
			$prevPostedByID = $row['PostedByID'];
			
			if(prevID == "")
			{
				$prevID = $row['NewsID'];
			}
			else
			{
				if($prevID == 0)
				{
					$prevID = $row['NewsID'];
				}
				else
				{
					$prevID = $prevID . ", " . $row['NewsID'];
				}
			}
			
			//If $sectionNames is Empty
			if($sectionNames == "") 
			{
				$sectionNames = $row['SectionName'];
				$sectionID = $row['SectionID'];
			} 
			else //Not Empty
			{
				$sectionNames = $sectionNames . ", " . $row['SectionName'];
				$sectionID = $sectionID . ", " . $row['SectionID'];
			}

			$count++; //$count + 1
			$i++; //$i + 1
		}	
	}
	else
    {
		echo "<div class=\"newsItem\" style=\"min-width: 282px\">\n"
		   . "<div class=\"dateContainer\"><div class=\"month\">News</div><div class=\"day\">N/A</div></div>\n"
		   . "<div class=\"slugline\">Posted by Server</div>\n"
		   . "<div class=\"newsText\">There is no News currently Available.</div>\n"
		   . "</div>\n";
    }

	$db->Close();
}





function printShifts($userID, $type) 
{
	global $db;
	connectDB();

	if (strtoupper($type) != "VERBOSE") 
	{
		$startDate = date("m/d/Y");
		$endDate = date("m/d/Y", strtotime(dateAdd("d", 7, date("m/d/Y"))));

		$sqlQuery = "SELECT * "
		          . "FROM vAppShift "
			      . "WHERE userID = " . $userID . " "
			      . "AND ShiftFrom BETWEEN '" . $startDate . "' "
			      . "AND '" . $endDate . "' "
			      . "ORDER BY ShiftFrom";	  
		$rs = $db->Execute($sqlQuery);
		
		//If Rows are Returned
		if ($rs->RowCount()) 
		{
			echo "<table cellspacing=\"0\" border=\"0\" width=\"100%\" id=\"shiftTable\">\n"
			   . "<thead>\n"
			   . "<tr>\n"
			   . "<th>Date</th>\n"
			   . "<th>Start Time</th>\n"
			   . "<th>End Time</th>\n"
			   . "<th>Section</th>\n"
			   . "<th colspan=\"2\">Duration</th>\n"
			   . "</tr>\n"
			   . "</thead>\n"
			   . "<tbody>\n";
			
			//While Rows are Returned
			while ($row = $rs->FetchRow()) 
			{
				if (strtotime(date("m/d/Y", strtotime($row['ShiftFrom']))) == strtotime($startDate)) 
				{
					$rowClass = " class=\"today\" ";
				} 
				else 
				{
					$rowClass = "";
				}
				
				//If the ShiftDurationHours comes back a negative number, add 24 to offset it
				if(strpbrk($row['ShiftDurationHours'], "-"))
				{	
					$totaltoRemove = $row['ShiftDurationHours'] * 60;
					
					$row['ShiftDurationHours'] = $row['ShiftDurationHours'] + 24;
					$row['ShiftDurationMinutes'] = $row['ShiftDurationMinutes'] - $totaltoRemove;
				}
				else
				{	
					$totaltoRemove = $row['ShiftDurationHours'] * 60;
					
					$row['ShiftDurationHours'] = $row['ShiftDurationHours'];
					$row['ShiftDurationMinutes'] = $row['ShiftDurationMinutes'] - $totaltoRemove;
				}

				echo "<tr $rowClass>"
					. "<td width=\"100\">". date("D m/d/Y", strtotime($row['ShiftFrom'])) ."</td>"
					. "<td width=\"75\">". date("g:i A", strtotime($row['ShiftFrom'])) ."</td>"
					. "<td width=\"75\">". date("g:i A", strtotime($row['ShiftTo'])) ."</td>"
					. "<td>". $row['SectionName'] ."</td>"
					. "<td width=\"65\" align=\"right\">". $row['ShiftDurationHours'] ."h " . $row['ShiftDurationMinutes'] . "m</td>"
					. "<td width=\"50\" align=\"right\"><a href=\"javascript:popDetails('". $row['ShiftID'] ."')\">Details</a></td>"
					. "</tr>";
			}
			echo "</tbody>\n</table>\n";
		} 
		else //No Rows Returned
		{
			echo "You currently have no Upcoming Shifts.";
		}
	} 
	else 
	{
		$currentMonth = date("m");
		$loopMonth = 0;

		$sqlQuery = "SELECT datepart(mm, ShiftFrom) AS ShiftMonth, * "
		          . "FROM vAppShift "
			      . "WHERE userID = " . $userID . " "
			      . "AND DATEPART(mm, ShiftFrom) >= $currentMonth "
			      . "ORDER BY ShiftFrom";		  
		$rs = $db->Execute($sqlQuery);
		
		//If Rows are Returned
		if($rs->RowCount()) 
		{
			//While Rows are Returned
			while ($row = $rs->FetchRow()) 
			{
				if (strtotime(date("m/d/Y", strtotime($row['ShiftFrom']))) == strtotime($startDate)) 
				{
					$rowClass = " class=\"today\" ";
				} 
				else 
				{
					$rowClass = "";
				}

				if ($loopMonth != $row['ShiftMonth']) 
				{
					if ($loopMonth != 0) echo "</tbody>\n</table><br />\n";

					$loopMonth = $row['ShiftMonth'];
					echo "<h1>" . date("F Y", strtotime($row['ShiftFrom'])) . "</h1>"
					   . "<table cellspacing=\"0\" border=\"0\" width=\"100%\" id=\"shiftTable\">\n"
					   . "<thead>\n"
					   . "<tr>\n"
					   . "<th>Date</th>\n"
					   . "<th>Start Time</th>\n"
					   . "<th>End Time</th>\n"
					   . "<th>Section</th>\n"
					   . "<th colspan=\"2\">Duration</th>\n"
					   . "</tr>\n"
					   . "</thead>\n"
					   . "<tbody>\n";
				}

				echo "<tr $rowClass>"
					. "<td width=\"100\">" . date("D m/d/Y", strtotime($row['ShiftFrom'])) . "</td>"
					. "<td width=\"75\">" . date("g:i A", strtotime($row['ShiftFrom'])) . "</td>"
					. "<td width=\"75\">" . date("g:i A", strtotime($row['ShiftTo'])) . "</td>"
					. "<td>" . $row['SectionName'] . "</td>"
					. "<td width=\"65\" align=\"right\">" . $row['ShiftDuration'] . " hrs</td>"
					. "<td width=\"50\" align=\"right\"><a href=\"javascript:popDetails('" . $row['ShiftID'] . "')\">Details</a></td>"
					. "</tr>";
			}
		}
	}

	$db->Close();
}






function printUserControls($userID)
{
	global $db;
	connectDB();
	
	$sqlQuery = "SELECT IsAdmin, SectionName, SectionID "
	          . "FROM vAppUserControl "
			  . "WHERE UserID = " . $userID . " "
			  . "GROUP BY SectionName, SectionID, IsAdmin";		
	$rs = $db->Execute($sqlQuery);
	
	echo "<div style=\"float: left; background-color: #eee; clear: both\">"
	   . "<table width=\"380px\">"
	   . "<tr>"
	   . "<td colspan=\"4\"><h1>Section Selection</h1></td>"
	   . "</tr>";
	   
	//If Rows are Returned
	if ($rs->RowCount()) 
	{
		$i = 0; //Define $i as 0
		
		//While Rows are Returned
		while($row = $rs->FetchRow())
		{	
			echo "<tr>"
			   . "<td width=\"180px\">" . $row['SectionName'] . "</td>";
		   
			echo "<td width=\"16px\" align=\"right\"><a href=\"shifts.php?type=me&shiftid=" . $row['SectionID'] . "\"><img src=\"images/user_go.png\" border=\"0\" alt=\"You\" title=\"View your Shifts by Category\" /></a></td>";
	
			//Check if the Current Logged in User is an Admin of the Current Section in the Loop
			if($row['IsAdmin'] == 1)
			{
				//Image Link to filter the each Section, will show all users but this link is only available to Admin of the Section
				echo "<td width=\"16px\" align=\"right\"><a href=\"shifts.php?type=category&shiftid=" . $row['SectionID'] . "\"><img src=\"images/group_go.png\" border=\"0\" alt=\"Group\" title=\"View everyone's Shifts by Category\" /></a></td>";
			}
			echo "</tr>";
			
			$usedSection[$i] = $row['SectionID']; //Add SectionID to an Array useing $i as Array Keys
	
			$i++; //$i + 1
		} 
	}
	
	//Count how man Section that the current user has hours in
	$usedSection_Count = count($usedSection); 

	$sqlQuery = "SELECT IsAdmin, SectionName, SectionID "
	          . "FROM vAppAdmin "
	          . "WHERE UserID = " . $userID . " "; 
	
	//Loop though each of the Used Section from the Above SQL Query, Select any SectionID not used Above
	for($i = 0; $i < $usedSection_Count; $i++)
	{
		if($i >= 1)
		{
			$sqlQuery .= "AND SectionID != " . $usedSection[$i] . " ";
		}
		else
		{
			$sqlQuery .= "AND SectionID != " . $usedSection[$i] . " ";
		}
	}	  
	$sqlQuery .= "GROUP BY SectionName, SectionID, IsAdmin";
	$rs = $db->Execute($sqlQuery);
	
	//If Rows are Returned
	if ($rs->RowCount()) 
	{
		//While Rows are Returned
		while($row = $rs->FetchRow())
		{
			echo "<tr>"
			   . "<td width=\"180px\">" . $row['SectionName'] . "</td>"
			    //Image Link to filter the current users by Section
			   . "<td width=\"16px\" align=\"right\"><a href=\"shifts.php?type=me&shiftid=" . $row['SectionID'] . "\"><img src=\"images/user_go.png\" border=\"0\" alt=\"You\" title=\"View your Shifts by Category\"/></a></td>";
			
			//Check if the current user is admin of the current section in the loop
			if($row['IsAdmin'] == 1)
			{
				//Image Link to filter the each Section, will show all users but this link is only available to Admin of the Section
				echo "<td width=\"16px\" align=\"right\"><a href=\"shifts.php?type=category&shiftid=" . $row['SectionID'] . "\"><img src=\"images/group_go.png\" border=\"0\" alt=\"Group\" title=\"View everyone's Shifts by Category\" /></a></td>";
			}
			
			echo "</tr>";
		} 
	} 
	echo "</table>"
       . "</div>";
	
	$db->Close();
}






// *****************************************
// CASCADING SECTION / USER DROP DOWN MENUS
// *****************************************

function sectionUserDropDownMenu($UserID) {

	global $db;
	connectDB();
	
	$output 					= "";
	$sectionDropDown 			= "";
	$userSectionDropDowns 		= "";
	$allUsers					= "";
	$sectionCount				= 1;
	$isAdmin					= userIsAdmin($UserID);
	
	$usersSections = array();
	
	// if user is admin show all
//	if( userIsAdmin($UserID) ) {
		
		// Get all available sections
//		$sqlQuerySections 	= "SELECT SectionID, SectionName "
//							. "FROM vAppSectionNames "
//							. "ORDER BY SectionName ASC";

	// if user is NOT admin	
//	} else {
		
		// Get all available sections
		$sqlQuerySections 	= "SELECT SectionID, SectionName "
							. "FROM vAppSectionNames "
							. "WHERE";

		$usersSections = userBelongsTo($UserID); 
		
		$arraySize = count($usersSections);
		$i = 1;
		
		foreach($usersSections as $value) {
			
			$sqlQuerySections 	.= " (SectionID = ".$value;
			
			if($i < $arraySize) {
				$sqlQuerySections 	.= ") OR ";
				
			} else {
				$sqlQuerySections 	.= ") ";
			}
			
			$i++;
		}
						
		$sqlQuerySections 	.= "ORDER BY SectionName ASC";
	
//	}

	$rs = $db->Execute($sqlQuerySections);
	
	// If records exist
	if($rs->RowCount()) {
			
		// Start section drop down menu
//		$sectionDropDown 	.= "<select id=\"Categories\" name=\"selected_section\" onchange=\"displayDropDown(this,".$rs->RowCount().")\">\n"
		$sectionDropDown 	.= "<select id=\"Categories\" name=\"selected_section\" >\n"
							. "<option value=\"0\">All Sections</option>\n";
		
		// Show all Categories
		// Show all users

		$userSectionDropDowns = "<option value=\"0\">All Users</option>\n";

		// Loop through each section
		while($row = $rs->FetchRow()) {	
		
			// Add section to the section drop down menu
			$sectionDropDown .= "<option value=\"".$row['SectionID']."\">".$row['SectionName']."</option>\n";

			if(userIsAdminForShifts($UserID,$row['SectionID'])) {
					
				//$userSectionDropDowns 	.= "<select id=\"UsersSection".$row['SectionID']."\" name=\"selected_user\" style=\"display:none;\">\n";
				
				// **********************************
				// PRINT USER MENU FOR THAT SECTION
				// **********************************
	
				if( $isAdmin ) {
					
					// Get user data
					$sqlQueryUsers 	= "SELECT UserID, FirstName, LastName "
									. "FROM vAppUsers "
									. "WHERE (IsActive = 1) AND (SectionID = ".$row['SectionID'].")"
									. "ORDER BY FirstName ASC";
							
					$rsUsers = $db->Execute($sqlQueryUsers);
					
					// If users exist
					if($rsUsers->RowCount()) {
						
						// Open/Start user select menu
						$userSectionDropDowns 	.= "<optgroup id=\"UsersSection".$row['SectionID']."\" label=\"".$row['SectionName']."\">\n";
					
						// Loop through users that belong to that section
						while($user = $rsUsers->FetchRow()) {	
						
							// Add users to the drop down menu
							$userSectionDropDowns .= "<option value=\"".$user['UserID']."\">".$user['FirstName']." ".$user['LastName']."</option>\n";
						
						}
						
						// Close/End user select menu
						$userSectionDropDowns .= "</optgroup>\n\n";
							
					}	
				
				}
				
			}
			
			//$userSectionDropDowns .= "</select>\n";

		}	
		
		// Close/End user select menu
		$sectionDropDown .= "</select>\n<br />\n";
		
	}
	
	// ***********************
	// JUST LIST CURRENT USER
	// ***********************
	if( !$isAdmin ) {

		// Get user data
		$sqlQueryUsers 	= "SELECT UserID, FirstName, LastName "
						. "FROM vAppUsers "
						. "WHERE (IsActive = 1) AND (UserID = ".$UserID.") " 
						. "ORDER BY FirstName ASC";	
	
		$rsUsers = $db->Execute($sqlQueryUsers);
		
		// If users exist
		if($rsUsers->RowCount()) {

			$user = $rsUsers->FetchRow();
			$userSectionDropDowns = "<option value=\"".$user['UserID']."\">".$user['FirstName']." ".$user['LastName']."</option>\n";
			
		}
			
	}
	
	
			
	// Assemble output order
	$output .= $sectionDropDown."\n"
			. "</td>\n"
	   		. "</tr>\n"
	   		. "<tr>\n"
	   		. "<td align=\"right\">User: </td>\n"
	   		. "<td align=\"right\" style=\"padding-right: 27px;\">\n"
			. "<select name=\"selected_user\">\n".$userSectionDropDowns."</select>\n";
	
	return $output;

}





function printUserHourFilter($selected_section, $start_date, $end_date)
{
	global $db;
	connectDB();

	//echo "<div style=\"float: right; width:400px;\">";
	
	//Shift Date Filter
	echo "<div style=\"background-color: #eee; width:480px;padding:10px;\">\n"
	   . "<table width=\"100%\">\n"
	   . "<tr>\n"
	   . "<td colspan=\"2\"><h1>Hour Filter</h1></td>\n"
	   . "</tr>\n"
	   . "<tr>\n"
	   . "<td>Section: </td>\n"
	   . "<td align=\"right\" style=\"padding-right: 27px\">\n"       
	   . "<form name=\"shiftForm\" method=\"get\">\n"
	   . "<input type=\"hidden\" name=\"type\" value=\"date\" />\n";


		// Get all available sections
		$sqlQuerySections 	= "SELECT SectionID, SectionName "
							. "FROM vAppSectionNames ORDER BY SectionName ASC";
	
		$rs = $db->Execute($sqlQuerySections);
		
		// If records exist
		if($rs->RowCount()) {
				
			// Start section drop down menu
			$sectionDropDown 	.= "<select id=\"Categories\" name=\"selected_section\" >\n";
			
			while($row = $rs->FetchRow()) {	
				// Add section to the section drop down menu
				if($row['SectionID'] == $selected_section) {
					$sectionDropDown .= "<option value=\"".$row['SectionID']."\" SELECTED>".$row['SectionName']."</option>\n";
				} else {
					$sectionDropDown .= "<option value=\"".$row['SectionID']."\">".$row['SectionName']."</option>\n";
				}
			}
			
			$sectionDropDown 	.= "</select>\n";
			
		}
		
		echo $sectionDropDown;
		
		// Show all Categories
		// Show all users
	   
	echo "</td>\n"
	   . "</tr>\n"
	   . "<tr>\n"
	   . "<td width=\"100px\">Start Date: </td>\n"
	   . "<td class=\"SearchBox\">\n"
	   . "<input class=\"datesel\" type=\"text\" name=\"start_date\" readonly value=\"$start_date\" />\n"
	   . "<img src=\"images/icon_cal.gif\" border=\"0\" title=\"Calendar\" onclick=\"displayCalendar(document.shiftForm.start_date, 'mm/dd/yyyy', this)\" onmouseover=\"this.style.cursor='pointer';\">\n"
	   . "<img src=\"images/refresh.png\" border=\"0\" title=\"Clear Start Date\" onclick=\"document.forms[0].start_date.value = '';\" onmouseover=\"this.style.cursor='pointer';\" />\n"
	   . "</td>\n"
	   . "</tr>\n"
	   . "<tr>\n"
	   . "<td width=\"100px\">End Date: </td>\n"
	   . "<td class=\"SearchBox\">\n"
	   . "<input class=\"datesel\" type=\"text\" name=\"end_date\" readonly value=\"$end_date\" />\n"
	   . "<img src=\"images/icon_cal.gif\" border=\"0\" onclick=\"displayCalendar(document.shiftForm.end_date, 'mm/dd/yyyy', this)\" onmouseover=\"this.style.cursor='pointer';\">\n"
	    . "<img src=\"images/refresh.png\" border=\"0\" title=\"Clear End Date\" onclick=\"document.forms[0].end_date.value = '';\" onmouseover=\"this.style.cursor='pointer';\" />\n"
	   . "</td>\n"
	   . "</tr>\n"
	   . "</table>\n"
	   . "<div id=\"buttonbar\" style=\"text-align:right;padding-right:30px;\">\n"
	   . "<input type=\"submit\" value=\"Search\" />\n"
	   . "</div>\n"
	   . "</form>\n"
	   . "</div>\n";
	   //. "</div>\n";
}





function printAdvancedFilter($userID, $filter, $selected_section, $selected_user, $start_date, $end_date)
{
	global $db;
	connectDB();

	echo "<div style=\"float: right; width=400px;\">";
	
	//Shift Date Filter
	echo "<div style=\"background-color: #eee;\">\n"
	   . "<table width=\"400px\">\n"
	   . "<tr>\n"
	   . "<td colspan=\"2\"><h1>Shift Filter</h1></td>\n"
	   . "</tr>\n"
	   . "<tr>\n"
	   . "<td align=\"right\">Section: </td>\n"
	   . "<td align=\"right\" style=\"padding-right: 27px\">\n"       
	   . "<form name=\"shiftForm\" method=\"get\">\n"
	   . "<input type=\"hidden\" name=\"type\" value=\"date\" />\n";
	   
	   
	echo sectionUserDropDownMenu($userID);
	   
	   
	echo "</td>\n"
	   . "</tr>\n"
	   . "<tr>\n"
	   . "<td width=\"100px\" align=\"right\">Start Date: </td>\n"
	   . "<td class=\"SearchBox\" width=\"400px\">\n"
	   . "<input class=\"datesel\" type=\"text\" name=\"start_date\" readonly value=\"$start_date\" />\n"
	   . "<img src=\"images/icon_cal.gif\" border=\"0\" title=\"Calendar\" onclick=\"displayCalendar(document.shiftForm.start_date, 'mm/dd/yyyy', this)\" onmouseover=\"this.style.cursor='pointer';\">\n"
	   . "<img src=\"images/refresh.png\" border=\"0\" title=\"Clear Start Date\" onclick=\"document.forms[0].start_date.value = '';\" onmouseover=\"this.style.cursor='pointer';\" />\n"
	   . "</td>\n"
	   . "</tr>\n"
	   . "<tr>\n"
	   . "<td width=\"100px\" align=\"right\">End Date: </td>\n"
	   . "<td class=\"SearchBox\" width=\"400px\">\n"
	   . "<input class=\"datesel\" type=\"text\" name=\"end_date\" readonly value=\"$end_date\" />\n"
	   . "<img src=\"images/icon_cal.gif\" border=\"0\" onclick=\"displayCalendar(document.shiftForm.end_date, 'mm/dd/yyyy', this)\" onmouseover=\"this.style.cursor='pointer';\">\n"
	    . "<img src=\"images/refresh.png\" border=\"0\" title=\"Clear End Date\" onclick=\"document.forms[0].end_date.value = '';\" onmouseover=\"this.style.cursor='pointer';\" />\n"
	   . "</td>\n"
	   . "</tr>\n"
	   . "</table>\n"
	   . "<div id=\"buttonbar\">\n"
	   . "<input type=\"submit\" value=\"Search\" />\n"
	   . "</div>\n"
	   . "</form>\n"
	   . "</div>\n"
	   . "</div>\n";
}




/* OLD

function printAdvancedFilter($userID, $filter, $selected_section, $selected_user, $start_date, $end_date)
{
	global $db;
	connectDB();

	echo "<div style=\"float: right; width=400px;\">";
	
	//Shift Date Filter
	echo "<div style=\"background-color: #eee;\">\n"
	   . "<table width=\"400px\">\n"
	   . "<tr>\n"
	   . "<td colspan=\"2\"><h1>Shift Filter</h1></td>\n"
	   . "</tr>\n"
	   . "<tr>\n"
	   . "<td align=\"right\">Section: </td>\n"
	   . "<td align=\"right\" style=\"padding-right: 27px\">\n"       
	   . "<form name=\"shiftForm\" method=\"get\">\n"
	   . "<input type=\"hidden\" name=\"type\" value=\"date\" />\n"
	   . "<select name=\"selected_section\" style=\"color: #999999\">\n";
	   
		//If the selected sectionid is greater then 0, set the dropdown to the current section specified
		if($selected_section > 0) 
		{
			//Query the section specified in the url parma
			$sqlQuery = "SELECT * "
			          . "FROM AppSection "
			          . "WHERE SectionID = " . $selected_section . "";
			$rs = $db->Execute($sqlQuery);
			$row = $rs->FetchRow();
			
			echo "<option value=\"$selected_section\">" . $row['SectionName'] . "</option>\n<option value=\"0\">All Categories</option>\n<option value=\"0\"></option>\n";
		}
		else //No url parma
		{
			echo "<option value=\"0\" selected=\"selected\">--All Sections--</option>\n<option value=\"0\"></option>\n";
		}
		
		//Category Selection
		$sqlQuery = "SELECT * "
	              . "FROM AppSection "
		          . "INNER JOIN AppAdmin "
		          . "ON AppSection.SectionID = AppAdmin.SectionID "
		          . "WHERE AppAdmin.UserID = " . $userID . " "
		          . "ORDER BY AppSection.SectionName ASC";
		$rs = $db->Execute($sqlQuery);
		
		//While Rows are Returned
		while ($row = $rs->FetchRow()) 
		{
			echo "<option value=\"" . $row['SectionID'] . "\">" . $row['SectionName'] . "</option>\n";
		}
		
	echo "</select>\n"
	   . "</td>\n"
	   . "</tr>\n"
	   . "<tr>\n"
	   . "<td align=\"right\">Users: </td>\n"
	   . "<td align=\"right\" style=\"padding-right: 27px;\">\n"
       . "<select name=\"selected_user\" style=\"color: #999999\">\n";
	   
	   	//If the selected userid is greater then 0, set the dropdown to the current user specified
	   	if($selected_user > 0) 
	   	{
			//Query the user specified in the url parma
			$sqlQuery = "SELECT * "
			          . "FROM AppUser "
			          . "WHERE UserID = " . $selected_user . "";
			$rs = $db->Execute($sqlQuery);
			$row = $rs->FetchRow();
		
			echo "<option value=\"$selected_user\">" . $row['FirstName'] . " " . $row['LastName'] . "</option>\n<option value=\"0\">--All Users--</option>\n<option value=\"0\"></option>\n";
		}
		else //No url parma
		{
			echo "<option value=\"0\" selected=\"selected\">--All Users--</option>\n"
			   . "<option value=\"0\"></option>\n";
		}
		
		$sqlQuery = "SELECT * "
		          . "FROM AppAdmin "
		          . "WHERE UserID = " . $userID . " "
				  . "AND IsAdmin = 1";
		$rs = $db->Execute($sqlQuery);
		
		//If Rows are Returned
		if($rs->RowCount()) 
		{
			$i = 0; //Define $i as 0
		
			//User Selection
			$sqlQuery = "SELECT UserID, FirstName, LastName, IsActive, SectionID "
			          . "FROM vAppUsers WHERE ";

			//Loop though all Sections available to the current or selected user
			while($row = $rs->FetchRow())
			{
				//If $i is Less Then 1
				if($i < 1)
				{
					$sqlQuery .= "((SectionID = " . $row['SectionID'] . ")"; //Only used if one Section is available.
				}
				else //Not Less Then 1
				{
					$sqlQuery .= " OR (SectionID = " . $row['SectionID'] . ")"; //Adds this once $i has counted over 1
				}
				
				$i++; //$i + 1
			}
			
			$sqlQuery .= ") "
			           . "AND IsActive = 1 "
			           //. "GROUP BY FirstName, LastName, UserID, IsActive, SectionID";
					   . "ORDER BY FirstName, LastName ASC";
			$rs = $db->Execute($sqlQuery);
			
			$i = 0; //Define $i as 0
			
			$currentUserInLoop = array();
			
			//While Rows are Returned
			while ($row = $rs->FetchRow()) 
			{
				$currentUserInLoop[$i] = $row['UserID'];
				
				if($row['UserID'] != $currentUserInLoop[$i - 1])
				{
					echo "<option value=\"" . $row['UserID'] . "\">". $row['FirstName'] . " " . $row['LastName'] . "</option>\n";
				}
				
				$i++; //$i + 1
			}
		}
		else //No Rows Returned
		{
			$sqlQuery = "SELECT * "
					  . "FROM AppUser "
					  . "WHERE UserID = " . $userID . ""; 
			$rs = $db->Execute($sqlQuery);
			
			//While Rows are Returned
			while ($row = $rs->FetchRow()) 
			{
				echo "<option value=\"" . $row['UserID'] . "\" selected=\"selected\">". $row['FirstName'] . " " . $row['LastName'] . "</option>\n";
			}
		}
	echo "</select>\n"
	   . "</td>\n"
	   . "</tr>\n"
	   . "<tr>\n"
	   . "<td width=\"100px\" align=\"right\">Start Date: </td>\n"
	   . "<td class=\"SearchBox\" width=\"400px\">\n"
	   . "<input class=\"datesel\" type=\"text\" name=\"start_date\" readonly value=\"$start_date\" />\n"
	   . "<img src=\"images/icon_cal.gif\" border=\"0\" title=\"Calendar\" onclick=\"displayCalendar(document.shiftForm.start_date, 'mm/dd/yyyy', this)\" onmouseover=\"this.style.cursor='pointer';\">\n"
	   . "<img src=\"images/refresh.png\" border=\"0\" title=\"Clear Start Date\" onclick=\"document.forms[0].start_date.value = '';\" onmouseover=\"this.style.cursor='pointer';\" />\n"
	   . "</td>\n"
	   . "</tr>\n"
	   . "<tr>\n"
	   . "<td width=\"100px\" align=\"right\">End Date: </td>\n"
	   . "<td class=\"SearchBox\" width=\"400px\">\n"
	   . "<input class=\"datesel\" type=\"text\" name=\"end_date\" readonly value=\"$end_date\" />\n"
	   . "<img src=\"images/icon_cal.gif\" border=\"0\" onclick=\"displayCalendar(document.shiftForm.end_date, 'mm/dd/yyyy', this)\" onmouseover=\"this.style.cursor='pointer';\">\n"
	    . "<img src=\"images/refresh.png\" border=\"0\" title=\"Clear End Date\" onclick=\"document.forms[0].end_date.value = '';\" onmouseover=\"this.style.cursor='pointer';\" />\n"
	   . "</td>\n"
	   . "</tr>\n"
	   . "</table>\n"
	   . "<div id=\"buttonbar\">\n"
	   . "<input type=\"submit\" value=\"Search\" />\n"
	   . "</div>\n"
	   . "</form>\n"
	   . "</div>\n"
	   . "</div>\n";
}



*/



/******************************************************************************
  ADDED:  		July 22th, 2011
  AUTHOR: 		Gian Pompilio
  DESCRIPTION:	Prints a list of user and their total hours for a giving 
  				timeframe by category and date range. Based on 
				printShiftByDateFilter() found below.
*******************************************************************************/
function printShiftHourByDateRange($selected_section, $start_date, $end_date)
{		
	global $db;
	connectDB();
	
	// Check that variables were passed and were not empty.
	if(isset($selected_section) && isset($start_date) && isset($end_date)
		&& $selected_section != '' && $start_date != '' && $end_date != '')
	{
		// Variable declarations
		$shiftHours 		= 0;
		$shiftMins		 	= 0;
		$userHours 			= 0;
		$userMins		 	= 0;
		$sectionHours 		= 0;
		$sectionMins 		= 0;
		$lastUserID			= 0;
		$count	 			= 0;
		$output				= "";
		$noRows				= "There are no records in that date range.";
			
		// Explode the Start Date into an Array and store them in the appropriate variables
		$start_dateArray 	= explode("/", $start_date);
		$startDate_Month 	= $start_dateArray[0];
		$startDate_Day		= $start_dateArray[1];
		$startDate_Year		= $start_dateArray[2];
		$startDate_Time		= " 00:00:00.000";
		$start_date 		= $startDate_Month . "/" . $startDate_Day ."/" . $startDate_Year;
		
		// Explode the End Date into an Array and store them in the appropriate variables
		$end_dateArray 		= explode("/", $end_date);
		$endDate_Month 		= $end_dateArray[0];
		$endDate_Day		= $end_dateArray[1];
		$endDate_Year		= $end_dateArray[2];
		$endDate_Time		= " 00:00:00.000";
		$end_date 			= $endDate_Month . "/" . $endDate_Day ."/" . $endDate_Year;
	
		// Get all the records between a particular date and sort them by user id
		$sqlQuery 			= "SELECT * FROM vAppShift WHERE ShiftFrom BETWEEN '" . $start_date . "' AND '" . $end_date . "' AND SectionID = " . $selected_section . "ORDER BY UserID ASC";	
		$rs 				= $db->Execute($sqlQuery);

		// If records exist
		if($numRowCount = $rs->RowCount()) 
		{
			// Print column headers
			echo "<table cellspacing=\"0\" border=\"0\" style=\"width:500px; margin-top:15px;\" id=\"shiftTable\">\n"
				   ."<thead>\n"
				   ."<tr>\n"
					   ."<th>Volunteer</th>\n"
					   ."<th style=\"text-align:right;\">Time</th>\n"
				   ."</tr>\n"
				   ."</thead>\n"
				   ."<tbody>\n";
	
			// Loop through records		
			while ($row = $rs->FetchRow()) 
			{						
				// Print previous users details when a new user is detected (but ignore the first instance
				if($row['UserID'] != $lastUserID && $count != 0) 
				{	
					echo "<tr>\n"
						 ."<td>".$lastUserName."</td>\n"
						 ."<td style=\"text-align:right;\" >".$userHours."h ".$userMinutes."m</td>\n"
						 ."</tr>\n";
					
					// Reset hours and minutes
					$userHours 		= 0;
					$userMinutes 	= 0;		
					
				}
				
				// Parse shift duration from database
				if(strpbrk($row['ShiftDurationHours'], "-")) 							// If shift hours are negative
				{
					$totaltoRemove 	= $row['ShiftDurationHours'] * 60;					// Convert hours to minutes
					$shiftHours 	= $row['ShiftDurationHours'] + 24;					// Get hours - add 24 to offset negative value
					$shiftMinutes 	= $row['ShiftDurationMinutes'] - $totaltoRemove;	// Dertermine remaining minutes
				
				} 
				else																	// If shift hours are positive 
				{
					$totaltoRemove 	= $row['ShiftDurationHours'] * 60;					// Convert hours to minutes
					$shiftHours		= $row['ShiftDurationHours'];						// Get total hours
					$shiftMinutes	= $row['ShiftDurationMinutes'] - $totaltoRemove;	// Dertermine remaining minutes
				}
				
				if(strpbrk($shiftMinutes, "-"))								// If minutes are negative, make adjustments
				{
					$shiftMinutes 	= $shiftMinutes + 60;					// Add 60 minutes
					$shiftHours 	= $shiftHours - 1;						// And, remove an hour
				}
				
				if($shiftMinutes >= 60)										// If remaining minutes are 60 or more, make adjustments
				{
					$shiftMinutes -= 60;									// Substract 60 minutes
					$shiftHours += 1;										// Add 1 hour
				}
				
				$userHours += $shiftHours;									// Add shifts hours to total users hours
				$userMinutes += $shiftMinutes;								// Add shifts minutes to total users minutes
				
				if($userMinutes >= 60)										// If total user minutes is 60 or more, make adjustments
				{
					$userMinutes -= 60;										// Substract 60 minutes
					$userHours += 1;										// Add 1 hour
				}
				
				$sectionHours += $shiftHours;								// Add user hours to total section hours
				$sectionMins += $shiftMinutes;								// Add user minutes to total section minutes
				
				if($sectionMins >= 60)										// If total section minutes is 60 or more, make adjustments
				{
					$sectionMins -= 60;										// Substract 60 minutes
					$sectionHours += 1;										// Add 1 hour
				}	
			
				$lastUserID 	= $row['UserID'];							// update lastUserID
				$lastUserName	= $row['FirstName']." ".$row['LastName'];	// update lastUserName
							
				$count++;													// update counter
				
				if($count == $numRowCount)									// if this is the last record, print the final row
				{
					echo "<tr>\n"
						 ."<td>".$lastUserName."</td>\n"
						 ."<td style=\"text-align:right;\" >".$userHours."h ".$userMinutes."m</td>\n"
						 ."</tr>\n";
				}
				
				// FOR TESTING/DEBUGING ONLY - Capture every shift line-by-line.
				$output .= "<tr>\n"
						."<td>".$row['FirstName']." ".$row['LastName']."</td>\n"
						."<td style=\"text-align:right;\" >".$shiftHours."h ".$shiftMinutes."m</td>\n"
						."</tr>\n";
				
			}
			
			// print total hours the entire section during this date range.
			echo 	"<tr>\n"
					."<td style=\"font-weight:bold;\">Total for Section</td>\n"
					."<td style=\"text-align:right; font-weight:bold;\" >".$sectionHours."h ".$sectionMins."m</td>\n"
					."</tr>\n";
			
					
			echo "</tbody></table>";
			
			// FOR TESTING/DEBUGING ONLY
			/* 
			echo "<table>\n".$output."</table>\n";
			echo $count;  
			*/
	
		}
		else		// If no records were returned, display this message.
		{
			echo "<p>No records were found.</p><p>Please select a section and date range from the 'Hour Filter' box above.</p>";
		}
	}
	else			// If no variables were passed into the function, display this message.
	{
		echo "<p>Please select a section and date range from the 'Hour Filter' box above.</p>";
	}
	
	$db->Close();	// Close the database connection.
	
}








function printShiftByDateFilter($userID, $selected_section, $selected_user, $start_date, $end_date)
{		
	global $db;
	connectDB();
		
	//Explode the Start Date into an Array
	$start_dateArray 	= explode("/", $start_date);
	
	$startDate_Month 	= $start_dateArray[0];
	$startDate_Day		= $start_dateArray[1];
	$startDate_Year		= $start_dateArray[2];
	$startDate_Time		= " 00:00:00.000";
	
	$start_date = $startDate_Month . "/" . $startDate_Day ."/" . $startDate_Year;
	
	//Explode the End Date into an Array
	$end_dateArray 		= explode("/", $end_date);
	
	$endDate_Month 		= $end_dateArray[0];
	$endDate_Day		= $end_dateArray[1];
	$endDate_Year		= $end_dateArray[2];
	$endDate_Time		= " 00:00:00.000";
	
	$end_date = $endDate_Month . "/" . $endDate_Day ."/" . $endDate_Year;

	$sqlQuery = "SELECT * "
	          . "FROM vAppAdmin "
	          . "WHERE UserID = " . $userID . " "; 
	$rs = $db->Execute($sqlQuery);
	
	//If Rows are Returned
	if($rs->RowCount()) 
	{
		$i = 0; //Define $i as 0
		
		$sqlQuery = "SELECT DATEPART(mm, ShiftFrom) AS ShiftMonth, * "
		          . "FROM vAppShift ";
		
		//If $start_date and $end_date are not empty
		if($start_date != "//" && $end_date != "/1/")
		{
			$sqlQuery .= "WHERE ShiftFrom BETWEEN '" . $start_date . "' AND '" . $end_date . "' AND ";
		}
		else //Not Empty
		{
			$sqlQuery .= "WHERE ";
		}
		
		//Check if there is a selected section
		if($selected_section == 0)
		{
			//While Rows are Returned
			while($row = $rs->FetchRow())
			{
				//If $i is Less Then 1
				if($i < 1)
				{
					$sqlQuery .= "((SectionID = " . $row['SectionID'] . ")"; //Only used if one Section is available.
				}
				else //Not Less Then 1
				{
					$sqlQuery .= " OR (SectionID = " . $row['SectionID'] . ")"; //Adds this once $i has counted over 1
				}
				
				$i++; //$i + 1
			}
			$sqlQuery .= ") ";
		}
		else //No Rows Returned
		{
			$sqlQuery .= "SectionID = " . $selected_section . " ";
		}
		
		//Check if there is a selected user
		if($selected_user > 0)
		{
			$sqlQuery .= "AND UserID = " . $selected_user . " ";
		}

//		previously only shifts of active users would be displayed		
//		$sqlQuery .= " AND IsActive = '1'";
		$sqlQuery .= "ORDER BY ShiftFrom DESC";

		//If $start_date and $end_date are not empty
		if($start_date != "//" && $end_date != "/1/")
		{
			$noRows = "<table width=\"100%\">\n"
			        . "<tr>\n"
			        . "<td>No Shifts where found for dates between <strong>" . $start_date . "</strong> and <strong>" . $end_date . "</strong>.</td>\n"
			        . "</tr>\n"
			        . "</table>";
		}
		else //Not Empty
		{
			$noRows = "<table width=\"100%\">\n"
			        . "<tr>\n"
			        . "<td>No Shifts where found, please refine your search.</td>\n"
			        . "</tr>\n"
			        . "</table>";
		}
	}
	else //No Rows Returned
	{
		$sqlQuery = "SELECT DATEPART(mm, ShiftFrom) AS ShiftMonth, * " 
		          . "FROM vAppShift ";
		
		//If $start_date and $end_date are not empty  
		if($start_date != "//" && $end_date != "/1/")
		{
			$sqlQuery .= "WHERE (ShiftFrom >= '" . $start_date . "') AND (ShiftTo <= '" . $end_date . "') AND ";
		}
		else
		{
		 	$sqlQuery .= "WHERE ";
		}
		
		$sqlQuery .= "UserID = " . $userID . " "
		           . "ORDER BY ShiftFrom DESC";

		$noRows = "<table width=\"100%\">\n"
		        . "<tr>\n"
		        . "<td>You have no Shifts for dates between <strong>" . $start_date . "</strong> and <strong>" . $end_date . "</strong>.</td>\n"
		        . "</tr>\n"
		        . "</table>";
	}
	
	$rs = $db->Execute($sqlQuery);
	
	//If Rows are Returned
	if($rs->RowCount()) {
				
			$totalMonthlySectionHours 	= array();
			$totalMonthlySectionMins 	= array();
			
			$totalMonthlyHours 			= 0;
			$totalMonthlyMins 			= 0;

			$grandTotalSectionHours 	= array();
			$grandTotalSectionMins 		= array();
			
			$grandtotalHours 			= 0;
			$grandtotalMins 			= 0;
			
			$endFlag 					= 0;
			
			// ********************
			// WHILE RECORDS EXIST
			// ********************
			while ($row = $rs->FetchRow()) {
				
				if (strtotime(date("m/d/Y", strtotime($row['ShiftFrom']))) == strtotime($startDate)) {
					$rowClass = " class=\"today\" ";

				} else {
					$rowClass = "";
				}
	
	
				// *********************
				// IF MONTH HAS CHANGED
				// *********************
				if ($loopMonth != $row['ShiftMonth']) {
					
					// ************************
					// AND NOT THE FIRST MONTH
					// ************************
					if ($loopMonth != 0) {
												
						foreach ($totalMonthlySectionHours as $key => $value) {
						
						// count how many sections are to be totaled.
						//$count = count($grandTotalSectionHours);
					
						// then display totals for each section for that month.
						//for($i=0; $i < $count; $i++) {
						
							// Echo total row
							echo "<tr>\n"
							   . "<td colspan=4><strong>Total for ".sectionName($key).":</strong></td>\n"
							   . "<td colspan=\"2\" align=\"right\"><strong>" . $totalMonthlySectionHours[$key] . "h " . $totalMonthlySectionMins[$key] . "m</strong></td>\n"
							   . "<td>&nbsp;</td>\n"
							   . "</tr>\n";
							   
						}


						echo "<tr>\n"
						   . "<td colspan=4><strong>Grand total for the month:</strong></td>\n"
						   . "<td colspan=\"2\" align=\"right\"><strong>" . $totalMonthlyHours . "h " . $totalMonthlyMins . "m</strong></td>\n"
						   . "<td>&nbsp;</td>\n"
						   . "</tr>\n"
						   . "</tbody>\n"
						   . "</table><br />\n";
						   

						// reset monthly variables
						unset($totalMonthlySectionHours);
						unset($totalMonthlySectionMins);
						$totalMonthlyHours = 0;
						$totalMonthlyMins = 0;

					} // end monthly totals
					
					// set loop month
					$loopMonth = $row['ShiftMonth'];
	
	
	
					// *************************************
					// PRINT COLUMN TITLES FOR MONTHLY DATA
					// *************************************
					echo "<table width=\"100%\">\n"
					   . "<tr>\n"
					   . "<td><h1>" . date("F Y", strtotime($row['ShiftFrom'])) . "</h1></td>\n"
					   . "</tr>\n"
					   . "</table>\n"
					   . "<table cellspacing=\"0\" border=\"0\" width=\"100%\" id=\"shiftTable\">\n"
					   . "<thead>\n"
					   . "<tr>\n"
					   . "<th>Volunteer</th>\n"
					   . "<th>Date</th>\n"
					   . "<th>Start Time</th>\n"
					   . "<th>End Time</th>\n"
					   . "<th>Section</th>\n"
					   . "<th>Duration</th>\n"
					   . "<th>&nbsp;</th>\n"
					   . "</tr>\n"
					   . "</thead>\n"
					   . "<tbody>\n";
				}
				
				
				
				// *********************************
				// BEGIN PRINTING SHIFTS ROW BY ROW
				// *********************************
				echo "<tr $rowClass>";
				
				// bold currently logged in users names.
				if($row['UserID'] == $current_userID) {
					echo "<td width=\"100px\"><strong>" . $row['FirstName'] . " " . $row['LastName'] . "</strong></td>"; //Added by Brandon, shows Volunteer First and Last Name
				
				// if user was specifically requested, don't link users name
				} elseif($row['UserID'] == $requested_userID){
					echo "<td width=\"100px\">" . $row['FirstName'] . " " . $row['LastName'] . "</td>"; //Added by Brandon, shows Volunteer First and Last Name

				// otherwise don't bold users names.
				} else {
					echo "<td width=\"100px\"><a href=\"shifts.php?type=person&userid=" . $row['UserID'] . "\" title=\"View " . $row['FirstName'] . " " . $row['LastName'] . " Shifts\">" . $row['FirstName'] . " " . $row['LastName'] . "</a></td>\n"; //Added by Brandon, shows Volunteer First and Last Name
				}
							
				// If the ShiftDurationHours comes back a negative number, add 24 to offset it
				if(strpbrk($row['ShiftDurationHours'], "-")) {	
					$totaltoRemove = $row['ShiftDurationHours'] * 60;				
					$row['ShiftDurationHours'] = $row['ShiftDurationHours'] + 24;
					$row['ShiftDurationMinutes'] = $row['ShiftDurationMinutes'] - $totaltoRemove;
				} else {	
					$totaltoRemove = $row['ShiftDurationHours'] * 60;
					$row['ShiftDurationHours'] = $row['ShiftDurationHours'];
					$row['ShiftDurationMinutes'] = $row['ShiftDurationMinutes'] - $totaltoRemove;
				}
				
				// Check if minutes are negative and adjust to be positive values if required
				if(strpbrk($row['ShiftDurationMinutes'], "-")){	
					$row['ShiftDurationMinutes'] = $row['ShiftDurationMinutes'] + 60;
					$row['ShiftDurationHours'] = $row['ShiftDurationHours'] - 1;
				}	
				
				// echo shift date, start and end times, section name, total hours and minutes, and a link to get shift details
				echo "<td width=\"100px\">" . date("D m/d/Y", strtotime($row['ShiftFrom'])) . "</td>\n"
				   . "<td width=\"75px\">" . date("g:i A", strtotime($row['ShiftFrom'])) . "</td>\n"
				   . "<td width=\"75px\">" . date("g:i A", strtotime($row['ShiftTo'])) . "</td>\n"
				   . "<td width=\"auto\">" . $row['SectionName'] . "</td>\n"
				   . "<td width=\"75px\" align=\"right\">" . $row['ShiftDurationHours'] . "h " . $row['ShiftDurationMinutes'] . "m</td>\n"
				   . "<td width=\"60px\" align=\"right\"><a href=\"javascript:popDetails('" . $row['ShiftID'] . "')\">Details</a></td>\n"
				   . "</tr>";	
				   
				
				// **************
				// UPDATE TOTALS
				// **************			
				
				// update total monthly hours by section
				$totalMonthlySectionHours[$row['SectionID']] += $row['ShiftDurationHours'];
				$totalMonthlySectionMins[$row['SectionID']] += $row['ShiftDurationMinutes'];

				// update total monthly hours
				$totalMonthlyHours += $row['ShiftDurationHours'];
				$totalMonthlyMins += $row['ShiftDurationMinutes'];

				// update grand total by section
				$grandTotalSectionHours[$row['SectionID']] += $row['ShiftDurationHours'];
				$grandTotalSectionMins[$row['SectionID']] += $row['ShiftDurationMinutes'];
				
				// update grand total
				$grandtotalHours += $row['ShiftDurationHours'];
				$grandtotalMins += $row['ShiftDurationMinutes'];

	
				// *************************************
				// TURN MINUTES INTO HOURS, IF REQUIRED
				// *************************************
				
				// monthly totals by section
				if($totalMonthlySectionMins[$row['SectionID']] >= 60) {
					$totalMonthlySectionHours[$row['SectionID']]++;
					$totalMonthlySectionMins[$row['SectionID']] -= 60;
				}				
				// monthly totals
				if($totalMonthlyMins >= 60) {
					$totalMonthlyHours++;
					$totalMonthlyMins -= 60;
				}				
				// grand total by section
				if($grandTotalSectionMins[$row['SectionID']] >= 60) {
					$grandTotalSectionHours[$row['SectionID']]++;
					$grandTotalSectionMins[$row['SectionID']] -= 60;
				}				
				// grand total
				if($grandtotalMins >= 60) {
					$grandtotalHours++;
					$grandtotalMins -= 60;
				}
				
				$endFlag++;
				
			} // on to next row
			
			
	
			if($endFlag != 0) {
				
				foreach ($totalMonthlySectionHours as $key => $value) {
			
					// Echo total row
					echo "<tr>\n"
					   . "<td colspan=4><strong>Total for ".sectionName($key).":</strong></td>\n"
					   . "<td colspan=\"2\" align=\"right\"><strong>" . $totalMonthlySectionHours[$key] . "h " . $totalMonthlySectionMins[$key] . "m</strong></td>\n"
					   . "<td>&nbsp;</td>\n"
					   . "</tr>\n";
					   
				}
				
				echo "<tr>\n"
				   . "<td colspan=4><strong>Grand total for the month:</strong></td>\n"
				   . "<td colspan=\"2\" align=\"right\"><strong>" . $totalMonthlyHours . "h " . $totalMonthlyMins . "m</strong></td>\n"
				   . "<td>&nbsp;</td>\n"
				   . "</tr>\n"
				   . "</tbody>\n"
				   . "</table><br />\n";
				   
				
				// reset monthly variables
				unset($totalMonthlySectionHours);
				unset($totalMonthlySectionMins);
				$totalMonthlyHours = 0;
				$totalMonthlyMins = 0;	
			
			}
		
			
			echo "<table class=\"GrandTotals\" width=\"100%\">\n";
			
			foreach ($grandTotalSectionHours as $key => $value) {
				   
				// Echo total row
				echo "<tr>\n"
				   . "<td><strong>Grand total for ".sectionName($key).":</strong></td>\n"
				   . "<td align=\"right\"><strong>" . $grandTotalSectionHours[$key] . "h " . $grandTotalSectionMins[$key] . "m</strong></td>\n"
				   . "</tr>\n";
				   
			}
			
			echo "<tr>\n"
			   . "<td><strong>Overall grand total for results:</strong></td>\n"
			   . "<td align=\"right\"><strong>" . $grandtotalHours . "h " . $grandtotalMins . "m</strong></td>\n"
			   . "</tr>\n"
			   . "</table>\n";

				   
			echo "</div>\n";
			
	}
	else //No Rows Returned
	{
		echo $noRows;
	}
	
	$db->Close();
	
}







function printShiftByNameFilter($userID, $filter)
{
	global $db;
	connectDB();
		  
	$sqlQuery	= "SELECT FirstName, LastName, UserID "
	            . "FROM vAppAdmin "
	            . "WHERE ((FirstName Like '%$filter%') OR (LastName Like '%$filter%'))";
	$rs = $db->Execute($sqlQuery);
	
	//If Rows are Returned
	if ($rs->RowCount()) 
	{
		$i = 0; //Define $i as 0
		
		//While Rows are Returned
		while($row = $rs->FetchRow())
		{	
			$filteredUsers[$i] = $row['UserID'];
			$i++;
		} 
	}
	else //No Rows Returned
	{
		echo "<table width=\"100%\">\n"
		   . "<tr>\n"
		   . "<td>No Volunteers where found that have Upcoming or Past Shifts.<br /><br />You searched for: <strong>'" . $filter . "'</strong>.</td>\n"
		   . "</tr>\n"
		   . "</table>";
	}
	
	$filteredUsers = array_unique($filteredUsers);
	$filteredUsers_Count = count($filteredUsers);
	
	foreach($filteredUsers as $k => $v)
	{
		printShiftsByUser($userID, $v, 1);
	}

   $db->Close();
}









function printShiftsByUser($current_userID, $requested_userID, $filtered)
{
	global $db;
	connectDB();
	
	$currentMonth = date("m");
	$loopMonth = 0;
			
	$sqlQuery = "SELECT datepart(mm, ShiftFrom) AS 'ShiftMonth', * " 
			  . "FROM vAppShift "
	          . "WHERE DATEPART(mm, ShiftFrom) <= $currentMonth "
	          . "AND UserID = " . $requested_userID . " "
	          . "ORDER BY ShiftFrom DESC";
	$rs = $db->Execute($sqlQuery);
	
	//If Rows are Returned
	if ($rs->RowCount()) {
		
		
		//$totalHours = 0;
		//$totalMins = 0;
		
		//$grandtotalHours = 0;
		//$grandtotalMins = 0;
		
		
		$totalMonthlySectionHours 	= array();
		$totalMonthlySectionMins 	= array();
		
		$totalMonthlyHours 			= 0;
		$totalMonthlyMins 			= 0;

		$grandTotalSectionHours 	= array();
		$grandTotalSectionMins 		= array();
		
		$grandtotalHours 			= 0;
		$grandtotalMins 			= 0;
			
		$endFlag 					= 0;
		
		//While Rows are Returned
		while ($row = $rs->FetchRow()){
			
			if (strtotime(date("m/d/Y", strtotime($row['ShiftFrom']))) == strtotime($startDate)) 
			{
				$rowClass = " class=\"today\" ";
			}
			else 
			{
				$rowClass = "";
			}



			// *********************
			// IF MONTH HAS CHANGED
			// *********************
			if ($loopMonth != $row['ShiftMonth']) {
				
				// ************************
				// AND NOT THE FIRST MONTH
				// ************************
				if ($loopMonth != 0) {
											
					foreach ($totalMonthlySectionHours as $key => $value) {
					
					// count how many sections are to be totaled.
					//$count = count($grandTotalSectionHours);
				
					// then display totals for each section for that month.
					//for($i=0; $i < $count; $i++) {
					
						// Echo total row
						echo "<tr>\n"
						   . "<td colspan=4><strong>Total for ".sectionName($key).":</strong></td>\n"
						   . "<td colspan=\"2\" align=\"right\"><strong>" . $totalMonthlySectionHours[$key] . "h " . $totalMonthlySectionMins[$key] . "m</strong></td>\n"
						   . "<td>&nbsp;</td>\n"
						   . "</tr>\n";
						   
					}


					echo "<tr>\n"
					   . "<td colspan=4><strong>Grand total for the month:</strong></td>\n"
					   . "<td colspan=\"2\" align=\"right\"><strong>" . $totalMonthlyHours . "h " . $totalMonthlyMins . "m</strong></td>\n"
					   . "<td>&nbsp;</td>\n"
					   . "</tr>\n"
					   . "</tbody>\n"
					   . "</table><br />\n";
					   

					// reset monthly variables
					unset($totalMonthlySectionHours);
					unset($totalMonthlySectionMins);
					$totalMonthlyHours = 0;
					$totalMonthlyMins = 0;

				} // end monthly totals
				
				

				// set loop month
				$loopMonth = $row['ShiftMonth'];



				// *************************************
				// PRINT COLUMN TITLES FOR MONTHLY DATA
				// *************************************
				echo "<table width=\"100%\">\n"
				   . "<tr>\n"
				   . "<td><h1>" . date("F Y", strtotime($row['ShiftFrom'])) . "</h1></td>\n"
				   . "</tr>\n"
				   . "</table>\n"
				   . "<table cellspacing=\"0\" border=\"0\" width=\"100%\" id=\"shiftTable\">\n"
				   . "<thead>\n"
				   . "<tr>\n"
				   . "<th>Volunteer</th>\n"
				   . "<th>Date</th>\n"
				   . "<th>Start Time</th>\n"
				   . "<th>End Time</th>\n"
				   . "<th>Section</th>\n"
				   . "<th>Duration</th>\n"
				   . "<th>&nbsp;</th>\n"
				   . "</tr>\n"
				   . "</thead>\n"
				   . "<tbody>\n";
			}
			
			
			
			// *********************************
			// BEGIN PRINTING SHIFTS ROW BY ROW
			// *********************************				
			echo "<tr $rowClass>";
					
			if($row['UserID'] == $current_userID)
			{
				echo "<td width=\"100px\"><strong>" . $row['FirstName'] . " " . $row['LastName'] . "</strong></td>"; //Added by Brandon, shows Volunteer First and Last Name
			}
			elseif($row['UserID'] == $requested_userID)
			{
				echo "<td width=\"100px\">" . $row['FirstName'] . " " . $row['LastName'] . "</td>"; //Added by Brandon, shows Volunteer First and Last Name
			}
			else
			{
				echo "<td width=\"100px\"><a href=\"shifts.php?type=person&userid=" . $row['UserID'] . "\" title=\"View " . $row['FirstName'] . " " . $row['LastName'] . " Shifts\">" . $row['FirstName'] . " " . $row['LastName'] . "</a></td>\n"; //Added by Brandon, shows Volunteer First and Last Name
			}
			
			
						
			// If the ShiftDurationHours comes back a negative number, add 24 to offset it
			if(strpbrk($row['ShiftDurationHours'], "-")) {	
				$totaltoRemove = $row['ShiftDurationHours'] * 60;				
				$row['ShiftDurationHours'] = $row['ShiftDurationHours'] + 24;
				$row['ShiftDurationMinutes'] = $row['ShiftDurationMinutes'] - $totaltoRemove;
			} else {	
				$totaltoRemove = $row['ShiftDurationHours'] * 60;
				$row['ShiftDurationHours'] = $row['ShiftDurationHours'];
				$row['ShiftDurationMinutes'] = $row['ShiftDurationMinutes'] - $totaltoRemove;
			}
			
			// Check if minutes are negative and adjust to be positive values if required
			if(strpbrk($row['ShiftDurationMinutes'], "-")){	
				$row['ShiftDurationMinutes'] = $row['ShiftDurationMinutes'] + 60;
				$row['ShiftDurationHours'] = $row['ShiftDurationHours'] - 1;
			}	


			// echo shift date, start and end times, section name, total hours and minutes, and a link to get shift details
			echo "<td width=\"100px\">" . date("D m/d/Y", strtotime($row['ShiftFrom'])) . "</td>\n"
			   . "<td width=\"75px\">" . date("g:i A", strtotime($row['ShiftFrom'])) . "</td>\n"
			   . "<td width=\"75px\">" . date("g:i A", strtotime($row['ShiftTo'])) . "</td>\n"
			   . "<td width=\"auto\">" . $row['SectionName'] . "</td>\n"
			   . "<td width=\"75px\" align=\"right\">" . $row['ShiftDurationHours'] . "h " . $row['ShiftDurationMinutes'] . "m</td>\n"
			   . "<td width=\"60px\" align=\"right\"><a href=\"javascript:popDetails('" . $row['ShiftID'] . "')\">Details</a></td>\n"
			   . "</tr>";	
					



			
			// **************
			// UPDATE TOTALS
			// **************			
			
			// update total monthly hours by section
			$totalMonthlySectionHours[$row['SectionID']] += $row['ShiftDurationHours'];
			$totalMonthlySectionMins[$row['SectionID']] += $row['ShiftDurationMinutes'];

			// update total monthly hours
			$totalMonthlyHours += $row['ShiftDurationHours'];
			$totalMonthlyMins += $row['ShiftDurationMinutes'];

			// update grand total by section
			$grandTotalSectionHours[$row['SectionID']] += $row['ShiftDurationHours'];
			$grandTotalSectionMins[$row['SectionID']] += $row['ShiftDurationMinutes'];
			
			// update grand total
			$grandtotalHours += $row['ShiftDurationHours'];
			$grandtotalMins += $row['ShiftDurationMinutes'];


			// *************************************
			// TURN MINUTES INTO HOURS, IF REQUIRED
			// *************************************
			
			// monthly totals by section
			if($totalMonthlySectionMins[$row['SectionID']] >= 60) {
				$totalMonthlySectionHours[$row['SectionID']]++;
				$totalMonthlySectionMins[$row['SectionID']] -= 60;
			}				
			// monthly totals
			if($totalMonthlyMins >= 60) {
				$totalMonthlyHours++;
				$totalMonthlyMins -= 60;
			}				
			// grand total by section
			if($grandTotalSectionMins[$row['SectionID']] >= 60) {
				$grandTotalSectionHours[$row['SectionID']]++;
				$grandTotalSectionMins[$row['SectionID']] -= 60;
			}				
			// grand total
			if($grandtotalMins >= 60) {
				$grandtotalHours++;
				$grandtotalMins -= 60;
			}
			
			$endFlag++;
					
		} // on to next row


		if($endFlag != 0) {
			
			foreach ($totalMonthlySectionHours as $key => $value) {
		
				// Echo total row
				echo "<tr>\n"
				   . "<td colspan=4><strong>Total for ".sectionName($key).":</strong></td>\n"
				   . "<td colspan=\"2\" align=\"right\"><strong>" . $totalMonthlySectionHours[$key] . "h " . $totalMonthlySectionMins[$key] . "m</strong></td>\n"
				   . "<td>&nbsp;</td>\n"
				   . "</tr>\n";
				   
			}
			
			echo "<tr>\n"
			   . "<td colspan=4><strong>Grand total for the month:</strong></td>\n"
			   . "<td colspan=\"2\" align=\"right\"><strong>" . $totalMonthlyHours . "h " . $totalMonthlyMins . "m</strong></td>\n"
			   . "<td>&nbsp;</td>\n"
			   . "</tr>\n"
			   . "</tbody>\n"
			   . "</table><br />\n";
			   
			
			// reset monthly variables
			unset($totalMonthlySectionHours);
			unset($totalMonthlySectionMins);
			$totalMonthlyHours = 0;
			$totalMonthlyMins = 0;	
		
		}
	
		
		echo "<table class=\"GrandTotals\" width=\"100%\">\n";
		
		foreach ($grandTotalSectionHours as $key => $value) {
			   
			// Echo total row
			echo "<tr>\n"
			   . "<td><strong>Grand total for ".sectionName($key).":</strong></td>\n"
			   . "<td align=\"right\"><strong>" . $grandTotalSectionHours[$key] . "h " . $grandTotalSectionMins[$key] . "m</strong></td>\n"
			   . "</tr>\n";
			   
		}
		
		echo "<tr>\n"
		   . "<td><strong>Overall grand total for results:</strong></td>\n"
		   . "<td align=\"right\"><strong>" . $grandtotalHours . "h " . $grandtotalMins . "m</strong></td>\n"
		   . "</tr>\n"
		   . "</table>\n";

			   
		echo "</div>\n";
		
	}
	else //No Rows Returned
	{
		if($current_userID != $requested_userID)
		{
			if($filtered == 0)
			{
				echo "<table width=\"100%\">\n"
				   . "<tr>\n"
				   . "<td>Selected Users currently has no Upcoming or Past Shifts.</td>\n"
				   . "</tr>\n"
				   . "</table>";
			}
		}
		else
		{
			if($filtered == 0)
			{
				echo "<table width=\"100%\">\n"
				   . "<tr>\n"
				   . "<td>You currently have no Upcoming or Past Shifts.</td>\n"
				   . "</tr>\n"
				   . "</table>";
			}
		}
	}
	
	$db->Close();
	
} //Function End










function printShiftsBySection($userID, $sectionID, $type)
{
	global $db;
	connectDB();
	
	$sqlQuery = "SELECT * "
	          . "FROM vAppAdmin "
	          . "WHERE UserID = " . $userID . " "
			  . "AND IsAdmin = 1";			
	$rs = $db->Execute($sqlQuery);
	
	//If Rows are Returned
	if ($rs->RowCount()) 
	{
		$currentMonth = date("m");
		$loopMonth = 0;

		if($type == "me")
		{
			$sqlQuery = "SELECT datepart(mm, ShiftFrom) AS ShiftMonth, * "
					  . "FROM vAppShift "
					  . "WHERE DATEPART(mm, ShiftFrom) <= " . $currentMonth . " "
					  . "AND SectionID = " . $sectionID . " "
					  . "AND UserID = " . $userID . " "
//					  . "AND IsActive = '1' "
					  . "ORDER BY ShiftFrom DESC";

			print "DB CALL ["  . $sqlQuery . "]";		  
					  
			$noRows = "<table width=\"100%\">\n"
			        . "<tr>\n"
			        . "<td>You currently have no Shifts in this Section.<td>\n"
			        . "<tr>\n"
			        . "</table>";
		}
		
		if($type == "category")
		{
			$sqlQuery = "SELECT datepart(mm, ShiftFrom) AS ShiftMonth, * "
					  . "FROM vAppShift "
					   . "WHERE DATEPART(mm, ShiftFrom) <= " . $currentMonth . " "
					  . "AND SectionID = " . $sectionID . " "
//					  . "AND IsActive = '1' "
					  . "ORDER BY ShiftFrom DESC";
					  
			$noRows = "<table width=\"100%\">\n"
			        . "<tr>\n"
			        . "<td>This Section currently has no Shift.<td>\n"
			        . "<tr>\n"
			        . "</table>";
		}
		$rs = $db->Execute($sqlQuery);
		
		//If Rows are Returned
		if ($rs->RowCount()) 
		{
			$totalHours = 0;
			$totalMins = 0;
			
			$grandtotalHours = 0;
			$grandtotalMins = 0;
			
			//While Rows are Returned
			while ($row = $rs->FetchRow())		
			{
				if (strtotime(date("m/d/Y", strtotime($row['ShiftFrom']))) == strtotime($startDate)) 
				{
					$rowClass = " class=\"today\" ";
				}
				else 
				{
					$rowClass = "";
				}
	
				if ($loopMonth != $row['ShiftMonth']) 
				{	
					if ($loopMonth != 0) 
					{			
						echo "<tr>\n"
						   . "<td>&nbsp;</td>\n"
						   . "<td>&nbsp;</td>\n"
						   . "<td>&nbsp;</td>\n"
						   . "<td>&nbsp;</td>\n"
						   . "<td colspan=\"2\" align=\"right\"><strong>Total: " . $totalHours . "h " . $totalMins . "m</strong></td>\n"
						   . "<td>&nbsp;</td>\n"
						   . "</tr>\n"
						   . "</tbody>\n"
						   . "</table><br />\n";
							
						$grandtotalHours = $grandtotalHours + $totalHours;
						$grandtotalMins = $grandtotalMins + $totalMins;
						
						$totalHours = 0;
						$totalMins = 0;			
					}
					
					$loopMonth = $row['ShiftMonth'];
	
					echo "<table width=\"100%\">\n"
					   . "<tr>\n"
					   . "<td><h1>" . date("F Y", strtotime($row['ShiftFrom'])) . "</h1></td>\n"
					   . "</tr>\n"
					   . "</table>\n"
					   . "<table cellspacing=\"0\" border=\"0\" width=\"100%\" id=\"shiftTable\">\n"
					   . "<thead>\n"
					   . "<tr>\n"
					   . "<th>Volunteer</th>\n"
					   . "<th>Date</th>\n"
					   . "<th>Start Time</th>\n"
					   . "<th>End Time</th>\n"
					   . "<th>Section</th>\n"
					   . "<th>Duration</th>\n"
					   . "<th>&nbsp;</th>\n"
					   . "</tr>\n"
					   . "</thead>\n"
					   . "<tbody>\n";
				}
	
				echo "<tr $rowClass>";
						
				if($row['UserID'] == $current_userID)
				{
					echo "<td width=\"100px\"><strong>" . $row['FirstName'] . " " . $row['LastName'] . "</strong></td>"; //Added by Brandon, shows Volunteer First and Last Name
				}
				elseif($row['UserID'] == $requested_userID)
				{
					echo "<td width=\"100px\">" . $row['FirstName'] . " " . $row['LastName'] . "</td>"; //Added by Brandon, shows Volunteer First and Last Name
				}
				else
				{
					echo "<td width=\"100px\"><a href=\"shifts.php?type=person&userid=" . $row['UserID'] . "\" title=\"View " . $row['FirstName'] . " " . $row['LastName'] . " Shifts\">" . $row['FirstName'] . " " . $row['LastName'] . "</a></td>\n"; //Added by Brandon, shows Volunteer First and Last Name
				}
				
				//If the ShiftDurationHours comes back a negative number, add 24 to offset it
				if(strpbrk($row['ShiftDurationHours'], "-"))
				{	
					$totaltoRemove = $row['ShiftDurationHours'] * 60;
					
					$row['ShiftDurationHours'] = $row['ShiftDurationHours'] + 24;
					$row['ShiftDurationMinutes'] = $row['ShiftDurationMinutes'] - $totaltoRemove;
				}
				else
				{	
					$totaltoRemove = $row['ShiftDurationHours'] * 60;
					
					$row['ShiftDurationHours'] = $row['ShiftDurationHours'];
					$row['ShiftDurationMinutes'] = $row['ShiftDurationMinutes'] - $totaltoRemove;
				}
				
				//Check if minutes are negative
				if(strpbrk($row['ShiftDurationMinutes'], "-"))
				{	
					$row['ShiftDurationMinutes'] = $row['ShiftDurationMinutes'] + 60;
					$row['ShiftDurationHours'] = $row['ShiftDurationHours'] - 1;
				}
			
				echo "<td width=\"100px\">" . date("D m/d/Y", strtotime($row['ShiftFrom'])) . "</td>\n"
				   . "<td width=\"75px\">" . date("g:i A", strtotime($row['ShiftFrom'])) . "</td>\n"
				   . "<td width=\"75px\">" . date("g:i A", strtotime($row['ShiftTo'])) . "</td>\n"
				   . "<td width=\"auto\">" . $row['SectionName'] . "</td>\n"
				   . "<td width=\"75px\" align=\"right\">" . $row['ShiftDurationHours'] . "h " . $row['ShiftDurationMinutes'] . "m</td>\n"
				   . "<td width=\"60px\" align=\"right\"><a href=\"javascript:popDetails('" . $row['ShiftID'] . "')\">Details</a></td>\n"
				   . "</tr>";	
							
				$totalHours = $totalHours + $row['ShiftDurationHours'];
				$totalMins = $totalMins + $row['ShiftDurationMinutes'];
	
				if($totalMins >= 60)
				{
					$totalHours = $totalHours + 1;
					$totalMins = $totalMins - 60;
				}
			}
			
			$totalHours = $totalHours + $row['ShiftDurationHours'];
			$totalMins = $totalMins + $row['ShiftDurationMinutes'];	
			
			$grandtotalHours = $grandtotalHours + $totalHours;
			$grandtotalMins = $grandtotalMins + $totalMins;
			
			echo "<tr>\n"
			   . "<td>&nbsp;</td>\n"
			   . "<td>&nbsp;</td>\n"
			   . "<td>&nbsp;</td>\n"
			   . "<td>&nbsp;</td>\n"
			   . "<td colspan=\"2\" align=\"right\"><strong>Total: " . $totalHours . "h " . $totalMins . "m</strong></td>\n"
			   . "<td>&nbsp;</td>\n"
			   . "</tr>\n"
			   . "<tr>\n"
			   . "<td>&nbsp;</td>\n"
			   . "<td>&nbsp;</td>\n"
			   . "<td>&nbsp;</td>\n"
			   . "<td>&nbsp;</td>\n"
			   . "<td colspan=\"2\" align=\"right\"><strong>Grand Total: " . $grandtotalHours . "h " . $grandtotalMins . "m</strong></td>\n"
			   . "<td>&nbsp;</td>\n"
			   . "</tr>\n"
			   . "</tbody>\n"
			   . "</table>\n"
			   . "</div>\n";
		}
		else //No rows returned
		{
			echo $noRows;
		}
	}
	else //No Rows Returned
	{
		$currentMonth = date("m");
		$loopMonth = 0;
		
		if($type == "me")
		{
			$sqlQuery = "SELECT datepart(mm, ShiftFrom) AS ShiftMonth, * "
					  . "FROM vAppShift "
					  . "WHERE DATEPART(mm, ShiftFrom) <= " . $currentMonth . " "
					  . "AND SectionID = " . $sectionID . " "
					  . "AND UserID = " . $userID . " "
					  . "ORDER BY ShiftFrom DESC";
					  
			$noRows = "<table width=\"100%\"><tr><td>You currently have no Shifts in this Section.<td><tr></table>";
		}
		
		if($type == "category")
		{
			$sqlQuery = "SELECT datepart(mm, ShiftFrom) AS ShiftMonth, * "
					  . "FROM vAppShift "
					  . "WHERE DATEPART(mm, ShiftFrom) <= " . $currentMonth . " "
					  . "AND SectionID = " . $sectionID . " "
					  . "ORDER BY ShiftFrom DESC";
					  
			$noRows = "<table width=\"100%\"><tr><td>This Section currently has no Shift.<td><tr></table>";
		}
		$rs = $db->Execute($sqlQuery);
		
		//If Rows are Returned
		if($rs->RowCount())
		{
			$totalHours = 0;
			$totalMins = 0;
			
			$grandtotalHours = 0;
			$grandtotalMins = 0;
			
			//While Rows are Returned
			while ($row = $rs->FetchRow())		
			{
				if (strtotime(date("m/d/Y", strtotime($row['ShiftFrom']))) == strtotime($startDate)) 
				{
					$rowClass = " class=\"today\" ";
				}
				else 
				{
					$rowClass = "";
				}
	
				if ($loopMonth != $row['ShiftMonth']) 
				{	
					if ($loopMonth != 0) 
					{			
						echo "<tr>\n"
						   . "<td>&nbsp;</td>\n"
						   . "<td>&nbsp;</td>\n"
						   . "<td>&nbsp;</td>\n"
						   . "<td>&nbsp;</td>\n"
						   . "<td colspan=\"2\" align=\"right\"><strong>Total: " . $totalHours . "h " . $totalMins . "m</strong></td>\n"
						   . "<td>&nbsp;</td>\n"
						   . "</tr>\n"
						   . "</tbody>\n"
						   . "</table><br />\n";
							
						$grandtotalHours = $grandtotalHours + $totalHours;
						$grandtotalMins = $grandtotalMins + $totalMins;
						
						$totalHours = 0;
						$totalMins = 0;			
					}
					
					$loopMonth = $row['ShiftMonth'];
	
					echo "<table width=\"100%\">\n"
					   . "<tr>\n"
					   . "<td><h1>" . date("F Y", strtotime($row['ShiftFrom'])) . "</h1></td>\n"
					   . "</tr>\n"
					   . "</table>\n"
					   . "<table cellspacing=\"0\" border=\"0\" width=\"100%\" id=\"shiftTable\">\n"
					   . "<thead>\n"
					   . "<tr>\n"
					   . "<th>Volunteer</th>\n"
					   . "<th>Date</th>\n"
					   . "<th>Start Time</th>\n"
					   . "<th>End Time</th>\n"
					   . "<th>Section</th>\n"
					   . "<th>Duration</th>\n"
					   . "<th>&nbsp;</th>\n"
					   . "</tr>\n"
					   . "</thead>\n"
					   . "<tbody>\n";
				}
	
				echo "<tr $rowClass>";
						
				if($row['UserID'] == $current_userID)
				{
					echo "<td width=\"100px\"><strong>" . $row['FirstName'] . " " . $row['LastName'] . "</strong></td>"; //Added by Brandon, shows Volunteer First and Last Name
				}
				elseif($row['UserID'] == $requested_userID)
				{
					echo "<td width=\"100px\">" . $row['FirstName'] . " " . $row['LastName'] . "</td>"; //Added by Brandon, shows Volunteer First and Last Name
				}
				else
				{
					echo "<td width=\"100px\"><a href=\"shifts.php?type=person&userid=" . $row['UserID'] . "\" title=\"View " . $row['FirstName'] . " " . $row['LastName'] . " Shifts\">" . $row['FirstName'] . " " . $row['LastName'] . "</a></td>\n"; //Added by Brandon, shows Volunteer First and Last Name
				}
				
				//If the ShiftDurationHours comes back a negative number, add 24 to offset it
				if(strpbrk($row['ShiftDurationHours'], "-"))
				{	
					$totaltoRemove = $row['ShiftDurationHours'] * 60;
					
					$row['ShiftDurationHours'] = $row['ShiftDurationHours'] + 24;
					$row['ShiftDurationMinutes'] = $row['ShiftDurationMinutes'] - $totaltoRemove;
				}
				else
				{	
					$totaltoRemove = $row['ShiftDurationHours'] * 60;
					
					$row['ShiftDurationHours'] = $row['ShiftDurationHours'];
					$row['ShiftDurationMinutes'] = $row['ShiftDurationMinutes'] - $totaltoRemove;
				}
			
				echo "<td width=\"100px\">" . date("D m/d/Y", strtotime($row['ShiftFrom'])) . "</td>\n"
				   . "<td width=\"75px\">" . date("g:i A", strtotime($row['ShiftFrom'])) . "</td>\n"
				   . "<td width=\"75px\">" . date("g:i A", strtotime($row['ShiftTo'])) . "</td>\n"
				   . "<td width=\"auto\">" . $row['SectionName'] . "</td>\n"
				   . "<td width=\"75px\" align=\"right\">" . $row['ShiftDurationHours'] . "h " . $row['ShiftDurationMinutes'] . "m</td>\n"
				   . "<td width=\"60px\" align=\"right\"><a href=\"javascript:popDetails('" . $row['ShiftID'] . "')\">Details</a></td>\n"
				   . "</tr>";	
							
				$totalHours = $totalHours + $row['ShiftDurationHours'];
				$totalMins = $totalMins + $row['ShiftDurationMinutes'];
	
				if($totalMins >= 60)
				{
					$totalHours = $totalHours + 1;
					$totalMins = $totalMins - 60;
				}
			}
			
			$totalHours = $totalHours + $row['ShiftDurationHours'];
			$totalMins = $totalMins + $row['ShiftDurationMinutes'];	
			
			$grandtotalHours = $grandtotalHours + $totalHours;
			$grandtotalMins = $grandtotalMins + $totalMins;
			
			echo "<tr>\n"
			   . "<td>&nbsp;</td>\n"
			   . "<td>&nbsp;</td>\n"
			   . "<td>&nbsp;</td>\n"
			   . "<td>&nbsp;</td>\n"
			   . "<td colspan=\"2\" align=\"right\"><strong>Total: " . $totalHours . "h " . $totalMins . "m</strong></td>\n"
			   . "<td>&nbsp;</td>\n"
			   . "</tr>\n"
			   . "<tr>\n"
			   . "<td>&nbsp;</td>\n"
			   . "<td>&nbsp;</td>\n"
			   . "<td>&nbsp;</td>\n"
			   . "<td>&nbsp;</td>\n"
			   . "<td colspan=\"2\" align=\"right\"><strong>Grand Total: " . $grandtotalHours . "h " . $grandtotalMins . "m</strong></td>\n"
			   . "<td>&nbsp;</td>\n"
			   . "</tr>\n"
			   . "</tbody>\n"
			   . "</table>\n"
			   . "</div>\n";
		}
		else //No Rows Returned
		{
			echo $noRows;
		}
	}
	
	$db->Close();
	
} //Function End

/*
Added by Brandon
This function is the same as the above but will list all Shift to Section Admins, and list previous Shift in Past Months.
function printShiftsAll($userID, $type) {
	global $db;
	connectDB();
	
	$sqlQuery = "SELECT vAppAdmin.* FROM AppAdmin INNER JOIN vAppAdmin ON AppAdmin.SectionID = vAppAdmin.SectionID WHERE (AppAdmin.IsAdmin = 1) AND (AppAdmin.UserID = $userID)";			
	$rs = $db->Execute($sqlQuery);
	
	while($row = $rs->FetchRow())
	{
		print_r($row);
		echo "<br />";
	}
	
	if ($rs->RowCount()) //If Rows are Returned
	{
		if (strtoupper($type) != "VERBOSE") 
		{
			$startDate 	= date("m/d/Y");
			$endDate 	= date("m/d/Y", strtotime(dateAdd("d", 7, date("m/d/Y"))));
	
			$sqlQuery = "SELECT * FROM vAppShift WHERE ShiftFrom BETWEEN '$startDate' AND '$endDate' ORDER BY ShiftFrom";
			$rs = $db->Execute($sqlQuery);
	
			if ($rs->RowCount()) //If Rows are Returned
			{
				echo "<table cellspacing=\"0\" border=\"0\" width=\"100%\" id=\"shiftTable\">\n"
					. "<thead><tr><th>Volunteer</th><th>Date</th><th>Start Timea</th><th>End Time</th><th>Category</th><th colspan=\"2\">Duration</th></tr></thead>"
					. "<tbody>\n";
	
				while ($row = $rs->FetchRow()) //While Rows are Returned 
				{
					if (strtotime(date("m/d/Y", strtotime($row['ShiftFrom']))) == strtotime($startDate)) 
					{
						$rowClass = " class=\"today\" ";
					} 
					else 
					{
						$rowClass = "";
					}
	
					echo "<tr $rowClass>"
						. "<td width=\"100\">". $row['FirstName'] . " " . $row['LastName'] ."</td>" //Added by Brandon, shows Volunteer First and Last Name
						. "<td width=\"100\">". date("D m/d/Y", strtotime($row['ShiftFrom'])) ."</td>"
						. "<td width=\"75\">". date("g:i A", strtotime($row['ShiftFrom'])) ."</td>"
						. "<td width=\"75\">". date("g:i A", strtotime($row['ShiftTo'])) ."</td>"
						. "<td>". $row['SectionName'] ."</td>"
						. "<td width=\"65\" align=\"right\">". $row['ShiftDuration'] ." hrs</td>"
						. "<td width=\"50\" align=\"right\"><a href=\"javascript:popDetails('". $row['ShiftID'] ."')\">Details</a></td>"
						. "</tr>";
				}
				
				echo "</tbody>\n</table>\n";
			} 
			else //No Rows are Returned
			{
				echo "You currently have no upcoming shifts.";
			}
		} 
		else //No Rows are Returned 
		{			
		
			echo "<h1>Shift Hoursa</h1>"
				. "<table cellspacing=\"0\" border=\"0\" width=\"100%\" id=\"shiftTable\">\n"
				. "<thead><tr><th>Section</th><th>Total Hours</th></tr></thead>"
				. "<tbody>\n";
				
			$sqlQuery = "SELECT SectionName, SUM(ShiftDuration) as 'Total' FROM vAppShift GROUP BY SectionName";
			$rs = $db->Execute($sqlQuery);
			
			while($row = $rs->FetchRow())
			{			
				echo "<tr>"
					. "<td width=\"100\">". $row['SectionName'] ."</td>" 
					. "<td width=\"100\">". $row['Total'] ."</td>"
					. "</tr>";
			}

			echo "</tbody>\n</table>\n<br />";
			
			$currentMonth = date("m");
			$loopMonth = 0;
			
			$sqlQuery = "SELECT datepart(mm, ShiftFrom) AS ShiftMonth, * FROM vAppShift WHERE DATEPART(mm, ShiftFrom) <= $currentMonth ORDER BY ShiftFrom DESC";
			$rs = $db->Execute($sqlQuery);

			if($rs->RowCount()) //If Rows are Returned 
			{
				while ($row = $rs->FetchRow()) //While Rows are Returned  
				{
					if (strtotime(date("m/d/Y", strtotime($row['ShiftFrom']))) == strtotime($startDate)) 
					{
						$rowClass = " class=\"today\" ";
					}
					else 
					{
						$rowClass = "";
					}
	
					if ($loopMonth != $row['ShiftMonth']) 
					{
						if ($loopMonth != 0) echo "</tbody>\n</table><br />\n";
	
						$loopMonth = $row['ShiftMonth'];
						
						echo "<h1>". date("F Y", strtotime($row['ShiftFrom'])) ."</h1>"
							. "<table cellspacing=\"0\" border=\"0\" width=\"100%\" id=\"shiftTable\">\n"
							. "<thead><tr><th>Volunteer</th><th>Date-----</th><th>Start Time</th><th>End Time</th><th>Category</th><th colspan=\"2\">Duration</th></tr></thead>"
							. "<tbody>\n";
					}
	
					echo "<tr $rowClass>"
						. "<td width=\"100\">". $row['FirstName'] . " " . $row['LastName'] ."</td>" //Added by Brandon, shows Volunteer First and Last Name
						. "<td width=\"100\">". date("D m/d/Y", strtotime($row['ShiftFrom'])) ."</td>"
						. "<td width=\"75\">". date("g:i A", strtotime($row['ShiftFrom'])) ."</td>"
						. "<td width=\"75\">". date("g:i A", strtotime($row['ShiftTo'])) ."</td>"
						. "<td>". $row['SectionName'] ."</td>"
						. "<td width=\"65\" align=\"right\">". $row['ShiftDuration'] ." hrs</td>"
						. "<td width=\"50\" align=\"right\"><a href=\"javascript:popDetails('". $row['ShiftID'] ."')\">Details</a></td>"
						. "</tr>";
				}
					

			}
		}
	}
	else //else not admin
	{
		if (strtoupper($type) != "VERBOSE") 
		{
			$startDate = date("m/d/Y");
			$endDate = date("m/d/Y", strtotime(dateAdd("d", 7, date("m/d/Y"))));
	
			$sqlQuery = "SELECT * FROM vAppShift WHERE userID=$userID AND ShiftFrom BETWEEN '$startDate' AND '$endDate' ORDER BY ShiftFrom";
			$rs = $db->Execute($sqlQuery);
	
			if ($rs->RowCount()) //If Rows are Returned  
			{
				echo "<table cellspacing=\"0\" border=\"0\" width=\"100%\" id=\"shiftTable\">\n"
					. "<thead><tr><th>Volunteer</th><th>Date</th><th>Start Time</th><th>End Time</th><th>Category</th><th colspan=\"2\">Duration</th></tr></thead>"
					. "<tbody>\n";
	
				while ($row = $rs->FetchRow()) //While Rows are Returned 
				{
					if (strtotime(date("m/d/Y", strtotime($row['ShiftFrom']))) == strtotime($startDate)) {
						$rowClass = " class=\"today\" ";
					} 
					else 
					{
						$rowClass = "";
					}
	
					echo "<tr $rowClass>"
						. "<td width=\"100\">". $row['FirstName'] . " " . $row['LastName'] ."</td>" //Added by Brandon, shows Volunteer First and Last Name
						. "<td width=\"100\">". date("D m/d/Y", strtotime($row['ShiftFrom'])) ."</td>"
						. "<td width=\"75\">". date("g:i A", strtotime($row['ShiftFrom'])) ."</td>"
						. "<td width=\"75\">". date("g:i A", strtotime($row['ShiftTo'])) ."</td>"
						. "<td>". $row['SectionName'] ."</td>"
						. "<td width=\"65\" align=\"right\">". $row['ShiftDuration'] ." hrs</td>"
						. "<td width=\"50\" align=\"right\"><a href=\"javascript:popDetails('". $row['ShiftID'] ."')\">Details</a></td>"
						. "</tr>";
				}
				
				echo "</tbody>\n</table>\n";
			} 
			else //No Rows are Returned 
			{
				echo "You currently have no upcoming shifts.";
			}
		} 
		else //No Rows are Returned
		{
			$currentMonth = date("m");
			$loopMonth = 0;
			
			echo "<h1>Shift Hoursb</h1>"
				. "<table cellspacing=\"0\" border=\"0\" width=\"100%\" id=\"shiftTable\">\n"
				. "<thead><tr><th>Section</th><th>Total Hours</th></tr></thead>"
				. "<tbody>\n";
				
			$sqlQuery = "SELECT SectionName, SUM(ShiftDuration) as 'Total' FROM vAppShift WHERE userID=$userID GROUP BY SectionName";
			$rs = $db->Execute($sqlQuery);
			
			while($row = $rs->FetchRow())
			{			
				echo "<tr>"
					. "<td width=\"100\">". $row['SectionName'] ."</td>" 
					. "<td width=\"100\">". $row['Total'] ."</td>"
					. "</tr>";
			}

			echo "</tbody>\n</table>\n<br />";
			
			//"SELECT vAppAdmin.* FROM AppAdmin INNER JOIN vAppAdmin ON AppAdmin.SectionID = vAppAdmin.SectionID WHERE (AppAdmin.IsAdmin = 1) AND (AppAdmin.UserID = $userID)"
			
			$sqlQuery = "SELECT datepart(mm, ShiftFrom) AS ShiftMonth, * FROM vAppShift WHERE userID=$userID AND DATEPART(mm, ShiftFrom) <= $currentMonth ORDER BY ShiftFrom DESC";
			$rs = $db->Execute($sqlQuery);
	
			if($rs->RowCount()) //If Rows are Returned 
			{
				while ($row = $rs->FetchRow()) //While Rows are Returned 
				{
					if (strtotime(date("m/d/Y", strtotime($row['ShiftFrom']))) == strtotime($startDate)) 
					{
						$rowClass = " class=\"today\" ";
					} 
					else 
					{
						$rowClass = "";
					}
					
					if ($loopMonth != $row['ShiftMonth']) {
					
						if ($loopMonth != 0) echo "</tbody>\n</table><br />\n";
	
						$loopMonth = $row['ShiftMonth'];
						
						echo "<h1>". date("F Y", strtotime($row['ShiftFrom'])) ."</h1>"
							. "<table cellspacing=\"0\" border=\"0\" width=\"100%\" id=\"shiftTable\">\n"
							. "<thead><tr><th>Volunteer</th><th>Date</th><th>Start Time</th><th>End Time</th><th>Category</th><th colspan=\"2\">Duration</th></tr></thead>"
							. "<tbody>\n";
					}
	
					echo "<tr $rowClass>"
						. "<td width=\"100\">". $row['FirstName'] . " " . $row['LastName'] ."</td>" //Added by Brandon, shows Volunteer First and Last Name
						. "<td width=\"100\">". date("D m/d/Y", strtotime($row['ShiftFrom'])) ."</td>"
						. "<td width=\"75\">". date("g:i A", strtotime($row['ShiftFrom'])) ."</td>"
						. "<td width=\"75\">". date("g:i A", strtotime($row['ShiftTo'])) ."</td>"
						. "<td>". $row['SectionName'] ."</td>"
						. "<td width=\"65\" align=\"right\">". $row['ShiftDuration'] ." hrs</td>"
						. "<td width=\"50\" align=\"right\"><a href=\"javascript:popDetails('". $row['ShiftID'] ."')\">Details</a></td>"
						. "</tr>";
						

				}	
			}
		}
	}
	
	$db->Close();
}
*/

function printNewsSP($type, $sectionID, $userID) {
	global $db;
	connectDB();

	//$sqlQuery = "EXEC hrpcal_AppNewsListAllFor_User '$type', $userID";
	//$rs = $db->Execute($sqlQuery);

	//$stmt = $db->PrepareSP('hrpcal_AppNewsListAllFor_User');
	//$db->InParameter($stmt, $type, 'type');
	//$db->InParameter($stmt, $userID, 'userID');
	//$rs = $db->Execute($stmt);

	while ($row = $rs->FetchRow()) {
		echo "<div class=\"newsItem\">\n"
			. "<div class=\"dateContainer\"><div class=\"month\">". date("M", strtotime($row['PostingDate'])) ."</div><div class=\"day\">". date("d", strtotime($row['PostingDate'])) ."</div></div>\n"
			. "<div class=\"slugline\">Posted in ". $row['SectionName'] ." by ". $row['FirstName'] ." ". $row['LastName']  ."</div>\n"
			. "<div class=\"newsText\">". $row['NewsText'] ."</div>\n"
			. "</div>\n";
	}

	$db->Close();
}

function drawMonth($month, $year, $currentCat, $userID) {
	if (!$month) $month = date("m");
	if (!$year) $year = date("Y");

	$date = mktime(12, 0, 0, $month, 1, $year);
	$daysInMonth = date("t", $date);

	$startDate = "$month/1/$year";
	$endDate = date("m/d/Y", strtotime(dateAdd("m", 1, $startDate)));

	//*********************************
	// Get the events
	//*********************************

	//retrieve all the month's events from the database
	global $db;
	connectDB();

/*
	$sqlQuery = "SELECT * FROM hrpcal_events "
		."INNER JOIN hrpcal_Categories ON hrpcal_Categories.cat_id = hrpcal_events.event_cat "
		."INNER JOIN AppAdmin ON hrpcal_categories.cat_id = AppAdmin.SectionID "
		."WHERE hrpcal_events.event_date BETWEEN '$startDate' AND '$endDate' AND AppAdmin.UserID = $userID ";
*/

	$sqlQuery = "SELECT * FROM hrpcal_events "
		."INNER JOIN hrpcal_Categories ON hrpcal_Categories.cat_id = hrpcal_events.event_cat "
		."INNER JOIN AppAdmin ON hrpcal_categories.cat_id = AppAdmin.SectionID "
		."WHERE hrpcal_events.event_date BETWEEN '$startDate' AND '$endDate' AND hrpcal_events.IsActive = '1' AND AppAdmin.UserID = $userID ";

	if ($currentCat && $currentCat != "ALL") $sqlQuery .= " AND hrpcal_events.event_cat = '$currentCat' ";

	$sqlQuery .= " ORDER BY hrpcal_events.event_date, hrpcal_events.event_timestart ASC";

	$rs = $db->Execute($sqlQuery);
	$arrEvents = $rs->GetRows();

	//echo "<pre>";
	//print_r($arrEvents);
	//echo "</pre>";

	//*********************************
	// Draw the Month
	//*********************************

	// calculate the position of the first day in the calendar (sunday = 1st column, etc)
	$offset = date("w", $date);
	$rows = 1;

	//for the prev/next link arrows
	$prevMonth = date("n", strtotime(dateadd("m", -1, "$year-$month-1")));
	$prevYear = date("Y", strtotime(dateadd("m", -1, "$year-$month-1")));

	$nextMonth = date("n", strtotime(dateadd("m", 1, "$year-$month-1")));
	$nextYear = date("Y", strtotime(dateadd("m", 1, "$year-$month-1")));

	//build the link urls
	$prevLink = "?m=$prevMonth&y=$prevYear&cat=$currentCat";
	$nextLink = "?m=$nextMonth&y=$nextYear&cat=$currentCat";


	//start the table
	echo "<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\"><tr>"
		. "<td width=\"32\"><a href=\"$prevLink\"><img src=\"images/leftArrow.gif\" border=\"0\" /></a></td>"
		. "<td><h2>Month View: ". date("F", $date) . " $year</h2></td>"
		. "<td align=\"right\"><a href=\"$nextLink\"><img src=\"images/rightArrow.gif\" border=\"0\" /></a></td>"
		. "</tr></table><br />"
		. "<table id=\"monthTable\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\">\n<tr class=\"monthHeader\">";

	//print the row with the days of the week
	echo "<tr class=\"monthHeader\">\n";
	$daysOfWeek = array("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat");
	foreach ($daysOfWeek as $dayIndex => $dayName) {
		echo "<td>$dayName</td>\n";
	}
	echo "</tr>\n";

	//fill in the spacers for the beginning of the month
	echo "<tr>\n";
	for ($i = 1; $i <= $offset; $i++) echo "<td class=\"daySpacer\">&nbsp;</td>";

	for ($day = 1; $day <= $daysInMonth; $day++) {
		//if it's sunday - end the row and start a new one
		if (($day + $offset - 1) % 7 == 0 && $day != 1) {
			echo "</tr>\n\t<tr>";
			$rows++;
		}

		//if the day is today's date, style appropriately
		if (strtotime("$year-$month-$day") == strtotime(date("Y-m-d"))) {
			echo "<td class=\"dayToday\"><div class=\"dayLabel\">$day</div>";
		} else {
			//check to see if day is a weekend, style appropriately
			if (date("N", strtotime("$year-$month-$day")) >= 6) {
				echo "<td class=\"dayWeekend\"><div class=\"dayLabel\">$day</div>";
			} else {
				echo "<td class=\"dayNormal\"><div class=\"dayLabel\">$day</div>";
			}
		}

		//loop through the recordset and print the items
		foreach ($arrEvents as &$rsRow) {
			if (strtotime(substr($rsRow['event_date'],0,10)) == strtotime("$year-$month-$day")) {
				//an item exists for this day - print it
				printEvent("MONTH", $rsRow);
			};
		}

		echo "</td>";
	}

	//fill in the spacers for the end of the month
	while (($day + $offset) <= $rows * 7) {
		echo "<td class=\"daySpacer\">&nbsp;</td>";
		$day++;
	}

	echo "</tr>\n";
	echo "</table>\n";

	$db->Close();
}



function printEvent($type, $rsRow) {
	echo "<div title=\"".$rsRow['Comment']."\" onclick=\"popDetails(". $rsRow['event_id'] .")\" class=\"monthEvent\" style=\"background-color: #". $rsRow['cat_hexcolor'] ."; border: 1px solid #fff;\"><a href=\"#\">"
		. date("g:i A", strtotime($rsRow['event_timestart'])) ."-". date("g:i A", strtotime($rsRow['event_timeend'])) ."<br />"
		. ucwords($rsRow['event_name'])
		."</a></div>";
}



function hexDarker($hex, $factor = 30) {
	$new_hex = '';

	$base['R'] = hexdec($hex{0}.$hex{1});
	$base['G'] = hexdec($hex{2}.$hex{3});
	$base['B'] = hexdec($hex{4}.$hex{5});

	foreach ($base as $k => $v) {
		$amount = $v / 100;
		$amount = round($amount * $factor);
		$new_decimal = $v - $amount;

		$new_hex_component = dechex($new_decimal);

		if(strlen($new_hex_component) < 2) $new_hex_component = "0".$new_hex_component;

		$new_hex .= $new_hex_component;
	}

	return $new_hex;
}



function getSimpleTime($longtime) {
	if ($longtime) {
		$tempArr = explode(":", $longtime);

		//break out the hour and the minute
		$simpleHour = $tempArr[0];
		$simpleMinute = $tempArr[1];

		//ampm stuff
		if ($simpleHour > 12) {
			$simpleHour = $simpleHour - 12;
			$ampmFlag = "PM";
		} else {
			$ampmFlag = "AM";
		}

		if ($simpleHour < 1) $simpleHour = "12";
		return $simpleHour .":". $simpleMinute ." ". $ampmFlag;
	}
}


function dateAdd($interval, $number, $dateTime) {
	$dateTime = (strtotime($dateTime) != -1) ? strtotime($dateTime) : $dateTime;
	$dateTimeArr = getdate($dateTime);

	$yr = $dateTimeArr[year];
	$mon = $dateTimeArr[mon];
	$day = $dateTimeArr[mday];
	$hr = $dateTimeArr[hours];
	$min = $dateTimeArr[minutes];
	$sec = $dateTimeArr[seconds];

	switch($interval) {
	case "s"://seconds
		$sec += $number;
		break;
	case "n"://minutes
		$min += $number;
		break;
	case "h"://hours
		$hr += $number;
		break;
	case "d"://days
		$day += $number;
		break;
	case "ww"://Week
		$day += ($number * 7);
		break;
	case "m": //similar result "m" dateDiff Microsoft
		$mon += $number;
		break;
	case "yyyy": //similar result "yyyy" dateDiff Microsoft
		$yr += $number;
		break;
	default:
		$day += $number;
	}

	$dateTime = mktime($hr,$min,$sec,$mon,$day,$yr);
	$dateTimeArr = getdate($dateTime);

	$nosecmin = 0;
	$min = $dateTimeArr[minutes];
	$sec = $dateTimeArr[seconds];

	if ($hr == 0) $nosecmin += 1;
	if ($min == 0) $nosecmin += 1;
	if ($sec == 0) $nosecmin += 1;

	if ($nosecmin > 2) {
		return(date("Y-m-d", $dateTime));
	} else {
		return(date("Y-m-d G:i:s", $dateTime));
	}
}


function str_replace_once($needle, $replace, $haystack) {
    $pos = strpos($haystack, $needle);

	if ($pos === false) {
        // Nothing found
        return $haystack;
    }

    return substr_replace($haystack, $replace, $pos, strlen($needle));
}


function drawTimeSelect($selectname, $selected) {
	if (!empty($selected)) {
		$selected = explode(":", $selected);
		$selectedHour = $selected[0];
		$selectMin = explode(" ", $selected[1]);
	}


	echo "<select name=\"". $selectname ."Hour\">\n";
	for ($i = 1; $i <= 12; $i++) {
		if (strlen($i) < 2) $i = "0$i";

		if ($i == $selectedHour) {
			echo "<option SELECTED value=\"$i\">$i</option>\n";
		} else {
			echo "<option value=\"$i\">$i</option>\n";
		}
	}
	echo "</select>\n";


	$arrMinutes = array("00", "05", "15", "20", "25", "30", "35", "40", "45", "50", "55");
	echo "<select name=\"". $selectname ."Min\">\n";
	foreach ($arrMinutes as $minIndex => $minDisplay) {
		if ($minDisplay == $selectMin[0]) {
			echo "<option SELECTED value=\"$minDisplay\">$minDisplay</option>\n";
		} else {
			echo "<option value=\"$minDisplay\">$minDisplay</option>\n";
		}
	}
	echo "</select>\n";


	$arrPeriods = array("AM", "PM");
	echo "<select name=\"". $selectname ."Ampm\">\n";
	foreach ($arrPeriods as $periodIndex => $periodDisplay) {
		if ($periodDisplay == strtoupper($selectMin[1])) {
			echo "<option SELECTED value=\"$periodDisplay\">$periodDisplay</option>\n";
		} else {
			echo "<option SELECTED value=\"$periodDisplay\">$periodDisplay</option>\n";
		}
	}
	$formName = $selectname . "Ampm";
	
	echo "</select>\n";
}

function printCategories($type, $currentCat = 0, $userID, $calselect) {
	//get all the categories from the database
	global $db;
	connectDB();

	$sqlQuery = "SELECT * "
              . "FROM AppSection INNER JOIN "
              . "AppAdmin ON AppSection.SectionID = AppAdmin.SectionID "
              . "WHERE (AppAdmin.UserID = " . $userID . ") "
              . "ORDER BY AppSection.SectionName";
	$rs = $db->Execute($sqlQuery);

	if ($rs->RowCount()) {
		if (strtoupper($type) == "LEGEND") {
			//print the legend
			while ($row = $rs->FetchRow()) {
				echo "<div class=\"navMenuItem\">"
					."<span class=\"catMarker\" style=\"background-color: #". $row['SectionColor'] .";\">&nbsp;</span>"
					. $row['SectionName'] ."</div>";
			}
		} elseif (strtoupper($type) == "SELECT") {
			//get the urls and the current query params
			$curURL = $_SERVER["PHP_SELF"];
			$curParams = $_SERVER['QUERY_STRING'];

			//convert params to an array
			$curParams = explode("&", $curParams);

			//loop thru the params
			foreach ($curParams as $arrindex => $param) {
				if (strrpos($param, "cat") === false) {
					//if the param is NOT a category param keep it in the query
					$newParams .= "&".$param;
				}
			}

			//get rid of the first ampersand
			$newParams = str_replace_once("&", "", $newParams);

			if ($calselect == true) {
				echo "<select name=\"fCategory\" onchange=\"document.location.href='$curURL?$newParams&cat=' + this.value;\">\n"
					. "<option value=\"ALL\">View All Categories</options>\n";
			} else {
				echo "<select name=\"fCategory\"><option value=\"0\"></option>\n";
			}

			while ($row = $rs->FetchRow()) {
				if ($currentCat == $row['SectionID']) {
					echo "<option SELECTED value=\"". $row['SectionID'] ."\">". $row['SectionName'] ."</option>\n";
				} else {
					echo "<option value=\"". $row['SectionID'] ."\">". $row['SectionName'] ."</option>\n";
				}
			}
			echo "</select>";
		}
	}

	$db->Close();
}

function drawUserSelect($userID, $selectname, $selected) {
	global $db;
	connectDB();

	$sqlQuery = "SELECT * "
		      . "FROM AppAdmin "
		      . "WHERE UserID = " . $userID . " "
			  . "AND IsAdmin = 1";
	$rs = $db->Execute($sqlQuery);
		
	//If Rows are Returned
	if($rs->RowCount()) 
	{
		echo "<select name=\"" . $selectname . "\" style=\"color: #999999\">\n";
		
		$i = 0; //Define $i as 0
		
		//User Selection
		$sqlQuery = "SELECT UserID, FirstName, LastName, IsActive, SectionID "
		          . "FROM vAppUsers WHERE ";

		//Loop though all Sections available to the current or selected user
		while($row = $rs->FetchRow())
		{
			//If $i is Less Then 1
			if($i < 1)
			{
				$sqlQuery .= "((SectionID = " . $row['SectionID'] . ")"; //Only used if one Section is available.
			}
			else //Not Less Then 1
			{
				$sqlQuery .= " OR (SectionID = " . $row['SectionID'] . ")"; //Adds this once $i has counted over 1
			}
			
			$i++; //$i + 1
		}
		
		$sqlQuery .= ") "
		           . "AND IsActive = 1 "
		           . "ORDER BY FirstName, LastName ASC";
		$rs = $db->Execute($sqlQuery);

		$i = 0; //Define $i as 0
		
		$currentUserInLoop = array();

		//While Rows are Returned
		while ($row = $rs->FetchRow()) 
		{
			$currentUserInLoop[$i] = $row['UserID'];
			
			if($row['UserID'] != $currentUserInLoop[$i - 1])
			{
				if($selected == $row['UserID'])
				{
					echo "<option selected=\"selected\" value=\"" . $row['UserID'] . "\">". $row['FirstName'] . " " . $row['LastName'] . "</option>\n";
				}
				else
				{
					echo "<option value=\"" . $row['UserID'] . "\">". $row['FirstName'] . " " . $row['LastName'] . "</option>\n";
				}
			}
			
			$i++; //$i + 1
		}
		echo "</select>";
	}
	else //No Rows Returned - Not admin of anything
	{
		$sqlQuery = "SELECT * "
				  . "FROM AppUser "
				  . "WHERE UserID = " . $userID . " "; 
		$rs = $db->Execute($sqlQuery);
		
		echo "<select name=\"" . $selectname . "\" style=\"color: #999999\">\n";
		
		//While Rows are Returned
		while ($row = $rs->FetchRow()) 
		{
			echo "<option value=\"" . $row['UserID'] . "\" selected=\"selected\">". $row['FirstName'] . " " . $row['LastName'] . " </option>\n";
		}
		
		echo "</select>";
	}
}


function getShiftFormData() {
	$shiftVolunteer = $_POST['fVolunteer'];
	$shiftFrom = $_POST['fShiftDate'] ." ". $_POST['fStartTimeHour'] .":". $_POST['fStartTimeMin'] .":00 ".	$_POST['fStartTimeAmpm'];
	$shiftTo = $_POST['fShiftDate'] ." ". $_POST['fEndTimeHour'] .":". $_POST['fEndTimeMin'] .":00 ".	$_POST['fEndTimeAmpm'];
	$shiftComments = strip_tags($_POST['fComments']);
	$shiftCategory = $_POST['fCategory'];
	$shiftID = $_POST['fShiftID'];

	$formValues = array($shiftVolunteer, $shiftFrom, $shiftTo, $shiftTo, $shiftComments, $shiftCategory, $shiftID);
	return $formValues;
}


function updateShift($action, $formValues, $shiftID) {
	global $db, $currentUserID;

	$currentDate = date("m/d/Y g:i:s a");
	$cleanedComment = str_replace("'", "", strip_tags($formValues[4]));

	switch ($action) {
	case "ADDNEW":
		$sqlQuery = "INSERT INTO AppShift "
			. "(SectionID, UserID, ShiftFrom, ShiftTo, Comment, AddedByID, DateAdded) "
			. "VALUES ($formValues[5], $formValues[0], '$formValues[1]', '$formValues[2]', '$cleanedComment', $currentUserID, '$currentDate')";
		break;
	case "EDIT":
		$sqlQuery = "UPDATE AppShift SET "
			. "SectionID=$formValues[5], UserID=$formValues[0], ShiftFrom='$formValues[1]', ShiftTo='$formValues[2]', Comment='$formValues[4]' "
			. "WHERE ShiftID=$formValues[6]";
		break;
	case "DELETE":
		$sqlQuery = "DELETE FROM AppShift "
			. "WHERE ShiftID=$shiftID";
		break;
	}

	connectDB();
	$rs = $db->Execute($sqlQuery);
	$db->Close();
}
?>