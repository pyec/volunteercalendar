<?php

function userIsAdmin($userID)
{
	global $db;
	connectDB();
		
	$sqlQuery = "SELECT IsAdmin, UserID "
			  . "FROM vAppAdmin "
			  . "WHERE UserID = $userID "
			  . "AND IsAdmin = 1";			
	$rs = $db->Execute($sqlQuery);
	
	if ($rs->RowCount()) 
	{
		return true;
	}
	else
	{
		return false;
	}
	
	$db->Close();
}

function printUsers($userID) 
{
	global $db;
	connectDB();
	
	echo "printUsers";
	
	echo "<table class=\"SearchAndAlert\"><tr><td valign=\"middle\" class=\"Action\">Manage Users</td>"
	   . "<td class=\"SearchBox\"><form id=\"AdminUserListSearch\" name=\"SearchAdminUserList\" method=\"get\" action=\"AdminUserList.php\">"
	   . "<input type=\"text\" name=\"filter\" id=\"filter\" value=\"Search for a user...\" onfocus=\"this.value=''\" />"
	   . "<input src=\"images/magnifier.png\" class=\"SearchButton\" value=\"Search\" type=\"image\" id=\"Search\" title=\"Search\" />"
	   . "<img src=\"images/refresh.png\" class=\"RefreshButton\" title=\"Clear Results\" onclick=\"window.location='AdminUserList.php'\" />"
	   . "</form></td></tr></table>"
	   . "<table cellspacing=\"0\" border=\"0\" width=\"100%\" id=\"userTable\">\n"
	   . "<thead>"
	   . "<tr>"
	   . "<th>&nbsp;</th>"
	   . "<th>Name</th>"
	   . "<th>Category</th>"
	   . "<th>Email</th>"
	   . "<th>Phone Number</th>"
	   . "</tr>"
	   . "</thead>"
	   . "<tbody>\n";

	$sqlQuery = "SELECT UserID, SectionID "
			  . "FROM vAppAdmin "
			  . "WHERE UserID = " . $userID . "";
	$rs = $db->Execute($sqlQuery);

	$i = 0; //Define $i as 0
	
	if ($rs->RowCount()) 
	{
		$sqlQuery = "SELECT * "
				  . "FROM vAppAdmin "
				  . "WHERE ";
					  
		//While Rows are Returned
		while($row = $rs->FetchRow())
		{
			//If $i is Less Then 1
			if($i < 1)
			{
				$sqlQuery .= "(SectionID = " . $row['SectionID'] . ""; //Only used if one Section is available.
				
				if($i == 1)
				{
					$sqlQuery .= ") ";
				}
			}
			else //Not Less Then 1
			{
				$sqlQuery .= " OR SectionID = " . $row['SectionID'] . ""; //Adds this once $i has counted over 1
			}
			
			$i++; //$i + 1
		}
		
		$sqlQuery .= ") "
				   . "AND (IsActive = 1) "
				   . "ORDER BY FirstName, LastName, SectionName ASC";
		echo $sqlQuery;
		$rs = $db->Execute($sqlQuery);
		
		$i = 1; //Define $i as 0
		$count = 0; //Define $count as 0
		
		$currentUserInLoop = array();
		$sectionNames = "";
		
		$currentUserInLoop[0] = 99;
		
		//While Rows are Returned
		while($row = $rs->FetchRow()) 
		{
			$currentUserInLoop[$i] = $row['UserID'];
			
			if($row['UserID'] != $currentUserInLoop[0])
			{
					//If $username_Prev comes back as true containing text
					if($userName_Prev)
					{
						//Check if the current logged in user is the current user in the loop
						if($userID_Prev == $userID)
						{
							echo "<td><a href=\"AdminUserDetails.php?userID=" . $userID_Prev . "&action=EDIT\" title=\"Edit " . $userName_Prev . "'s profile\"><strong>" . $userName_Prev . "</strong></a></td>";
						}
						else
						{
							if(userIsAdmin($userID))
							{
								echo "<td><a href=\"AdminUserDetails.php?userID=" . $userID_Prev . "&action=EDIT\" title=\"Edit " . $userName_Prev . "'s profile\">" . $userName_Prev . "</a></td>";
							}
							else
							{
								echo "<td>" . $userName_Prev . "</td>";
							}
						}
						
						echo "<td width=\"130\">" . $sectionNames . "</td>";
						
						//Search for the '@' symbol to check if the current email is a valid email and not just text.
						if(strpbrk($email_Prev, "@"))
						{
							echo "<td><a href=\"mailto:" . $email_Prev . "?subject=HRP Volunteer Calendar\" title=\"Send " . $userName_Prev . " an email\">" . $email_Prev . "</a></td>";
						}
						else
						{
							echo "<td>N/A</td>";
						}
						echo "<td width=\"110px\">" . $phone_Prev . "</td>"
						   . "</tr>\n";
					}
					
					$userID_Prev 		= $row['UserID'];
					$userName_Prev 		= $row['FirstName'] . " " . $row['LastName'];
					$email_Prev 		= $row['Email1'];
					$phone_Prev 		= $row['Phone1'];
					
					$sectionNames = "";	
			}
			
			//Check to see if $SectionName is empty
			if($sectionNames == "") 
			{
				$sectionNames = $row['SectionName'];
			} 
			else 
			{
				$sectionNames = $sectionNames . "<br />" . $row['SectionName'];
			}
			
			$count++; //$count + 1
			$i++; //$i + 1
		}
		
		print_r($currentUserInLoop);
	}
	
	$db->Close();
}

function printUsersByFilter($userID, $filter)
{
	global $db;
	connectDB();
	
	echo "printUsersByFilter";
	
	echo "<table class=\"SearchAndAlert\"><tr><td valign=\"middle\" class=\"Action\">Manage Users</td>"
	   . "<td class=\"SearchBox\"><form id=\"AdminUserListSearch\" name=\"SearchAdminUserList\" method=\"get\" action=\"AdminUserList.php\">"
	   . "<input type=\"text\" name=\"filter\" id=\"filter\" value=\"Search for a user...\" onfocus=\"this.value=''\" />"
	   . "<input src=\"images/magnifier.png\" class=\"SearchButton\" value=\"Search\" type=\"image\" id=\"Search\" title=\"Search\" />"
	   . "<img src=\"images/refresh.png\" class=\"RefreshButton\" title=\"Clear Results\" onclick=\"window.location='AdminUserList.php'\" />"
	   . "</form></td></tr></table>"
	   . "<table cellspacing=\"0\" border=\"0\" width=\"100%\" id=\"userTable\">\n"
	   . "<thead>"
	   . "<tr>"
	   . "<th>Name</th>"
	   . "<th>Category</th>"
	   . "<th>Email</th>"
	   . "<th>Phone Number</th>"
	   . "</tr>"
	   . "</thead>"
	   . "<tbody>\n";

	$sqlQuery = "SELECT UserID, SectionID "
			  . "FROM vAppAdmin "
			  . "WHERE UserID = " . $userID . "";
	$rs = $db->Execute($sqlQuery);

	$i = 0; //Define $i as 0
	
	if ($rs->RowCount()) 
	{
		$sqlQuery = "SELECT * "
				  . "FROM vAppAdmin "
				  . "WHERE ";
				  
		//Loop though all Sections available to the current or selected user
		while($row = $rs->FetchRow())
		{
			//If $i is Less Then 1
			if($i < 1)
			{
				$sqlQuery .= "(SectionID = " . $row['SectionID'] . ""; //Only used if one Section is available.
				
				if($i == 1)
				{
					$sqlQuery .= ") ";
				}
			}
			else //Not Less Then 1
			{
				$sqlQuery .= " OR SectionID = " . $row['SectionID'] . ""; //Adds this once $i has counted over 1
			}
			
			$i++; //$i + 1
		}
			
		$sqlQuery .= ") "
				   . "AND (FirstName LIKE '%" . $filter . "%' OR LastName LIKE '%" . $filter . "%') "
				   . "AND (IsActive = 1) "
				   . "ORDER BY FirstName, LastName, SectionName ASC";
		echo $sqlQuery;
		$rs = $db->Execute($sqlQuery);
		
		$i = 0; //Define $i as 0
		$count = 0; //Define $count as 0
		
		$currentUserInLoop = array();
		$sectionNames = "";
		$userID_Prev = 0;
		
		//While Rows are Returned
		while($row = $rs->FetchRow()) 
		{
			if($row['UserID'] == $userID_Prev)
			{
			
				$userID_Prev 		= $row['UserID'];
				$userName_Prev 		= $row['FirstName'] . " " . $row['LastName'];
				$email_Prev 		= $row['Email1'];
				$phone_Prev 		= $row['Phone1'];
					
				//$sectionNames = $row['SectionName'];
				
				if($sectionNames == "") {
					$sectionNames = $row['SectionName'];
				} else {
					$sectionNames = $sectionNames . "<br/>" . $row['SectionName'];
				}
				
			
				//if($sectionNames == "";
				//$sectionNames = $row['SectionName'];
				
				//If $username_Prev comes back as true containing text
				if($userName_Prev)
				{
					//Check if the current logged in user is the current user in the loop
					if($userID_Prev == $userID)
					{
						echo "<td><a href=\"AdminUserDetails.php?userID=" . $userID_Prev . "&action=EDIT\" title=\"Edit " . $userName_Prev . "'s profile\"><strong>" . $userName_Prev . "a</strong></a></td>";
					}
					else
					{
						if(userIsAdmin($userID))
						{
							echo "<td><a href=\"AdminUserDetails.php?userID=" . $userID_Prev . "&action=EDIT\" title=\"Edit " . $userName_Prev . "'s profile\">" . $userName_Prev . "a</a></td>";
						}
						else
						{
							echo "<td>" . $userName_Prev . "a</td>";
						}
					}
					
					echo "<td width=\"130\">" . $sectionNames . "</td>";
					
					//Search for the '@' symbol to check if the current email is a valid email and not just text.
					if(strpbrk($email_Prev, "@"))
					{
						echo "<td><a href=\"mailto:" . $email_Prev . "?subject=HRP Volunteer Calendar\" title=\"Send " . $userName_Prev . " an email\">" . $email_Prev . "</a></td>";
					}
					else
					{
						echo "<td>N/A</td>";
					}
					
					echo "<td width=\"110px\">" . $phone_Prev . "</td>"
					   . "</tr>\n";
				}	
			}
			else
			{
				$userID_Prev 		= $row['UserID'];
				$userName_Prev 		= $row['FirstName'] . " " . $row['LastName'];
				$email_Prev 		= $row['Email1'];
				$phone_Prev 		= $row['Phone1'];
					
				$sectionNames = $row['SectionName'];
				/*
				if($sectionNames == "") {
					$sectionNames = $row['SectionName'];
				} else {
					$sectionNames = $sectionNames . "<br/>" . $row['SectionName'];
				}
				*/
				
				//If $username_Prev comes back as true containing text
				if($userName_Prev)
				{
					//Check if the current logged in user is the current user in the loop
					if($userID_Prev == $userID)
					{
						echo "<td><a href=\"AdminUserDetails.php?userID=" . $userID_Prev . "&action=EDIT\" title=\"Edit " . $userName_Prev . "'s profile\"><strong>" . $userName_Prev . "</strong></a></td>";
					}
					else
					{
						if(userIsAdmin($userID))
						{
							echo "<td><a href=\"AdminUserDetails.php?userID=" . $userID_Prev . "&action=EDIT\" title=\"Edit " . $userName_Prev . "'s profile\">" . $userName_Prev . "</a></td>";
						}
						else
						{
							echo "<td>" . $userName_Prev . "</td>";
						}
					}
					
					echo "<td width=\"130\">" . $sectionNames . "</td>";
					
					//Search for the '@' symbol to check if the current email is a valid email and not just text.
					if(strpbrk($email_Prev, "@"))
					{
						echo "<td><a href=\"mailto:" . $email_Prev . "?subject=HRP Volunteer Calendar\" title=\"Send " . $userName_Prev . " an email\">" . $email_Prev . "</a></td>";
					}
					else
					{
						echo "<td>N/A</td>";
					}
					
					echo "<td width=\"110px\">" . $phone_Prev . "</td>"
					   . "</tr>\n";
				}
			}
			
			$count++; //$count + 1
			$i++; //$i + 1
		}
	}
	
	$db->Close();
}


































?>
