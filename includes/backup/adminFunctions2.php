<?php

function printUsers($userID, $filter, $modifiedID, $action) {
	global $db;
	connectDB();

	$sqlQuery = "SELECT * "
		      . "FROM vAppAdmin "
		      . "WHERE UserID = $userID "
			  . "AND IsAdmin = 1";
	$rs = $db->Execute($sqlQuery);
	
	//If Rows are Returned
	if ($rs->RowCount()) 
	{		
		$sqlQuery = "SELECT UserID, SectionID "
		          . "FROM vAppAdmin "
		          . "WHERE UserID = $userID";
		$rs = $db->Execute($sqlQuery);
	
		$i = 0; //Define $i as 0
		
		if($filter)
		{
			$sqlQuery = "SELECT UserID, FirstName, LastName, SectionName, SectionID, Email1, Phone1, IsActive, COUNT(*) "
			          . "FROM vAppAdmin "
			          . "WHERE ";
					  
			//Loop though all Sections available to the current or selected user
			while($row = $rs->FetchRow())
			{
				//If $i is Less Then 1
				if($i < 1)
				{
					$sqlQuery .= "((SectionID = {$row['SectionID']})"; //Only used if one Section is available.
				}
				else //Not Less Then 1
				{
					$sqlQuery .= " OR (SectionID = {$row['SectionID']})"; //Adds this once $i has counted over 1
				}
				
				$i++; //$i + 1
			}
			$sqlQuery .= ") "
			           . "AND ((FirstName Like '%$filter%') OR (LastName Like '%$filter%')) "
					   . "AND IsActive = 1 "
			           . "GROUP BY SectionName, FirstName, LastName, UserID, SectionID, Email1, Phone1, IsActive";
		}
		else
		{
			$sqlQuery = "SELECT UserID, FirstName, LastName, SectionName, SectionID, Email1, Phone1, IsActive, COUNT(*) "
			          . "FROM vAppAdmin "
			          . "WHERE ";
					  
			//Loop though all Sections available to the current or selected user
			while($row = $rs->FetchRow())
			{
				//If $i is Less Then 1
				if($i < 1)
				{
					$sqlQuery .= "((SectionID = {$row['SectionID']})"; //Only used if one Section is available.
				}
				else //Not Less Then 1
				{
					$sqlQuery .= " OR (SectionID = {$row['SectionID']})"; //Adds this once $i has counted over 1
				}
				
				$i++; //$i + 1
			}
			$sqlQuery .= ") "
			           . "AND IsActive = 1 "
			           . "GROUP BY SectionName, FirstName, LastName, UserID, SectionID, Email1, Phone1, IsActive";
		}
		$rs = $db->Execute($sqlQuery);

		//If Rows are Returned
		if ($rs->RowCount()) {
			
			//if the rowcount is not zero it returned rows
			echo "<table class=\"SearchAndAlert\"><tr><td valign=\"middle\" class=\"Action\">Manage Users</td>"
				. "<td class=\"SearchBox\"><form id=\"AdminUserListSearch\" name=\"SearchAdminUserList\" method=\"get\" action=\"AdminUserList.php\">"
				. "<input type=\"text\" name=\"filter\" id=\"filter\" value=\"Search for a user...\" onfocus=\"this.value=''\" />"
				. "<input src=\"images/magnifier.png\" class=\"SearchButton\" value=\"Search\" type=\"image\" id=\"Search\" title=\"Search\" />"
				. "<img src=\"images/refresh.png\" class=\"RefreshButton\" title=\"Clear Results\" onclick=\"window.location='AdminUserList.php'\" />"
				. "</form></td></tr></table>"
				. "<table cellspacing=\"0\" border=\"0\" width=\"100%\" id=\"userTable\">\n"
				. "<thead>"
				. "<tr>"
				. "<th>&nbsp;</th>"
				. "<th>Name</th>"
				. "<th>Category</th>"
				. "<th>Email</th>"
				. "<th>Phone Number</th>"
			  //. "<th>&nbsp;</th>"
				. "</tr>"
				. "</thead>"
				. "<tbody>\n";
		}
		else //No Rows Returned
		{
			echo "No Users where Found.";
		}
	
		while ($row = $rs->FetchRow()) {
			//loop through and output all the rows in the $rs object
	
			//$highlightUser = "1159";
			//$action = "UPDATED";
	
			//find out which user was last added or updated and prefix them with an appropriate image
			if ($row['UserID'] == $modifiedID) {
				$highlight = " class=\"HighlightUser\"";
				if($action == "UPDATED")
					$updateImage = "<img src=\"images/accept.png\" title=\"".$row['FirstName']." ".$row['LastName']."'s profile has been updated\" />";
				else if($action == "ADDED")
					$updateImage = "<img src=\"images/add.png\" title=\"".$row['FirstName']." ".$row['LastName']." has been added to the system\" />";
			}
				
			echo "<tr".$highlight.">"
			   . "<td><img src=\"images/icon_shiftlist.gif\" title=\"View Shifts for ".$row['FirstName']." ".$row['LastName']."\" border=\"0\" onclick=\"location.href='shifts.php?type=person&userid=" . $row['UserID'] . "'\" onmouseover=\"this.style.cursor='pointer';\" /></td>";
			
			//Check if the current logged in user is the current user in the loop
			if($row['UserID'] == $userID)
			{
				echo "<td>".$updateImage."<a href=\"AdminUserDetails.php?userID=".$row['UserID']."&action=EDIT\" title=\"Edit ".$row['FirstName']." ".$row['LastName']."'s profile\"><strong>".$row['FirstName']."&nbsp;".$row['LastName']."</strong></a></td>";
			}
			else
			{
				echo "<td>".$updateImage. "<a href=\"AdminUserDetails.php?userID=".$row['UserID']."&action=EDIT\" title=\"Edit ".$row['FirstName']." ".$row['LastName']."'s profile\">".$row['FirstName']."&nbsp;".$row['LastName']."</a></td>";
			}
			
			echo "<td width=\"130\">".$row['SectionName']."</td>";
			
			//Search for the '@' symbol to check if the current email is a valid email and not just text.
			if(strpbrk($row['Email1'], "@"))
			{
				echo "<td><a href=\"mailto:" . $row['Email1'] . "?subject=HRP Volunteer Calendar\" title=\"Send ".$row['FirstName']." ".$row['LastName']." an email\">".$row['Email1']."</a></td>";
			}
			else
			{
				echo "<td>N/A</td>";
			}
			echo "<td width=\"110px\">" . $row['Phone1'] . "</td>"
			   . "</tr>\n";
	
			$highlight = "";
			$userUpdated = "";
			$updateImage = "";
	
		}
	
		echo "</tbody></table>";
	}
	else //No Rows Returned
	{		
		//echo "Not Admin";
		
		$sqlQuery = "SELECT UserID, SectionID "
		          . "FROM vAppAdmin "
		          . "WHERE UserID = $userID";
		$rs = $db->Execute($sqlQuery);
		
		$i = 0; //Define $i as 0
		
		if($filter)
		{
			$sqlQuery = "SELECT UserID, FirstName, LastName, SectionName, SectionID, Email1, Phone1, IsActive, COUNT(*) "
			. "FROM vAppAdmin "
			. "WHERE ";
					  
			//Loop though all Sections available to the current or selected user
			while($row = $rs->FetchRow())
			{
				//If $i is Less Then 1
				if($i < 1)
				{
					$sqlQuery .= "((SectionID = {$row['SectionID']})"; //Only used if one Section is available.
				}
				else //Not Less Then 1
				{
					$sqlQuery .= " OR (SectionID = {$row['SectionID']})"; //Adds this once $i has counted over 1
				}
				
				$i++; //$i + 1
			}
			$sqlQuery .= ") "
			           . "AND ((FirstName Like '%$filter%') OR (LastName Like '%$filter%'))"
					   . "AND IsActive = 1"
			           . "GROUP BY SectionName, FirstName, LastName, UserID, SectionID, Email1, Phone1, IsActive";
		}
		else
		{
			$sqlQuery = "SELECT UserID, FirstName, LastName, SectionName, SectionID, Email1, Phone1, IsActive, COUNT(*) "
			          . "FROM vAppAdmin "
			          . "WHERE ";
					  
			//Loop though all Sections available to the current or selected user
			while($row = $rs->FetchRow())
			{
				//If $i is Less Then 1
				if($i < 1)
				{
					$sqlQuery .= "((SectionID = {$row['SectionID']})"; //Only used if one Section is available.
				}
				else //Not Less Then 1
				{
					$sqlQuery .= " OR (SectionID = {$row['SectionID']})"; //Adds this once $i has counted over 1
				}
				
				$i++; //$i + 1
			}
			$sqlQuery .= ") "
			           . "AND IsActive = 1"
			           . "GROUP BY SectionName, FirstName, LastName, UserID, SectionID, Email1, Phone1, IsActive";
		}
		$rs = $db->Execute($sqlQuery);
		
		//If Rows are Returned
		if ($rs->RowCount()) {
	
			//if the rowcount is not zero it returned rows
			echo "<table class=\"SearchAndAlert\"><tr><td valign=\"middle\" class=\"Action\">Contact Book</td>"
				. "<td class=\"SearchBox\"><form id=\"AdminUserListSearch\" name=\"SearchAdminUserList\" method=\"get\" action=\"AdminUserList.php\">"
				. "<input type=\"text\" name=\"filter\" id=\"filter\" value=\"Search for a user...\" onfocus=\"this.value=''\" />"
				. "<input src=\"images/magnifier.png\" class=\"SearchButton\" value=\"Search\" type=\"image\" id=\"Search\" title=\"Search\" />"
				. "<img src=\"images/refresh.png\" class=\"RefreshButton\" title=\"Clear Results\" onclick=\"window.location='AdminUserList.php'\" />"
				. "</form></td></tr></table>"
				. "<table cellspacing=\"0\" border=\"0\" width=\"100%\" id=\"userTable\">\n"
				. "<thead>"
				. "<tr>"
				. "<th>&nbsp;</th>"
				. "<th>Name</th>"
				. "<th>Category</th>"
				. "<th>Email</th>"
				. "<th>Phone Number</th>"
			  //. "<th>&nbsp;</th>"
				. "</tr>"
				. "</thead>"
				. "<tbody>\n";
		}
		else //No Rows Returned
		{
			echo "No Users where Found.";
		}
	
		while ($row = $rs->FetchRow()) {
			//loop through and output all the rows in the $rs object
	
			//$highlightUser = "1159";
			//$action = "UPDATED";
	
			//find out which user was last added or updated and prefix them with an appropriate image
			if ($row['UserID'] == $modifiedID) {
				$highlight = " class=\"HighlightUser\"";
				if($action == "UPDATED")
					$updateImage = "<img src=\"images/accept.png\" title=\"".$row['FirstName']." ".$row['LastName']."'s profile has been updated\" />";
				else if($action == "ADDED")
					$updateImage = "<img src=\"images/add.png\" title=\"".$row['FirstName']." ".$row['LastName']." has been added to the system\" />";
			}
	
			echo "<tr".$highlight.">";
			
			//Check if the current logged in user is the current user in the loop
			if($row['UserID'] == $userID)
			{
				echo "<td>".$updateImage."<a href=\"AdminUserDetails.php?userID=".$row['UserID']."&action=EDIT\" title=\"Edit ".$row['FirstName']." ".$row['LastName']."'s profile\"><strong>".$row['FirstName']."&nbsp;".$row['LastName']."</strong></a></td>";
			}
			else
			{
				echo "<td>".$updateImage. "<a href=\"AdminUserDetails.php?userID=".$row['UserID']."&action=EDIT\" title=\"Edit ".$row['FirstName']." ".$row['LastName']."'s profile\">".$row['FirstName']."&nbsp;".$row['LastName']."</a></td>";
			}
			
			echo "<td width=\"130\">".$row['SectionName']."</td>";
			
			//Search for the '@' symbol to check if the current email is a valid email and not just text.
			if(strpbrk($row['Email1'], "@"))
			{
				echo "<td><a href=\"mailto:" . $row['Email1'] . "?subject=HRP Volunteer Calendar\" title=\"Send ".$row['FirstName']." ".$row['LastName']." an email\">".$row['Email1']."</a></td>";
			}
			else
			{
				echo "<td>N/A</td>";
			}
			echo "<td width=\"110px\">" . $row['Phone1'] . "</td>"
			   . "</tr>\n";
	
			$highlight = "";
			$userUpdated = "";
			$updateImage = "";
			
		}
	
		echo "</tbody></table>";

	}

	$db->Close();
		
}






function printNavigation($userID)
{
	global $db;
	connectDB();
		
	$sqlQuery = "SELECT IsAdmin, UserID, SectionName, SectionID "
			  . "FROM vAppAdmin "
			  . "WHERE UserID = $userID "
			  . "AND IsAdmin = 1";			
	$rs = $db->Execute($sqlQuery);
	
	if ($rs->RowCount()) 
	{
		?>
		<div class="navMenu">
			<a class="navButton" href="index.php" title="Volunteer Home"><img src="images/icon_home.gif" border="0" alt="Volunteer Home" />Volunteer Home</a>
			<a class="navButton" href="cal.php" title="View Shift Calendar"><img src="images/icon_cal.gif" border="0" alt="View Shift Calendar" />Shift Calendar</a>
			<a class="navButton" href="shifts.php" title="View Shift List"><img src="images/icon_shiftlist.gif" border="0" alt="View Shift List" />Shift List</a>
			<a class="navButton" href="javascript:popDetails('','ADDNEW');" title="Add New Shift"><img src="images/icon_caladd.gif" border="0" alt="Add New Shift" />Add New Shift</a>
			<!--<a class="navButton" href="" title="View Messages"><img src="images/icon_message.gif" border="0" alt="Messages" />Message Box</a>-->
			<a class="navButton" href="news.php?action=ADDNEW" title="Post News Item"><img src="images/icon_newsadd.gif" border="0" alt="Post News Item" />Post News Item</a>
			<a class="navButton" href="AdminUserDetails.php?action=ADDNEW" title="Add User"><img src="images/group_add.png" border="0" alt="Add User" />Add User</a>
			<a class="navButton" href="AdminUserList.php" title="Manage Users"><img src="images/group_edit.png" border="0" alt="Manage Users" />Manage Users</a>
			<a class="navButton" href="login.php?action=LOGOUT" title="Logout"><img src="images/icon_logout.gif" border="0" alt="Logout" />Logout</a>
		</div>
		<? 
	}
	else
	{
		?>
		<div class="navMenu">
			<a class="navButton" href="index.php" title="Volunteer Home"><img src="images/icon_home.gif" border="0" alt="Volunteer Home" />Volunteer Home</a>
			<a class="navButton" href="cal.php" title="View Shift Calendar"><img src="images/icon_cal.gif" border="0" alt="View Shift Calendar" />Shift Calendar</a>
			<a class="navButton" href="shifts.php" title="View Shift List"><img src="images/icon_shiftlist.gif" border="0" alt="View Shift List" />Shift List</a>
			<a class="navButton" href="javascript:popDetails('','ADDNEW');" title="Add New Shift"><img src="images/icon_caladd.gif" border="0" alt="Add New Shift" />Add New Shift</a>
			<!--<a class="navButton" href="" title="View Messages"><img src="images/icon_message.gif" border="0" alt="Messages" />Message Box</a>-->
			<a class="navButton" href="news.php?action=ADDNEW" title="Post News Item"><img src="images/icon_newsadd.gif" border="0" alt="Post News Item" />Post News Item</a>
            <a class="navButton" href="AdminUserList.php" title="Contact Book"><img src="images/group_edit.png" border="0" alt="Contact Book" />Contact Book</a>
			<a class="navButton" href="login.php?action=LOGOUT" title="Logout"><img src="images/icon_logout.gif" border="0" alt="Logout" />Logout</a>
		</div>
		<? 
	}
	
	$db->Close();
}







function userDetails($current_userID, $requested_userID, $action, $status) {
	global $db;
	connectDB();

	if ($status == "") {

		switch($action) {
		case "ADDNEW":
			$pageTitle = "Add New User";
			$submitStatus = "Added";
			break;
		case "EDIT";
			$pageTitle = "Edit Mode";
			$submitStatus = "Updated";
			break;
		case "DELETE";
			$pageTitle = "User Deleted";
			$submitStatus = "Deleted";
			break;
		}

	}

	else {
		$pageTitle = $status;
		$submitStatus = $status;
	}

	$sqlQuery = "SELECT * "
			  . "FROM vAppAdmin "
			  . "WHERE UserID = $current_userID "
			  . "AND IsAdmin = 1";
	$rs = $db->Execute($sqlQuery);
	$row = $rs->FetchRow();
	
	if($row['IsAdmin'] == 1)
	{
		$currentUser_IsAdmin = 1;
	}
	else
	{
		$currentUser_IsAdmin = 0;
	}
		
	if($action == "ADDNEW")
	{
		$sqlQuery	= "SELECT UserID "
		            . "FROM AppUser "
		            . "ORDER BY UserID DESC";
		$rs = $db->Execute($sqlQuery);
		
		$row = $rs->FetchRow();
		$requested_userID = $row['UserID'] + 1; //Get next UserID by getting the current and adding 1 ;
	}
		
	echo "<table class=\"SearchAndAlert\"><tr><td class=\"Action\">".$pageTitle."</td></tr></table>\n"
		//."<form id=\"AdminUserDetails\" name=\"AdminUserDetails\" method=\"Post\" action=\"AdminUserDetails.php\" onsubmit=\"return validateTheForm(this)\">\n"
		."<form action=\"AdminUserDetails.php?userID=".$requested_userID."&action=".$action."&status=".$submitStatus."\" method=\"post\" name=\"AdminUserDetails\" id=\"AdminUserDetails\" onsubmit=\"return validateTheForm(this);\">"
		."<table cellspacing=\"0\" border=\"0\" width=\"100%\" id=\"userTable\">\n"
		."<thead>\n<tr><th>Item</th><th>Description</th><th><span style=\"float:right;\">";

	if ($requested_userID != '')	echo "User ID #".$requested_userID."<input type=\"hidden\" name=\"fUserID\" id=\"fUserID\" value=\"".$requested_userID."\" />\n";
	
	echo "</span></th></tr>\n</thead>\n<tbody>\n";

	if (($requested_userID != '') && ($action == "EDIT")) {

		$sqlQuery	= "SELECT * FROM AppUser "
		            . "WHERE UserID = $requested_userID";
		$rs = $db->Execute($sqlQuery);

		//if the rowcount is not zero it returned rows
		if ($rs->RowCount()) {

			//loop through and output all the rows in the $rs object
			while ($row = $rs->FetchRow()) {
				
				echo "<tr>\n"
					."<td class=\"Title\">First Name: </td><td colspan=\"2\"><input type=\"text\" name=\"fFirstName\" id=\"fFirstName\" value=\"".$row['FirstName']."\" onblur=\"RandomLoginGenerator(document.AdminUserDetails.fUsername)\" /></td>\n"
					."</tr>\n"
					."<tr>\n"
					."<td class=\"Title\">Last Name: </td><td colspan=\"2\"><input type=\"text\" name=\"fLastName\" id=\"fLastName\" value=\"".$row['LastName']."\" onblur=\"RandomLoginGenerator(document.AdminUserDetails.fUsername)\" /></td>\n"
					."</tr>\n"
					."<tr>\n"
					."<td class=\"Title\">Username: </td><td colspan=\"2\"><input type=\"text\" name=\"fUsername\" id=\"fUsername\" value=\"".$row['Username']."\" /></td>\n"
					."</tr>\n"
					."<tr>\n"
					."<td class=\"Title\">Password: </td><td colspan=\"2\"><input type=\"text\" name=\"fPassword\" id=\"fPassword\" value=\"".$row['Password']."\" /></td>\n"
					."</tr>\n"
					."<tr>\n"
					."<td class=\"Title\">Address 1: </td><td colspan=\"2\"><input type=\"text\" name=\"fAddress1\" id=\"fAddress1\" value=\"".$row['Address1']."\" /></td>\n"
					."</tr>\n"
					."<tr>\n"
					."<td class=\"Title\">Address 2: </td><td colspan=\"2\"><input type=\"text\" name=\"fAddress2\" id=\"fAddress2\" value=\"".$row['Address2']."\" /></td>\n"
					."</tr>\n"
					."<tr>\n"
					."<td class=\"Title\">City: </td><td colspan=\"2\"><input type=\"text\" name=\"fCity\" id=\"fCity\" value=\"".$row['City']."\" /></td>\n"
					."</tr>\n"
					."<tr>\n"
					."<td class=\"Title\">Province: </td><td colspan=\"2\"><input type=\"text\" name=\"fProvince\" id=\"fProvince\" value=\"".$row['Province']."\" /></td>\n"
					."</tr>\n"
					."<tr>\n"
					."<td class=\"Title\">Postal Code: </td><td colspan=\"2\"><input type=\"text\" name=\"fPostalCode\" id=\"fPostalCode\" value=\"".$row['PostalCode']."\" /></td>\n"
					."</tr>\n"
					."<tr>\n"
					."<td class=\"Title\">Phone: </td><td colspan=\"2\"><input type=\"text\" name=\"fPrimaryPhone\" id=\"fPrimaryPhone\" value=\"".$row['Phone1']."\" /> Example: <strong>(902) 555-5555</strong></td>\n"
					."</tr>\n"
					//."<tr>\n"
					//."<td class=\"Title\">Secondary Phone: </td><td colspan=\"2\"><input type=\"text\" name=\"fSecondaryPhone\" id=\"fSecondaryPhone\" value=\"".$row['Phone2']."\" /></td>\n"
					//."</tr>\n"
					//."<tr>\n"
					//."<td class=\"Title\">Mobile: </td><td colspan=\"2\"><input type=\"text\" name=\"fMobile\" id=\"fMobile\" value=\"".$row['PhoneCell']."\" /></td>\n"
					//."</tr>\n"
					."<tr>\n"
					."<td class=\"Title\">Email: </td><td colspan=\"2\"><input type=\"text\" name=\"fPrimaryEmail\" id=\"fPrimaryEmail\" value=\"".$row['Email1']."\" /> Example: <strong>you@halifax.ca</strong></td>\n"
					."</tr>\n"
					."<tr>\n"
					."<td class=\"Title\">Date of Birth: </td><td colspan=\"2\"><input class=\"datesel\" type=\"text\" name=\"fDateofBirth\" readonly value=\"".date("m/d/Y", strtotime($row['DateofBirth']))."\" />\n"
					."<img src=\"images/icon_cal.gif\" border=\"0\" onclick=\"displayCalendar(document.forms[0].fDateofBirth, 'mm/dd/yyyy', this)\" onmouseover=\"this.style.cursor='pointer';\">\n"
					."<img src=\"images/refresh.png\" border=\"0\" title=\"Clear\" onclick=\"document.forms[0].fDateofBirth.value = '';\" onmouseover=\"this.style.cursor='pointer';\" />\n"
					."</td>\n"
					."<tr>\n"
					."<td class=\"Title\">Volunteer Since: </td><td colspan=\"2\"><input class=\"datesel\" type=\"text\" name=\"fVolunteerSince\" readonly value=\"".date("m/d/Y", strtotime($row['VolunteerSince']))."\" />\n"
					."<img src=\"images/icon_cal.gif\" border=\"0\" onclick=\"displayCalendar(document.forms[0].fVolunteerSince, 'mm/dd/yyyy', this)\" onmouseover=\"this.style.cursor='pointer';\">\n"
					."<img src=\"images/refresh.png\" border=\"0\" title=\"Clear\" onclick=\"document.forms[0].fVolunteerSince.value = '';\" onmouseover=\"this.style.cursor='pointer';\" />\n"
					."</td>\n"
					//."<tr>\n"
					//."<td class=\"Title\">Secondary Email: </td><td colspan=\"2\"><input type=\"text\" name=\"fSecondaryEmail\" id=\"fSecondaryEmail\" value=\"".$row['Email2']."\" /></td>\n"
					//."</tr>\n"
					//."<tr>\n"
					//."<td class=\"Title\">Text Messaging: </td><td colspan=\"2\"><input type=\"text\" name=\"fTextMessaging\" id=\"fTextMessaging\" value=\"".$row['TextMessage']."\" /></td>\n"
					//."</tr>\n"
					."<tr>\n"
					."<td class=\"Title\">Comment: </td><td colspan=\"2\"><textarea rows=\"5\" name=\"fComments\" id=\"fComments\">".$row['Comment']."</textarea></td>\n"
					."</tr>\n";
					
					if($currentUser_IsAdmin == 1)
					{
						echo "<tr><td class=\"Title\">Category Details: </td><td colspan=\"2\">\n";
						userSections($requested_userID);
					}
					
					echo "</td>\n"
					."</tr>\n"
					."<tr>\n"
					."<td class=\"Title\">Is Active: </td>\n";
					
					if($requested_userID == $current_userID)
					{
						echo "<td>"
						."<img src=\"images\delete.png\" /> WARNING:<br />If you Deactive yourself you will be unable to Reactive yourself or Login onces you Logout.<br />You will need to contact an Administrator to Reactive you."
						."</td>";
					}
					
					echo "<td>";
					
					if ($row['IsActive'] == 0) {
						echo "<input class=\"RadioBoxes\" type=\"radio\" name=\"fIsActive\" id=\"fIsActive\" value=\"1\" />Yes<br />\n"
							."<input class=\"RadioBoxes\" type=\"radio\" name=\"fIsActive\" id=\"fIsActive\" value=\"0\" CHECKED />No\n";
					}
					else {
						echo "<input class=\"RadioBoxes\" type=\"radio\" name=\"fIsActive\" id=\"fIsActive\" value=\"1\" CHECKED />Yes<br />\n"
							."<input class=\"RadioBoxes\" type=\"radio\" name=\"fIsActive\" id=\"fIsActive\" value=\"0\" />No\n";
					}
					
					//echo "<tr><td class=\"Title\">Is Active: </td><td colspan=\"2\"><input type=\"text\" name=\"fIsActive\" id=\"fIsActive\" value=\"".$row['IsActive']."\" /></td></tr>\n";			
					echo "</td>\n"
					."</tr>\n";			

				//echo "<pre>";
				//print_r($row);
				//echo "</pre>";

			}
		}

	}

	elseif (($action == "ADDNEW") && (array_key_exists('fFirstName', $_POST))) {
	
	echo "<tr>\n"
		."<td class=\"Title\">First Name: </td><td colspan=\"2\"><input type=\"text\" name=\"fFirstName\" id=\"fFirstName\" value=\"".$row['FirstName']."\" onblur=\"RandomLoginGenerator(document.AdminUserDetails.fUsername)\" /></td>\n"
		."</tr>\n"
		."<tr>\n"
		."<td class=\"Title\">Last Name: </td><td colspan=\"2\"><input type=\"text\" name=\"fLastName\" id=\"fLastName\" value=\"".$row['LastName']."\" onblur=\"RandomLoginGenerator(document.AdminUserDetails.fUsername)\" /></td>\n"
		."</tr>\n"
		."<tr>\n"
		."<td class=\"Title\">Username: </td><td colspan=\"2\"><input type=\"text\" name=\"fUsername\" id=\"fUsername\" value=\"".$row['Username']."\" /></td>\n"
		."</tr>\n"
		."<tr>\n"
		."<td class=\"Title\">Password: </td><td colspan=\"2\"><input type=\"text\" name=\"fPassword\" id=\"fPassword\" value=\"".$row['Password']."\" /></td>\n"
		."</tr>\n"
		."<tr>\n"
		."<td class=\"Title\">Address 1: </td><td colspan=\"2\"><input type=\"text\" name=\"fAddress1\" id=\"fAddress1\" value=\"".$row['Address1']."\" /></td>\n"
		."</tr>\n"
		."<tr>\n"
		."<td class=\"Title\">Address 2: </td><td colspan=\"2\"><input type=\"text\" name=\"fAddress2\" id=\"fAddress2\" value=\"".$row['Address2']."\" /></td>\n"
		."</tr>\n"
		."<tr>\n"
		."<td class=\"Title\">City: </td><td colspan=\"2\"><input type=\"text\" name=\"fCity\" id=\"fCity\" value=\"".$row['City']."\" /></td>\n"
		."</tr>\n"
		."<tr>\n"
		."<td class=\"Title\">Province: </td><td colspan=\"2\"><input type=\"text\" name=\"fProvince\" id=\"fProvince\" value=\"".$row['Province']."\" /></td>\n"
		."</tr>\n"
		."<tr>\n"
		."<td class=\"Title\">Postal Code: </td><td colspan=\"2\"><input type=\"text\" name=\"fPostalCode\" id=\"fPostalCode\" value=\"".$row['PostalCode']."\" /></td>\n"
		."</tr>\n"
		."<tr>\n"
		."<td class=\"Title\">Phone: </td><td colspan=\"2\"><input type=\"text\" name=\"fPrimaryPhone\" id=\"fPrimaryPhone\" value=\"".$row['Phone1']."\" /> Example: <strong>(902) 555-5555</strong></td>\n"
		."</tr>\n"
		//."<tr>\n"
		//."<td class=\"Title\">Secondary Phone: </td><td colspan=\"2\"><input type=\"text\" name=\"fSecondaryPhone\" id=\"fSecondaryPhone\" value=\"".$row['Phone2']."\" /></td>\n"
		//."</tr>\n"
		//."<tr>\n"
		//."<td class=\"Title\">Mobile: </td><td colspan=\"2\"><input type=\"text\" name=\"fMobile\" id=\"fMobile\" value=\"".$row['PhoneCell']."\" /></td>\n"
		//."</tr>\n"
		."<tr>\n"
		."<td class=\"Title\">Email: </td><td colspan=\"2\"><input type=\"text\" name=\"fPrimaryEmail\" id=\"fPrimaryEmail\" value=\"".$row['Email1']."\" /> Example: <strong>you@halifax.ca</strong></td>\n"
		."</tr>\n"
		."<tr>\n"
		."<td class=\"Title\">Date of Birth: </td><td colspan=\"2\"><input class=\"datesel\" type=\"text\" name=\"fDateofBirth\" readonly value=\"".date("m/d/Y", strtotime($row['DateofBirth']))."\" />\n"
		."<img src=\"images/icon_cal.gif\" border=\"0\" onclick=\"displayCalendar(document.forms[0].fDateofBirth, 'mm/dd/yyyy', this)\" onmouseover=\"this.style.cursor='pointer';\">\n"
		."<img src=\"images/refresh.png\" border=\"0\" title=\"Clear\" onclick=\"document.forms[0].fDateofBirth.value = '';\" onmouseover=\"this.style.cursor='pointer';\"/>\n"
		."</td>\n"
		."</tr>\n"
		."<tr>\n"
		."<td class=\"Title\">Volunteer Since: </td><td colspan=\"2\"><input class=\"datesel\" type=\"text\" name=\"fVolunteerSince\" readonly value=\"".date("m/d/Y", strtotime($row['VolunteerSince']))."\" />\n"
		."<img src=\"images/icon_cal.gif\" border=\"0\" onclick=\"displayCalendar(document.forms[0].fVolunteerSince, 'mm/dd/yyyy', this)\" onmouseover=\"this.style.cursor='pointer';\">\n"
		."<img src=\"images/refresh.png\" border=\"0\" title=\"Clear\" onclick=\"document.forms[0].fVolunteerSince.value = '';\" onmouseover=\"this.style.cursor='pointer';\"/>\n"
		."</td>\n"
		."</tr>\n"
		//."<tr>\n"
		//."<td class=\"Title\">Secondary Email: </td><td colspan=\"2\"><input type=\"text\" name=\"fSecondaryEmail\" id=\"fSecondaryEmail\" value=\"".$row['Email2']."\" /></td>\n"
		//."</tr>\n"
		//."<tr>\n"
		//."<td class=\"Title\">Text Messaging: </td><td colspan=\"2\"><input type=\"text\" name=\"fTextMessaging\" id=\"fTextMessaging\" value=\"".$row['TextMessage']."\" /></td>\n"
		//."</tr>\n"
		."<tr>\n"
		."<td class=\"Title\">Comment: </td><td colspan=\"2\"><textarea rows=\"5\" name=\"fComments\" id=\"fComments\">".$row['Comment']."</textarea></td>\n"
		."</tr>\n"
		."<tr><td class=\"Title\">Category Details: </td><td colspan=\"2\">\n";
		
		userCategories();

		echo "</td></tr>\n<tr><td class=\"Title\">Is Active: </td><td colspan=\"2\">";
		
			/*
			Added by Brandon
			Replaced a text box showing the 'Active' (ie: 1 or 0) status, all other pages have radio boxs, so I added them here to. 
			*/
			if($_POST['fIsActive'] = 0)
			{
				echo "<input class=\"RadioBoxes\" type=\"radio\" name=\"fIsActive\" id=\"fIsActive\" value=\"1\" />Yes<br />\n"
				    ."<input class=\"RadioBoxes\" type=\"radio\" name=\"fIsActive\" id=\"fIsActive\" value=\"0\" CHECKED />No\n";
			}
			else //check if false is returned -Brandon
			{
				echo "<input class=\"RadioBoxes\" type=\"radio\" name=\"fIsActive\" id=\"fIsActive\" value=\"1\" CHECKED />Yes<br />\n"
				    ."<input class=\"RadioBoxes\" type=\"radio\" name=\"fIsActive\" id=\"fIsActive\" value=\"0\" />No\n";
			}
			
		echo "</td></tr>\n"
			."<input type=\"hidden\" name=\"fPassword\" id=\"fPassword\" />\n";

	}
	else {

	echo "<tr>\n"
		."<td class=\"Title\">First Name: </td><td colspan=\"2\"><input type=\"text\" name=\"fFirstName\" id=\"fFirstName\" value=\"".$row['FirstName']."\" onblur=\"RandomLoginGenerator(document.AdminUserDetails.fUsername)\" /></td>\n"
		."</tr>\n"
		."<tr>\n"
		."<td class=\"Title\">Last Name: </td><td colspan=\"2\"><input type=\"text\" name=\"fLastName\" id=\"fLastName\" value=\"".$row['LastName']."\" onblur=\"RandomLoginGenerator(document.AdminUserDetails.fUsername)\" /></td>\n"
		."</tr>\n"
		."<tr>\n"
		."<td class=\"Title\">Username: </td><td colspan=\"2\"><input type=\"text\" name=\"fUsername\" id=\"fUsername\" value=\"".$row['Username']."\" /></td>\n"
		."</tr>\n"
		."<tr>\n"
		."<td class=\"Title\">Password: </td><td colspan=\"2\"><input type=\"text\" name=\"fPassword\" id=\"fPassword\" value=\"".$row['Password']."\" /></td>\n"
		."</tr>\n"
		."<tr>\n"
		."<td class=\"Title\">Address 1: </td><td colspan=\"2\"><input type=\"text\" name=\"fAddress1\" id=\"fAddress1\" value=\"".$row['Address1']."\" /></td>\n"
		."</tr>\n"
		."<tr>\n"
		."<td class=\"Title\">Address 2: </td><td colspan=\"2\"><input type=\"text\" name=\"fAddress2\" id=\"fAddress2\" value=\"".$row['Address2']."\" /></td>\n"
		."</tr>\n"
		."<tr>\n"
		."<td class=\"Title\">City: </td><td colspan=\"2\"><input type=\"text\" name=\"fCity\" id=\"fCity\" value=\"".$row['City']."\" /></td>\n"
		."</tr>\n"
		."<tr>\n"
		."<td class=\"Title\">Province: </td><td colspan=\"2\"><input type=\"text\" name=\"fProvince\" id=\"fProvince\" value=\"".$row['Province']."\" /></td>\n"
		."</tr>\n"
		."<tr>\n"
		."<td class=\"Title\">Postal Code: </td><td colspan=\"2\"><input type=\"text\" name=\"fPostalCode\" id=\"fPostalCode\" value=\"".$row['PostalCode']."\" /></td>\n"
		."</tr>\n"
		."<tr>\n"
		."<td class=\"Title\">Phone: </td><td colspan=\"2\"><input type=\"text\" name=\"fPrimaryPhone\" id=\"fPrimaryPhone\" value=\"".$row['Phone1']."\" /> Example: <strong>(902) 555-5555</strong></td>\n"
		."</tr>\n"
		//."<tr>\n"
		//."<td class=\"Title\">Secondary Phone: </td><td colspan=\"2\"><input type=\"text\" name=\"fSecondaryPhone\" id=\"fSecondaryPhone\" value=\"".$row['Phone2']."\" /></td>\n"
		//."</tr>\n"
		//."<tr>\n"
		//."<td class=\"Title\">Mobile: </td><td colspan=\"2\"><input type=\"text\" name=\"fMobile\" id=\"fMobile\" value=\"".$row['PhoneCell']."\" /></td>\n"
		//."</tr>\n"
		."<tr>\n"
		."<td class=\"Title\">Email: </td><td colspan=\"2\"><input type=\"text\" name=\"fPrimaryEmail\" id=\"fPrimaryEmail\" value=\"".$row['Email1']."\" /> Example: <strong>you@halifax.ca</strong></td>\n"
		."</tr>\n"
		."<tr>\n"
		."<td class=\"Title\">Date of Birth: </td><td colspan=\"2\"><input class=\"datesel\" type=\"text\" name=\"fDateofBirth\" readonly value=\"\" />\n"
		."<img src=\"images/icon_cal.gif\" border=\"0\" onclick=\"displayCalendar(document.forms[0].fDateofBirth, 'mm/dd/yyyy', this)\" onmouseover=\"this.style.cursor='pointer';\">\n"
		."<img src=\"images/refresh.png\" border=\"0\" title=\"Clear\" onclick=\"document.forms[0].fDateofBirth.value = '';\" onmouseover=\"this.style.cursor='pointer';\" />\n"
		."</td>\n"
		."<tr>\n"
		."<td class=\"Title\">Volunteer Since: </td><td colspan=\"2\"><input class=\"datesel\" type=\"text\" name=\"fVolunteerSince\" readonly value=\"\" />\n"
		."<img src=\"images/icon_cal.gif\" border=\"0\" onclick=\"displayCalendar(document.forms[0].fVolunteerSince, 'mm/dd/yyyy', this)\" onmouseover=\"this.style.cursor='pointer';\">\n"
		."<img src=\"images/refresh.png\" border=\"0\" title=\"Clear\" onclick=\"document.forms[0].fVolunteerSince.value = '';\" onmouseover=\"this.style.cursor='pointer';\" />\n"
		."</td>\n"
		//."<tr>\n"
		//."<td class=\"Title\">Secondary Email: </td><td colspan=\"2\"><input type=\"text\" name=\"fSecondaryEmail\" id=\"fSecondaryEmail\" value=\"".$row['Email2']."\" /></td>\n"
		//."</tr>\n"
		//."<tr>\n"
		//."<td class=\"Title\">Text Messaging: </td><td colspan=\"2\"><input type=\"text\" name=\"fTextMessaging\" id=\"fTextMessaging\" value=\"".$row['TextMessage']."\" /></td>\n"
		//."</tr>\n"
		."<tr>\n"
		."<td class=\"Title\">Comment: </td><td colspan=\"2\"><textarea rows=\"5\" name=\"fComments\" id=\"fComments\">".$row['Comment']."</textarea></td>\n"
		."</tr>\n"
		."<tr><td class=\"Title\">Category Details: </td><td colspan=\"2\">\n";
			
		//userCategories();				
		userSections($requested_userID);
		
		echo "</td></tr>\n<tr><td class=\"Title\">Is Active: </td><td colspan=\"2\">"
			."<input class=\"RadioBoxes\" type=\"radio\" name=\"fIsActive\" id=\"fIsActive\" value=\"1\" CHECKED />Yes<br />\n"
			."<input class=\"RadioBoxes\" type=\"radio\" name=\"fIsActive\" id=\"fIsActive\" value=\"0\" />No\n"
			."</td></tr>\n"
			."<input type=\"hidden\" name=\"fPassword\" id=\"fPassword\" />\n";
	}

	echo "<tr>\n"
	   . "<td class=\"Title\">&nbsp;</td>\n"
	   . "<td colspan=\"2\">\n"
	   . "<input class=\"Buttons\" type=\"button\" onclick=\"history.back();\" value=\"&laquo; Cancel\" /><input class=\"Buttons\" type=\"submit\" value=\"Submit &raquo;\" name=\"submitform\" /></td>\n"
	   . "</tr>\n";
	 /* 
	if($action != "DELETE" && $action != "ADDNEW")
	{
		if($userID == 100)
		{		
			echo "<tr>\n"
	           . "<td class=\"Title\">&nbsp;</td>\n"
			   . "<td><img src=\"images/delete.png\" title=\"Cannot Delete\" />This User cannot be Deleted.</td>\n"
			   . "<td align=\"center\"><input class=\"Buttons\" type=\"delete\" value=\"Delete &raquo;\" disabled=\"disabled\"  /></td>\n";
		}
		else
		{
			echo "<tr>\n"
	           . "<td class=\"Title\">&nbsp;</td>\n" 
			   . "<td><img src=\"images/delete.png\" title=\"WARNING\" />WARNING:<br>Deleting a User will <u><strong>permanently</strong></u> delete them from the Database. This process cannot be undone.</td>\n"
			   . "<td align=\"center\"><input class=\"Buttons\" type=\"delete\" value=\"Delete &raquo;\" onclick=\"location.href='adminUserDetails.php?userID=$userID&action=DELETE';return false\"/></td>\n";
		}
	}
	*/
	echo "</tr>\n"
	   . "</tbody></table></form>\n";

	$db->Close();
}



function newsDetails($userID, $newsID, $action, $post)
{
	global $db;
	connectDB();
	
	$currentDate = date("m/d/Y");
	
	$sqlQuery = "SELECT * "
		      . "FROM vAppAdmin "
		      . "WHERE UserID = $userID";
	$rs = $db->Execute($sqlQuery);
	$row = $rs->FetchRow();
	
	switch($action) {
		case "ADDNEW":
			$pageTitle = "Add News Post";
			$submitStatus = "Added";
			break;
	}
	
	if($action == "ADDNEW")
	{
		echo "<table class=\"SearchAndAlert\">\n"
		   . "<tr>\n"
		   . "<td class=\"Action\">".$pageTitle."</td>\n"
		   . "</tr>\n"
		   . "</table>\n"
		   . "<form action=\"news.php?action=".$action."&status=".$submitStatus."\" method=\"post\" name=\"AdminUserDetails\" id=\"AdminUserDetails\" onsubmit=\"return validateTheForm(this);\">"
		   . "<input name=\"fuserID\" type=\"hidden\" value=\"".$userID."\" />"
		   . "<table cellspacing=\"0\" border=\"0\" width=\"100%\" id=\"userTable\">\n"
		   . "<thead>\n"
		   . "<tr>\n"
		   . "<th>Item</th>\n"
		   . "<th>Description</th>\n"
		   . "<th><span style=\"float:right;\">\n";
		
		echo "</span></th></tr>\n</thead>\n<tbody>\n";
		
	  //echo "<tr>\n"
		 //. "<td class=\"Title\">Post Title: </td><td colspan=\"2\"><input type=\"text\" name=\"fPostTitle\" id=\"fPostTitle\" value=\"\"/></td>\n"
		 //. "</tr>\n";
		echo "<tr>\n"
		   . "<td class=\"Title\">Content: </td><td colspan=\"2\"><textarea rows=\"5\" name=\"fContent\" id=\"fContent\"></textarea></td>\n"
		   . "</tr>\n"
		   . "<tr>\n"
		   . "<td class=\"Title\">Post By: </td><td colspan=\"2\"><input type=\"text\" name=\"fPostedBy\" id=\"fPostedBy\" value=\"" . $row['FirstName'] . " " . $row['LastName']."\" readonly/></td>\n"
		   . "</tr>\n"
		   . "<tr>\n"
		   . "<td class=\"Title\">Post On: </td><td colspan=\"2\"><input type=\"text\" name=\"fPostedOn\" id=\"fPostedOn\" value=\"" . $currentDate . "\" readonly/></td>\n"
		   . "</tr>\n"
		   ."<tr><td class=\"Title\">Post In: </td><td colspan=\"2\">\n";
		   
		   userNewsSection($userID);
			
		echo "</td></tr>\n<tr>\n"
	       . "<td class=\"Title\">&nbsp;</td>\n"
	       . "<td colspan=\"2\">\n"
	       . "<input class=\"Buttons\" type=\"button\" onclick=\"history.back();\" value=\"&laquo; Cancel\" /><input class=\"Buttons\" type=\"submit\" value=\"Submit &raquo;\" name=\"submitform\" /></td>\n"
	       . "</tr>\n";	
		   	   
		echo "</tr>\n"
		   . "</tbody></table></form>\n";
		   
		
		//postNews();
	}
	elseif($action == "PREVIEW")
	{		
		echo "<table class=\"SearchAndAlert\">\n"
		   . "<tr>\n"
		   . "<td class=\"Action\">News Posted!</td>\n"
		   . "</tr>\n"
		   . "</table>\n"
		   . "<div class=\"newsItem\" style=\"min-width: 282px\">\n"
		   . "<div class=\"dateContainer\"><div class=\"month\">". date("M", strtotime($post['fPostedOn'])) ."</div>\n"
		   . "<div class=\"day\">". date("d", strtotime($post['fPostedOn'])) ."</div>\n"
		   . "</div>\n"
		   . "<div class=\"slugline\">Posted in News Post Preview by ". $post['fPostedBy']  ."</div>\n"
		   . "<div class=\"newsText\">". $post['fContent'] ."</div>\n"
		   . "</div>\n";
	}
	
	
	$db->Close();
}

/*

//ouput the first field in the row
$db->SetFetchMode(ADODB_FETCH_ASSOC);
echo $row['UserID'];

//set the fetch mode to numeric
$db->SetFetchMode(ADODB_FETCH_NUM);
echo $row[0];

*/

function getUserFormData() 
{
	
	$FirstName		= $_POST['fFirstName'];
	$LastName		= $_POST['fLastName'];
	$UserName		= $_POST['fUsername'];
	$Password		= $_POST['fPassword'];
	$Address1		= $_POST['fAddress1'];

	$Address2		= $_POST['fAddress2'];
	$City			= $_POST['fCity'];
	$Province		= $_POST['fProvince'];
	$PostalCode		= $_POST['fPostalCode'];
	$PrimaryPhone	= $_POST['fPrimaryPhone'];

	$SecondaryPhone	= $_POST['fSecondaryPhone'];
	$Mobile			= $_POST['fMobile'];
	$PrimaryEmail	= $_POST['fPrimaryEmail'];
	$SecondaryEmail	= $_POST['fSecondaryEmail'];
	$TextMessaging	= $_POST['fTextMessaging'];

	$Comments		= str_replace("'","",strip_tags($_POST['fComments']));
	$Section		= $_POST['fSection'];
	$IsActive		= $_POST['fIsActive'];
	$UserID			= $_POST['fUserID'];
	
	$DateofBirth	= $_POST['fDateofBirth'];
	$VolunteerSince	= $_POST['fVolunteerSince'];

	$formValues = array($FirstName, $LastName, $UserName, $Password, $Address1, $Address2, $City, $Province, $PostalCode, $PrimaryPhone, $SecondaryPhone, $Mobile, $PrimaryEmail, $SecondaryEmail, $TextMessaging, $Comments, $IsActive, $UserID, $DateofBirth, $VolunteerSince);
	return $formValues;
}

function getUserSectionAndAdminData($userID, $post, $action, $status) 
{
	global $db;
	connectDB();
	
	$sectionStatus	= array();
	$adminStatus	= array();
	
	$sqlQuery = "SELECT * "
	          . "FROM AppSection";
	$rs = $db->Execute($sqlQuery);

	while($row = $rs->FetchRow())
	{		
		$sectionName = str_replace(" ", "", $row['SectionName']);
		$sectionName = str_replace("(", "", $sectionName);
		$sectionName = str_replace(")", "", $sectionName);
		
		foreach($post as $name => $value)
		{	
			if($name == "f" . $sectionName)
			{			
				$sectionStatus[$row['SectionID']] = $row['SectionID'];
			}
				
			if($name == "f" . $sectionName . "IsAdmin")
			{			
				$adminStatus[$row['SectionID']] = $value;
			}
		}
	}
	
	if($action == "EDIT" && $status = "Updated") 
	{
		$sqlQuery = "DELETE FROM AppAdmin "
			      . "WHERE UserID = " . $userID;  
		$rs2 = $db->Execute($sqlQuery);
	}
	
	for($i = 1; $i <= $rs->RowCount(); $i++)
	{
	
		if(empty($sectionStatus[$i]))
		{
			$sectionStatus[$i] = 0;
		}
		
		if(empty($adminStatus[$i]))
		{
			$adminStatus[$i] = 0;
		}
		
		if($action == "ADDNEW")
		{
			if($sectionStatus[$i] != 0)
			{
				$sqlQuery = "INSERT INTO AppAdmin "
						  . "(UserID, SectionID, IsAdmin) "
						  . "VALUES (" . $userID . ", " . $sectionStatus[$i] . ", " . $adminStatus[$i] . ")";
				$rs3 = $db->Execute($sqlQuery);
			}
		}	
		elseif($action == "EDIT")
		{	
			if($sectionStatus[$i] != 0)
			{  
				$sqlQuery = "INSERT INTO AppAdmin "
						  . "(UserID, SectionID, IsAdmin) "
						  . "VALUES (" . $userID . ", " . $sectionStatus[$i] . ", " . $adminStatus[$i] . ")";
				$rs3 = $db->Execute($sqlQuery);  
			}	
		}
	}
	
	$db->Close();
}

function getNewsPostData()
{	
	$PostByID 		= $_POST['fuserID'];
	$Content 		= $_POST['fContent'];
	
	$formValues = array($PostByID, $Content);
	return $formValues;
}


function checkIfUserExists($firstName, $lastName, $email) {

	global $db;
	connectDB();
	$userExists = false;

	$sqlQuery	= "SELECT * FROM AppUser WHERE (Email1='$email') OR ((FirstName='$firstName') AND (LastName='$lastName'))";
	$rs = $db->Execute($sqlQuery);

	//if the rowcount is not zero it returned rows
	if ($rs->RowCount()) {
		$userExists = true;
	}
	
	$db->Close();
	return $userExists;
		
}

function updateUsers($action, $formValues) {
	global $db, $currentUserID;

	$sqlQuery = "";

	switch ($action) {
	case "ADDNEW":
		if(checkIfUserExists($formValues[0], $formValues[1], $formValues[12]) === false) 
		{
			$sqlQuery = "INSERT INTO AppUser "
				      . "(FirstName, LastName, Username, Password, Address1, Address2, City, Province, PostalCode, Phone1, Phone2, PhoneCell, Email1, Email2, TextMessage, Comment, IsActive, DateofBirth, VolunteerSince) "
				      . "VALUES ('$formValues[0]', '$formValues[1]', '$formValues[2]', '$formValues[3]', '$formValues[4]', '$formValues[5]', '$formValues[6]', '$formValues[7]', '$formValues[8]', '$formValues[9]', '$formValues[10]', '$formValues[11]', '$formValues[12]', '$formValues[13]', '$formValues[14]', '$formValues[15]', $formValues[16], $formValues[18], $formValues[19])";
			
			$output = $formValues[0]." ".$formValues[1]." has been added to the system !";

		}
		else
		{	
			$output = "Oops! Either '<u>".$formValues[0]." ".$formValues[1]."</u>' already exists or someone is already using '<u>".$formValues[12]."</u>' as their primary email address !";
		}
		break;

	case "EDIT":
		//if(checkIfUserExists($formValues[0], $formValues[1], $formValues[12]) === false) {
			$sqlQuery = "UPDATE AppUser "
			          . "SET FirstName='$formValues[0]', LastName='$formValues[1]', Username='$formValues[2]', Password='$formValues[3]', Address1='$formValues[4]', Address2='$formValues[5]', City='$formValues[6]', Province='$formValues[7]', PostalCode='$formValues[8]', Phone1='$formValues[9]', Phone2='$formValues[10]', PhoneCell='$formValues[11]', Email1='$formValues[12]', Email2='$formValues[13]', TextMessage='$formValues[14]', Comment='$formValues[15]', IsActive=$formValues[16], DateofBirth='$formValues[18]', VolunteerSince='$formValues[19]' "
				      . "WHERE UserID = $formValues[17]";
		//}
		
		//echo $sqlQuery;

		$output = $formValues[0]." ".$formValues[1]."'s profile has been updated !";
		break;
	
	case "DELETE":
		$sqlQuery = "DELETE FROM AppUser WHERE UserID = $formValues[17]";
		
		$output = "user deleted";
		break;
	}
	
	if($sqlQuery != "") {
		connectDB();
		$rs1 = $db->Execute($sqlQuery);
		$db->Close();
	}

	return $output;
}

function updateNews($action, $post, $formValues) 
{
	global $db;
	connectDB();
	
	$sectionStatus	= array();
	
	$sqlQuery = "SELECT * "
	          . "FROM AppSection";
	$rs = $db->Execute($sqlQuery);

	while($row = $rs->FetchRow())
	{		
		$sectionName = str_replace(" ", "", $row['SectionName']);
		$sectionName = str_replace("(", "", $sectionName);
		$sectionName = str_replace(")", "", $sectionName);
		
		foreach($post as $name => $value)
		{	
			if($name == "f" . $sectionName)
			{			
				$sectionStatus[$row['SectionID']] = $row['SectionID'];
			}
		}
	}

	if($action == "ADDNEW")
	{
		for($i = 1; $i <= $rs->RowCount(); $i++)
		{		
			if(empty($sectionStatus[$i]))
			{
				$sectionStatus[$i] = 0;
			}
			
			if($sectionStatus[$i] != 0)
			{
				$sqlQuery = "INSERT INTO AppNews "
						  . "(SectionID, PostedByID, NewsText) "
						  . "VALUES (" . $sectionStatus[$i] . ", $formValues[0], '$formValues[1]')";
				$rs2 = $db->Execute($sqlQuery);
			}
		}
		
		$output = "PREVIEW";
	}
	
	$db->Close();
	return $output;
}

function userCategories() {

	global $db;
	connectDB();

	$sqlQuery = "SELECT * FROM AppSection "
	          . "ORDER BY SectionName ASC";

	$rs = $db->Execute($sqlQuery);

	if ($rs->RowCount()) {

		echo "<select name=\"fSection\" id=\"fSection\"><option value=\"0\">Assign this user a section...</option>\n<option value=\"0\">-------------------</option>\n";

		while ($row = $rs->FetchRow()) {
			//if ($currentCat == $row['SectionID']) {
			//	echo "<option SELECTED value=\"". $row['SectionID'] ."\">". $row['SectionName'] ."</option>\n";
			//} else {
				echo "<option value=\"". $row['SectionID'] ."\">". $row['SectionName'] ."</option>\n";
			//}
		}

		echo "</select>";
	
	}
	
	$db->Close();

}



function userSections($userID) {

	global $db;
	connectDB();
	$checkIt = "";

	$sqlQuery = "SELECT * FROM AppSection ORDER BY SectionID ASC";

	$rs = $db->Execute($sqlQuery);

	if ($rs->RowCount()) {

		//echo "<div class=\"SectionList\">";
		$i = 1;

		echo "<table class=\"SectionList\">\n";

		while ($row = $rs->FetchRow()) {
			//echo $i;
			
			$sectionName = str_replace(" ","",$row['SectionName']);
			$sectionName = str_replace("(","",$sectionName);
			$sectionName = str_replace(")","",$sectionName);
			$sectionName = "f".$sectionName;

			$checkIt = getUserSectionAccess($userID, $row['SectionID'], "SECTION");
			echo "<tr>\n<td style=\"width:250px;\"><input type=\"checkbox\" class=\"CheckBoxes\" name=\"". $sectionName ."\"  id=\"". $sectionName ."\" value=\"1\" $checkIt /><span>" . $row['SectionName'] . "</span></td>\n";

			$sectionNameAdmin = $sectionName . "IsAdmin";
			
			$checkIt = getUserSectionAccess($userID, $row['SectionID'], "ADMIN");
			echo "<td><input type=\"checkbox\" class=\"CheckBoxes\" name=\"". $sectionNameAdmin ."\" id=\"". $sectionNameAdmin ."\" value=\"1\" $checkIt /><span>Is Admin</span></td>\n</tr>\n";

			$checkIt = "";
			$i++;	
			
		}
		
		echo "</table>";
	
		//echo "</div>";
	
	}
	
	$db->Close();

}



function userNewsSection($userID)
{
	global $db;
	connectDB();
	//$checkIt = "";

	$sqlQuery = "SELECT * "
			  . "FROM AppSection "
			  . "INNER JOIN AppAdmin "
			  . "ON AppSection.SectionID = AppAdmin.SectionID "
			  . "WHERE AppAdmin.UserID = $userID "
			  . "ORDER BY AppSection.SectionName ASC";
	$rs = $db->Execute($sqlQuery);

	if ($rs->RowCount()) {

		//echo "<div class=\"SectionList\">";
		$i = 1;

		echo "<table class=\"SectionList\">\n";

		while ($row = $rs->FetchRow()) {
			//echo $i;
			
			$sectionName = str_replace(" ","",$row['SectionName']);
			$sectionName = str_replace("(","",$sectionName);
			$sectionName = str_replace(")","",$sectionName);
			$sectionName = "f".$sectionName;

			//$checkIt = getUserSectionAccess($userID, $row['SectionID'], "SECTION");
			echo "<tr>\n<td style=\"width:250px;\"><input type=\"checkbox\" class=\"CheckBoxes\" name=\"". $sectionName ."\"  id=\"". $sectionName ."\" value=\"1\" /><span>" . $row['SectionName'] . "</span></td>\n";

			//$checkIt = "";
			$i++;	
			
		}
		
		echo "</table>";
	
		//echo "</div>";
	
	}
	
	$db->Close();
}




function getUserSectionAccess($userID, $sectionID, $lookAt) {

	global $db;
	connectDB();
	$output = "";

	$sqlQuery = "SELECT SectionID, IsAdmin FROM AppAdmin WHERE UserID = $userID";
	$rs = $db->Execute($sqlQuery);

	if ($rs->RowCount()) {
		while ($row = $rs->FetchRow()) {
			
			switch ($lookAt) {

				case "SECTION":
					if($row['SectionID'] == $sectionID)	$output = "checked=\"checked\"";
					break;

				case "ADMIN":
					if($row['SectionID'] == $sectionID && $row['IsAdmin'] == 1)	$output = "checked=\"checked\"";
					break;
			}

		}
	}
	
	$db->Close();

	return $output;

}







?>

