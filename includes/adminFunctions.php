<?php

function userIsAdmin($userID)
{
	global $db;
	connectDB();
		
	$sqlQuery = "SELECT IsAdmin, UserID "
			  . "FROM vAppAdmin "
			  . "WHERE UserID = $userID "
			  . "AND IsAdmin = 1";			
	$rs = $db->Execute($sqlQuery);
	
	if ($rs->RowCount()) 
	{
		return true;
	}
	else
	{
		return false;
	}
	
	$db->Close();
}


function userIsSuperAdmin($userID)
{
	global $db;
	connectDB();
		
	$sqlQuery = "SELECT IsSuperAdmin, UserID "
			  . "FROM vAppAdmin "
			  . "WHERE UserID = $userID AND IsSuperAdmin = 1";	
			  
	$rs = $db->Execute($sqlQuery);
	
	if ($rs->RowCount()) 
	{
		return true;
	}
	else
	{
		return false;
	}
	
	$db->Close();
}


function userIsAdminForShifts($userID,$sectionID)
{
	global $db;
	connectDB();
		
	$sqlQuery = "SELECT IsAdmin, UserID "
			  . "FROM vAppAdmin "
			  . "WHERE UserID = $userID "
			  . "AND SectionID = $sectionID"
			  . "AND IsAdmin = 1";			
	$rs = $db->Execute($sqlQuery);
	
	if ($rs->RowCount()) 
	{
		return true;
	}
	else
	{
		return false;
	}
	
	$db->Close();
}

function userIsLeadForUser($leadID,$userID,$sectionID)
{
    global $db;
    connectDB();

    $sqlQuery = "SELECT AppLeadId "
        . "FROM AppLeads "
        . "WHERE UserId = $userID "
        . "AND SectionId = $sectionID"
        . "AND LeadId = $leadID";

    $rs = $db->Execute($sqlQuery);

    if ($rs->RowCount())
    {
        return true;
    }
    else
    {
        return false;
    }

    $db->Close();
}



function userBelongsTo($userID) {
	
	global $db;
	connectDB();
	
	$output[] = "";
	
	// Determine which sections the user belongs to.
	$sqlQuery = "SELECT SectionID "
			  . "FROM vAppAdmin "
			  . "WHERE (UserID = '" . $userID . "')";

	$rs = $db->Execute($sqlQuery);
	
	if ($rs->RowCount()) {
		
		$i = 0;
		
		while($row = $rs->FetchRow()) {
			
			$output[$i] = $row['SectionID'];
			$i++;
			
		}
		
	}
	
	return $output;

}


function sectionLinks($userID) {
	
	global $db;
	connectDB();
	
	$sections = userBelongsTo($userID);
	
	// ADDED
	$output .= "<form id=\"SectionSort\" name=\"SectionSort\" style=\"display:inline;\">\n"
			. "<select onchange=\"if(options[selectedIndex].value) window.location.href= (options[selectedIndex].value)\">\n"
			. "<option value=\"\" selected>-</option>\n"
			. "<option value=\"".$_SERVER['SCRIPT_NAME']."\">All</option>\n";	
	
	foreach($sections as $sectionNum) {
			
		// Determine which sections the user belongs to.
		$sqlQuery = "SELECT SectionName "
				  . "FROM AppSection "
				  . "WHERE SectionID = " . $sectionNum . "";
	
		$rs = $db->Execute($sqlQuery);
		
		if ($rs->RowCount()) {
					
			while($row = $rs->FetchRow()) {
				
				$output .= "<option value=\"".$_SERVER['SCRIPT_NAME']."?section=".$sectionNum."\">".$row['SectionName']."</option>\n";
				
			}
			
		}		
		
	}
	
	// ADDED
	$output .= "</select>\n</form>\n";

	return $output;	

}

function sectionLinksWithoutAll($userID,$sectionId) {

    global $db;
    connectDB();

    $sections = userBelongsTo($userID);

    // ADDED
    $output .= "<form id=\"SectionSort\" name=\"SectionSort\" style=\"display:inline;\">\n"
        . "<select id=\"slSection\" name=\"slSection\" onchange=\"slSectionOnChange()\">\n"
        . "<option value=\"\">Select</option>\n";

    foreach($sections as $sectionNum) {

        // Determine which sections the user belongs to.
        $sqlQuery = "SELECT SectionName "
            . "FROM AppSection "
            . "WHERE SectionID = " . $sectionNum . "";

        $rs = $db->Execute($sqlQuery);

        if ($rs->RowCount()) {

            while($row = $rs->FetchRow()) {
                if ($sectionId == $sectionNum) {
                    //$output .= "<option selected value=\"" . $_SERVER['SCRIPT_NAME'] . "?section=" . $sectionNum . "\">" . $row['SectionName'] . "</option>\n";
                    $output .= "<option selected value=\"" . $sectionNum . "\">" . $row['SectionName'] . "</option>\n";
                }
                else {
                    //$output .= "<option value=\"" . $_SERVER['SCRIPT_NAME'] . "?section=" . $sectionNum . "\">" . $row['SectionName'] . "</option>\n";
                    $output .= "<option value=\"" . $sectionNum . "\">" . $row['SectionName'] . "</option>\n";
                }

            }

        }

    }

    // ADDED
    $output .= "</select>\n</form>\n";

    return $output;

}


function sectionName($sectionID) {
	
	global $db;
	connectDB();
	
	$output = "";
	
	// Determine which sections the user belongs to.
	$sqlQuery = "SELECT SectionName "
			  . "FROM vAppSectionNames "
			  . "WHERE SectionID = " . $sectionID . "";

	$rs = $db->Execute($sqlQuery);
	
	if ($rs->RowCount()) {
		
		while($row = $rs->FetchRow()) {
			
			$output = $row['SectionName'];

		}
		
	}
	
	return $output;

}




function printUsers($userID) 
{
	global $db;
	connectDB();
	
	echo "<table class=\"SearchAndAlert\"><tr>"
	   ."<td valign=\"middle\" class=\"Action\">Manage Users</td>"
	   ."<td valign=\"middle\" class=\"sortBySection\" ><b>Sort by Section:</b> ".sectionLinks($userID)."</td>"
	   . "<td class=\"SearchBox\"><form id=\"AdminUserListSearch\" name=\"SearchAdminUserList\" method=\"get\" action=\"AdminUserList.php\">"
	   . "<input type=\"text\" name=\"filter\" id=\"filter\" value=\"Search for a user...\" onfocus=\"this.value=''\" />"
	   . "<input src=\"images/magnifier.png\" class=\"SearchButton\" value=\"Search\" type=\"image\" id=\"Search\" title=\"Search\" />"
	   . "<img src=\"images/refresh.png\" class=\"RefreshButton\" title=\"Clear Results\" onclick=\"window.location='AdminUserList.php'\" />"
	   . "</form></td></tr></table>"
	   . "<table cellspacing=\"0\" border=\"0\" width=\"100%\" id=\"userTable\">\n"
	   . "<thead>"
	   . "<tr>"
       . "<th>Name</th>"
	   . "<th width=\"200px\">Section</th>"
       . "<th width=\"200px\">Division</th>"
	   . "<th>Email</th>"
	   . "<th>Phone Number</th>"
	   . "</tr>"
	   . "</thead>"
	   . "<tbody>\n";

	// Determine which sections the user belongs to.
	$sqlQuery = "SELECT UserID, SectionID, IsSuperAdmin "
			  . "FROM vAppAdmin "
			  . "WHERE UserID = " . $userID . "";
	$rs = $db->Execute($sqlQuery);

	// Initialize counter.
	$i = 0;
	
	// If a user with that id exist.
	if ($rs->RowCount()) {
		
		// Begin assembling SQL query based on the following criterial...
		$sqlQuery = "SELECT * "
				  . "FROM vAppAdmin "
				  . "WHERE ";
				  
		// Add sections that the user has access to to the SQL query.
		while($row = $rs->FetchRow()) {
			
			// If user belongs to only one section. (First time through / Default)
			if($i < 1) {
				$sqlQuery .= "(SectionID = " . $row['SectionID'] . "";
			
			// If user belongs to more than one section.
			} else {
				$sqlQuery .= " OR SectionID = " . $row['SectionID'] . ""; //Adds this once $i has counted over 1
			}
			
			// increment counter.
			$i++;
			
			$isSuperAdmin = $row['IsSuperAdmin'];
			
		}
		
		if($isSuperAdmin == 1) {
			$sqlQuery .= ") ORDER BY IsActive DESC, FirstName, LastName, SectionName ASC";	
		
		} else {
			$sqlQuery .= ") AND (IsActive = 1) ORDER BY FirstName, LastName, SectionName ASC";	
		}
							
		$rs = $db->Execute($sqlQuery);
		
		$i = 0; //Define $i as 0
		$count = 0; //Define $count as 0
		
		$currentUserInLoop 	= array();
		$sectionNames 		= "";
		$userID_Prev 		= 0;
		$isInActiveFlag 	= 1;
		
		//While Rows are Returned
		while($row = $rs->FetchRow()) 
		{

			if($row['UserID'] == $userID_Prev)
			{
			
				$userID_Prev 		= $row['UserID'];
				$userName_Prev 		= $row['FirstName'] . " " . $row['LastName'];
				$email_Prev 		= $row['Email1'];
				$phone_Prev 		= $row['Phone1'];
				$isactive_prev		= $row['IsActive'];
				$divisionName       = $row['DivisionName'];
				
				if($isactive_prev == 0)
				{
					$isactive_prev = "Inactive";
				}
				elseif($isactive_prev == 1)
				{
					$isactive_prev = "Active";
				}
							
				if($sectionNames == "") {
					$sectionNames = $row['SectionName'];
				} else {
					$sectionNames = $sectionNames . "<br/>" . $row['SectionName'];
				}
			
			} else {

				//If $username_Prev comes back as true containing text
				if($userName_Prev)
				{
				
					if($isactive_prev == 0 && isInActiveFlag == 1){
						echo "<tr style='border-bottom:1px solid grey'>";
					} else {
						echo "<tr>";	
					}
				
					echo "<tr>";
					//Check if the current logged in user is the current user in the loop
					if($userID_Prev == $userID)
					{
						echo "<td><img src=\"images/status/" . $isactive_prev . ".png\" title=\"Account is: " . $isactive_prev . "\" /><a href=\"AdminUserDetails.php?userID=" . $userID_Prev . "&action=EDIT\" title=\"Edit " . $userName_Prev . "'s profile\"><strong>" . $userName_Prev . "</strong></a></td>";
					}
					else
					{
						if(userIsAdmin($userID))
						{
							echo "<td><img src=\"images/status/" . $isactive_prev . ".png\" title=\"Account is: " . $isactive_prev . "\" /><a href=\"AdminUserDetails.php?userID=" . $userID_Prev . "&action=EDIT\" title=\"Edit " . $userName_Prev . "'s profile\">" . $userName_Prev . "</a></td>";
						}
						else
						{
							echo "<td><img src=\"images/status/" . $isactive_prev . ".png\" title=\"Account is: " . $isactive_prev . "\" />" . $userName_Prev . "</td>";
						}
					}
					
					echo "<td>" . $sectionNames . "</td>";

                    echo "<td>" . $divisionName . "</td>";
					
					//Search for the '@' symbol to check if the current email is a valid email and not just text.
					if(strpbrk($email_Prev, "@"))
					{
						echo "<td><a href=\"mailto:" . $email_Prev . "?subject=HRP Volunteer Calendar\" title=\"Send " . $userName_Prev . " an email\">" . $email_Prev . "</a></td>";
					}
					else
					{
						echo "<td>N/A</td>";
					}
					
					if($phone_Prev == "")
					{
						echo "<td width=\"110px\">N/A</td>";

					}
					else
					{
						echo "<td width=\"110px\">" . $phone_Prev . "</td>";
					}
					
					echo "</tr>\n";
				}	
			
				$userID_Prev 		= $row['UserID'];
				$userName_Prev 		= $row['FirstName'] . " " . $row['LastName'];
				$email_Prev 		= $row['Email1'];
				$phone_Prev 		= $row['Phone1'];		
				$sectionNames 		= $row['SectionName'];
				$isactive_prev		= $row['IsActive'];
                $divisionName       = $row['DivisionName'];
				
				if($isactive_prev == 0)
				{
					$isactive_prev = "Inactive";
				}
				elseif($isactive_prev == 1)
				{
					$isactive_prev = "Active";
				}
			}
			
			$count++; //$count + 1
			$i++; //$i + 1
		}
		
		//If $username_Prev comes back as true containing text
		if($userName_Prev)
		{
		
			echo "<tr>";
			//Check if the current logged in user is the current user in the loop
			if($userID_Prev == $userID)
			{
				echo "<td><img src=\"images/status/" . $isactive_prev . ".png\" title=\"Account is: " . $isactive_prev . "\" /><a href=\"AdminUserDetails.php?userID=" . $userID_Prev . "&action=EDIT\" title=\"Edit " . $userName_Prev . "'s profile\"><strong>" . $userName_Prev . "</strong></a>";
			}
			else
			{
				if(userIsAdmin($userID))
				{
					echo "<td><img src=\"images/status/" . $isactive_prev . ".png\" title=\"Account is: " . $isactive_prev . "\" /><a href=\"AdminUserDetails.php?userID=" . $userID_Prev . "&action=EDIT\" title=\"Edit " . $userName_Prev . "'s profile\">" . $userName_Prev . "</a>";
				}
				else
				{
					echo "<td><img src=\"images/status/" . $isactive_prev . ".png\" title=\"Account is: " . $isactive_prev . "\" />" . $userName_Prev . "";
				}
			}

			
			echo "<td width=\"130\">" . $sectionNames . "</td>";
            echo "<td>" . $divisionName . "</td>";
			
			//Search for the '@' symbol to check if the current email is a valid email and not just text.
			if(strpbrk($email_Prev, "@"))
			{
				echo "<td><a href=\"mailto:" . $email_Prev . "?subject=HRP Volunteer Calendar\" title=\"Send " . $userName_Prev . " an email\">" . $email_Prev . "</a></td>";
			}
			else
			{
				echo "<td>N/A</td>";
			}
			
			if($phone_Prev == "")
			{
				echo "<td width=\"110px\">N/A</td>";

			}
			else
			{
				echo "<td width=\"110px\">" . $phone_Prev . "</td>";
			}
			
			echo "</tr>\n";
		}
	
	}
	
	echo "</tbody></table>\n";
	
	$db->Close();
}






function printUsersBySection($userID, $sectionID) 
{
	global $db;
	connectDB();
	
	echo "<table class=\"SearchAndAlert\"><tr>"
	   ."<td valign=\"middle\" class=\"Action\">Manage Users</td>"
	   ."<td valign=\"middle\" class=\"sortBySection\" ><b>Sort by Section:</b> ".sectionLinks($userID)."</td>"
	   . "<td class=\"SearchBox\"><form id=\"AdminUserListSearch\" name=\"SearchAdminUserList\" method=\"get\" action=\"AdminUserList.php\">"
	   . "<input type=\"text\" name=\"filter\" id=\"filter\" value=\"Search for a user...\" onfocus=\"this.value=''\" />"
	   . "<input src=\"images/magnifier.png\" class=\"SearchButton\" value=\"Search\" type=\"image\" id=\"Search\" title=\"Search\" />"
	   . "<img src=\"images/refresh.png\" class=\"RefreshButton\" title=\"Clear Results\" onclick=\"window.location='AdminUserList.php'\" />"
	   . "</form></td></tr></table>"
	   . "<table cellspacing=\"0\" border=\"0\" width=\"100%\" id=\"userTable\">\n"
	   . "<thead>"
	   . "<tr>"
       . "<th>Name</th>"
       . "<th width=\"200px\">Section</th>"
       . "<th width=\"200px\">Division</th>"
	   . "<th>Email</th>"
	   . "<th>Phone Number</th>"
	   . "</tr>"
	   . "</thead>"
	   . "<tbody>\n";

	// Determine which sections the user belongs to.
	$sqlQuery = "SELECT UserID, SectionID, IsSuperAdmin "
			  . "FROM vAppAdmin "
			  . "WHERE UserID = " . $userID . "";
	$rs = $db->Execute($sqlQuery);

	// Initialize counter.
	$i = 0;
	
	// If a user with that id exist.
	if ($rs->RowCount()) {
		
		// Begin assembling SQL query based on the following criterial...
		$sqlQuery = "SELECT * "
				  . "FROM vAppAdmin "
				  . "WHERE ";
				  
		// Add sections that the user has access to to the SQL query.
		while($row = $rs->FetchRow()) {
			
			// If user belongs to only one section. (First time through / Default)
			if($i < 1) {
				$sqlQuery .= "(SectionID = " . $row['SectionID'] . "";
			
			// If user belongs to more than one section.
			} else {
				$sqlQuery .= " OR SectionID = " . $row['SectionID'] . ""; //Adds this once $i has counted over 1
			}
			
			// increment counter.
			$i++;
			
			$isSuperAdmin = $row['IsSuperAdmin'];
			
		}
			
		if($isSuperAdmin == 1) {
			$sqlQuery .= ") AND (SectionID = ".$sectionID.") ORDER BY IsActive DESC, FirstName, LastName, SectionName ASC";
				
		} else {
			$sqlQuery .= ") AND (IsActive = 1) AND (SectionID = ".$sectionID.") ORDER BY FirstName, LastName, SectionName ASC";
		
		}
			
		$rs = $db->Execute($sqlQuery);
		
		$i = 0; //Define $i as 0
		$count = 0; //Define $count as 0
		
		$currentUserInLoop 	= array();
		$sectionNames 		= "";
		$userID_Prev 		= 0;
		$isInActiveFlag 	= 1;
		
		//While Rows are Returned
		while($row = $rs->FetchRow()) 
		{

			if($row['UserID'] == $userID_Prev)
			{
			
				$userID_Prev 		= $row['UserID'];
				$userName_Prev 		= $row['FirstName'] . " " . $row['LastName'];
				$email_Prev 		= $row['Email1'];
				$phone_Prev 		= $row['Phone1'];
				$isactive_prev		= $row['IsActive'];
				$divisionName       = $row['DivisionName'];

				if($isactive_prev == 0)
				{
					$isactive_prev = "Inactive";
				}
				elseif($isactive_prev == 1)
				{
					$isactive_prev = "Active";
				}
							
				if($sectionNames == "") {
					$sectionNames = $row['SectionName'];
				} else {
					$sectionNames = $sectionNames . "<br/>" . $row['SectionName'];
				}
			
			} else {

				//If $username_Prev comes back as true containing text
				if($userName_Prev)
				{
				
					if($isactive_prev == 0 && isInActiveFlag == 1){
						echo "<tr style='border-bottom:1px solid grey'>";
					} else {
						echo "<tr>";	
					}
				
					echo "<tr>";
					//Check if the current logged in user is the current user in the loop
					if($userID_Prev == $userID)
					{
						echo "<td><img src=\"images/status/" . $isactive_prev . ".png\" title=\"Account is: " . $isactive_prev . "\" /><a href=\"AdminUserDetails.php?userID=" . $userID_Prev . "&action=EDIT\" title=\"Edit " . $userName_Prev . "'s profile\"><strong>" . $userName_Prev . "</strong></a></td>";
					}
					else
					{
						if(userIsAdmin($userID))
						{
							echo "<td><img src=\"images/status/" . $isactive_prev . ".png\" title=\"Account is: " . $isactive_prev . "\" /><a href=\"AdminUserDetails.php?userID=" . $userID_Prev . "&action=EDIT\" title=\"Edit " . $userName_Prev . "'s profile\">" . $userName_Prev . "</a></td>";
						}
						else
						{
							echo "<td><img src=\"images/status/" . $isactive_prev . ".png\" title=\"Account is: " . $isactive_prev . "\" />" . $userName_Prev . "</td>";
						}
					}
					
					echo "<td>" . $sectionNames . "</td>";

                    echo "<td>" . $divisionName . "</td>";
					
					//Search for the '@' symbol to check if the current email is a valid email and not just text.
					if(strpbrk($email_Prev, "@"))
					{
						echo "<td><a href=\"mailto:" . $email_Prev . "?subject=HRP Volunteer Calendar\" title=\"Send " . $userName_Prev . " an email\">" . $email_Prev . "</a></td>";
					}
					else
					{
						echo "<td>N/A</td>";
					}
					
					if($phone_Prev == "")
					{
						echo "<td width=\"110px\">N/A</td>";

					}
					else
					{
						echo "<td width=\"110px\">" . $phone_Prev . "</td>";
					}
					
					echo "</tr>\n";
				}	
			
				$userID_Prev 		= $row['UserID'];
				$userName_Prev 		= $row['FirstName'] . " " . $row['LastName'];
				$email_Prev 		= $row['Email1'];
				$phone_Prev 		= $row['Phone1'];		
				$sectionNames 		= $row['SectionName'];
				$isactive_prev		= $row['IsActive'];
                $divisionName       = $row['DivisionName'];
				
				if($isactive_prev == 0)
				{
					$isactive_prev = "Inactive";
				}
				elseif($isactive_prev == 1)
				{
					$isactive_prev = "Active";
				}
			}
			
			$count++; //$count + 1
			$i++; //$i + 1
		}
		
		//If $username_Prev comes back as true containing text
		if($userName_Prev)
		{
		
			echo "<tr>";
			//Check if the current logged in user is the current user in the loop
			if($userID_Prev == $userID)
			{
				echo "<td><img src=\"images/status/" . $isactive_prev . ".png\" title=\"Account is: " . $isactive_prev . "\" /><a href=\"AdminUserDetails.php?userID=" . $userID_Prev . "&action=EDIT\" title=\"Edit " . $userName_Prev . "'s profile\"><strong>" . $userName_Prev . "</strong></a>";
			}
			else
			{
				if(userIsAdmin($userID))
				{
					echo "<td><img src=\"images/status/" . $isactive_prev . ".png\" title=\"Account is: " . $isactive_prev . "\" /><a href=\"AdminUserDetails.php?userID=" . $userID_Prev . "&action=EDIT\" title=\"Edit " . $userName_Prev . "'s profile\">" . $userName_Prev . "</a>";
				}
				else
				{
					echo "<td><img src=\"images/status/" . $isactive_prev . ".png\" title=\"Account is: " . $isactive_prev . "\" />" . $userName_Prev . "";
				}
			}

			
			echo "<td width=\"130\">" . $sectionNames . "</td>";

            echo "<td>" . $divisionName . "</td>";
			
			//Search for the '@' symbol to check if the current email is a valid email and not just text.
			if(strpbrk($email_Prev, "@"))
			{
				echo "<td><a href=\"mailto:" . $email_Prev . "?subject=HRP Volunteer Calendar\" title=\"Send " . $userName_Prev . " an email\">" . $email_Prev . "</a></td>";
			}
			else
			{
				echo "<td>N/A</td>";
			}
			
			if($phone_Prev == "")
			{
				echo "<td width=\"110px\">N/A</td>";

			}
			else
			{
				echo "<td width=\"110px\">" . $phone_Prev . "</td>";
			}
			
			echo "</tr>\n";
		}
	
	}
	
	echo "</tbody></table>\n";
	
	$db->Close();
}







function printUsersByFilter($userID, $filter)
{
	global $db;
	connectDB();
	
	echo "<table class=\"SearchAndAlert\"><tr><td valign=\"middle\" class=\"Action\">Manage Users</td>"
	   . "<td class=\"SearchBox\"><form id=\"AdminUserListSearch\" name=\"SearchAdminUserList\" method=\"get\" action=\"AdminUserList.php\">"
	   . "<input type=\"text\" name=\"filter\" id=\"filter\" value=\"Search for a user...\" onfocus=\"this.value=''\" />"
	   . "<input src=\"images/magnifier.png\" class=\"SearchButton\" value=\"Search\" type=\"image\" id=\"Search\" title=\"Search\" />"
	   . "<img src=\"images/refresh.png\" class=\"RefreshButton\" title=\"Clear Results\" onclick=\"window.location='AdminUserList.php'\" />"
	   . "</form></td></tr></table>"
	   . "<table cellspacing=\"0\" border=\"0\" width=\"100%\" id=\"userTable\">\n"
	   . "<thead>"
	   . "<tr>"
       . "<th>Name</th>"
       . "<th width=\"200px\">Section</th>"
       . "<th width=\"200px\">Division</th>"
	   . "<th>Email</th>"
	   . "<th>Phone Number</th>"
	   . "</tr>"
	   . "</thead>"
	   . "<tbody>\n";

	$sqlQuery = "SELECT UserID, SectionID "
			  . "FROM vAppAdmin "
			  . "WHERE UserID = " . $userID . "";
	$rs = $db->Execute($sqlQuery);

	$i = 0; //Define $i as 0
	
	if ($rs->RowCount()) 
	{
		$sqlQuery = "SELECT * "
				  . "FROM vAppAdmin "
				  . "WHERE ";
				  
		//Loop though all Sections available to the current or selected user
		while($row = $rs->FetchRow())
		{
			//If $i is Less Then 1
			if($i < 1)
			{
				$sqlQuery .= "(SectionID = " . $row['SectionID'] . ""; //Only used if one Section is available.
				
				if($i == 1)
				{
					$sqlQuery .= ") ";
				}
			}
			else //Not Less Then 1
			{
				$sqlQuery .= " OR SectionID = " . $row['SectionID'] . ""; //Adds this once $i has counted over 1
			}
			
			$i++; //$i + 1
		}
			
		$sqlQuery .= ") "
				   . "AND (FirstName LIKE '%" . $filter . "%' OR LastName LIKE '%" . $filter . "%') "
				   . "ORDER BY IsActive DESC, FirstName, LastName, SectionName ASC";
		$rs = $db->Execute($sqlQuery);
		
		$i = 0; //Define $i as 0
		$count = 0; //Define $count as 0
		
		$currentUserInLoop = array();
		$sectionNames = "";
		$userID_Prev = 0;
		
		//While Rows are Returned
		while($row = $rs->FetchRow()) 
		{
			if($row['UserID'] == $userID_Prev)
			{
				$userID_Prev 		= $row['UserID'];
				$userName_Prev 		= $row['FirstName'] . " " . $row['LastName'];
				$email_Prev 		= $row['Email1'];
				$phone_Prev 		= $row['Phone1'];
				$isactive_prev		= $row['IsActive'];
				$divisionName       = $row['DivisionName'];
				
				if($isactive_prev == 0)
				{
					$isactive_prev = "Inactive";
				}
				elseif($isactive_prev == 1)
				{
					$isactive_prev = "Active";
				}
							
				if($sectionNames == "") {
					$sectionNames = $row['SectionName'];
				} else {
					$sectionNames = $sectionNames . "<br/>" . $row['SectionName'];
				}
			
			} else {

				//If $username_Prev comes back as true containing text
				if($userName_Prev)
				{
					//Check if the current logged in user is the current user in the loop
					if($userID_Prev == $userID)
					{
						echo "<td><img src=\"images/status/" . $isactive_prev . ".png\" title=\"Account is: " . $isactive_prev . "\" /><a href=\"AdminUserDetails.php?userID=" . $userID_Prev . "&action=EDIT\" title=\"Edit " . $userName_Prev . "'s profile\"><strong>" . $userName_Prev . "</strong></a></td>";
					}
					else
					{
						if(userIsAdmin($userID))
						{
							echo "<td><img src=\"images/status/" . $isactive_prev . ".png\" title=\"Account is: " . $isactive_prev . "\" /><a href=\"AdminUserDetails.php?userID=" . $userID_Prev . "&action=EDIT\" title=\"Edit " . $userName_Prev . "'s profile\">" . $userName_Prev . "</a></td>";
						}
						else
						{
							echo "<td><img src=\"images/status/" . $isactive_prev . ".png\" title=\"Account is: " . $isactive_prev . "\" />" . $userName_Prev . "</td>";
						}
					}
					
					echo "<td>" . $sectionNames . "</td>";

					echo "<td>" . $divisionName . "</td>";
					
					//Search for the '@' symbol to check if the current email is a valid email and not just text.
					if(strpbrk($email_Prev, "@"))
					{
						echo "<td><a href=\"mailto:" . $email_Prev . "?subject=HRP Volunteer Calendar\" title=\"Send " . $userName_Prev . " an email\">" . $email_Prev . "</a></td>";
					}
					else
					{
						echo "<td>N/A</td>";
					}
					
					if($phone_Prev == "")
					{
						echo "<td width=\"110px\">N/A</td>"
						   . "</tr>\n";

					}
					else
					{
						echo "<td width=\"110px\">" . $phone_Prev . "</td>"
						   . "</tr>\n";
					}
				}	
			
				$userID_Prev 		= $row['UserID'];
				$userName_Prev 		= $row['FirstName'] . " " . $row['LastName'];
				$email_Prev 		= $row['Email1'];
				$phone_Prev 		= $row['Phone1'];		
				$sectionNames 		= $row['SectionName'];
				$isactive_prev		= $row['IsActive'];
                $divisionName       = $row['DivisionName'];
				
				if($isactive_prev == 0)
				{
					$isactive_prev = "Inactive";
				}
				elseif($isactive_prev == 1)
				{
					$isactive_prev = "Active";
				}
			}
			
			$count++; //$count + 1
			$i++; //$i + 1
		}
		
		//If $username_Prev comes back as true containing text
		if($userName_Prev)
		{
			//Check if the current logged in user is the current user in the loop
			if($userID_Prev == $userID)
			{
				echo "<td><img src=\"images/status/" . $isactive_prev . ".png\" title=\"Account is: " . $isactive_prev . "\" /><a href=\"AdminUserDetails.php?userID=" . $userID_Prev . "&action=EDIT\" title=\"Edit " . $userName_Prev . "'s profile\"><strong>" . $userName_Prev . "</strong></a></td>";
			}
			else
			{
				if(userIsAdmin($userID))
				{
					echo "<td><img src=\"images/status/" . $isactive_prev . ".png\" title=\"Account is: " . $isactive_prev . "\" /><a href=\"AdminUserDetails.php?userID=" . $userID_Prev . "&action=EDIT\" title=\"Edit " . $userName_Prev . "'s profile\">" . $userName_Prev . "</a></td>";
				}
				else
				{
					echo "<td><img src=\"images/status/" . $isactive_prev . ".png\" title=\"Account is: " . $isactive_prev . "\" />" . $userName_Prev . "</td>";
				}
			}
			
			echo "<td>" . $sectionNames . "</td>";

            echo "<td>" . $divisionName . "</td>";
			
			//Search for the '@' symbol to check if the current email is a valid email and not just text.
			if(strpbrk($email_Prev, "@"))
			{
				echo "<td><a href=\"mailto:" . $email_Prev . "?subject=HRP Volunteer Calendar\" title=\"Send " . $userName_Prev . " an email\">" . $email_Prev . "</a></td>";
			}
			else
			{
				echo "<td>N/A</td>";
			}
			
			if($phone_Prev == "")
			{
				echo "<td width=\"110px\">N/A</td>"
				   . "</tr>\n";

			}
			else
			{
				echo "<td width=\"110px\">" . $phone_Prev . "</td>"
				   . "</tr>\n";
			}
		}
	}
	
	echo "</tbody></table>\n";
	
	$db->Close();
}

function printNavigation($userID)
{
	global $db;
	connectDB();
		
	$sqlQuery = "SELECT IsAdmin, UserID, SectionName, SectionID, IsSuperAdmin "
			  . "FROM vAppAdmin "
			  . "WHERE UserID = $userID "
			  . "AND IsAdmin = 1";			
	$rs = $db->Execute($sqlQuery);

	if ($rs->RowCount())
	{
		?>
		<div class="navMenu">
			<a class="navButton" href="index.php" title="Volunteer Home"><img src="images/icon_home.gif" border="0" alt="Volunteer Home" />Volunteer Home</a>
			<a class="navButton" href="cal.php" title="View Shift Calendar"><img src="images/icon_cal.gif" border="0" alt="View Shift Calendar" />Shift Calendar</a>
			<a class="navButton" href="shifts.php" title="View Shift List"><img src="images/icon_shiftlist.gif" border="0" alt="View Shift List" />Shift List</a>
			<a class="navButton" href="hours.php" title="View Hour List"><img src="images/icon_shiftlist.gif" border="0" alt="View Hour List" />Hour List</a>

			<a class="navButton" href="javascript:popDetails('','ADDNEW');" title="Add New Shift"><img src="images/icon_caladd.gif" border="0" alt="Add New Shift" />Add New Shift</a>
			<!--<a class="navButton" href="" title="View Messages"><img src="images/icon_message.gif" border="0" alt="Messages" />Message Box</a>-->
			<a class="navButton" href="news.php?action=ADDNEW" title="Post News Item"><img src="images/icon_newsadd.gif" border="0" alt="Post News Item" />Post News Item</a>
			<a class="navButton" href="AdminUserDetails.php?action=ADDNEW" title="Add User"><img src="images/group_add.png" border="0" alt="Add User" />Add User</a>
			<a class="navButton" href="AdminUserList.php" title="Manage Users"><img src="images/group_edit.png" border="0" alt="Manage Users" />Manage Users</a>
            <?php if ($rs->fields['IsSuperAdmin'] == 1) { ?>
                <a class="navButton" href="milestones.php" title="Milestones"><img src="images/icon_cal.gif" border="0" alt="Milestones" />Milestones</a>
                <a class="navButton" href="leads.php" title="Leads"><img src="images/group_edit.png" border="0" alt="Milestones" />Leads</a>
            <?php
            }
            ?>
			<a class="navButton" href="login.php?action=LOGOUT" title="Logout"><img src="images/icon_logout.gif" border="0" alt="Logout" />Logout</a>
		</div>
		<?php
	}
	else
	{
		?>
		<div class="navMenu">
			<a class="navButton" href="index.php" title="Volunteer Home"><img src="images/icon_home.gif" border="0" alt="Volunteer Home" />Volunteer Home</a>
			<a class="navButton" href="cal.php" title="View Shift Calendar"><img src="images/icon_cal.gif" border="0" alt="View Shift Calendar" />Shift Calendar</a>
			<a class="navButton" href="shifts.php" title="View Shift List"><img src="images/icon_shiftlist.gif" border="0" alt="View Shift List" />Shift List</a>
			<a class="navButton" href="javascript:popDetails('','ADDNEW');" title="Add New Shift"><img src="images/icon_caladd.gif" border="0" alt="Add New Shift" />Add New Shift</a>
			<!--<a class="navButton" href="" title="View Messages"><img src="images/icon_message.gif" border="0" alt="Messages" />Message Box</a>-->
			<a class="navButton" href="news.php?action=ADDNEW" title="Post News Item"><img src="images/icon_newsadd.gif" border="0" alt="Post News Item" />Post News Item</a>
            <a class="navButton" href="AdminUserList.php" title="Contact Book"><img src="images/group_edit.png" border="0" alt="Contact Book" />Contact Book</a>
			<a class="navButton" href="login.php?action=LOGOUT" title="Logout"><img src="images/icon_logout.gif" border="0" alt="Logout" />Logout</a>
		</div>
		<?php 
	}
	
	$db->Close();
}







function userDetails($current_userID, $requested_userID, $action, $status, $status2) 
{
	global $db;
	connectDB();
	
	$currentUser_IsAdmin = 0;
	$currentUser_IsSuperAdmin = 0;

	if ($status == "") {
		switch($action) {
			case "ADDNEW":
				$pageTitle = "Add New User";
				$submitStatus = "Added";
				break;
			case "EDIT";
				$pageTitle = "Edit Mode";
				$submitStatus = "Updated";
				break;
			case "DELETE";
				$pageTitle = "User Deleted";
				$submitStatus = "Deleted";
				break;
		}
	}
	else {
		$pageTitle = $status2;
		$submitStatus = $status;
	}

	$sqlQuery = "SELECT * "
			  . "FROM vAppAdmin "
			  . "WHERE UserID = " . $current_userID . " "
			  . "AND IsAdmin = 1";
	$rs = $db->Execute($sqlQuery);
	$row = $rs->FetchRow();
	
	
	if($row['IsAdmin'] == 1) {
		$currentUser_IsAdmin = 1;
			
			if($row['IsSuperAdmin'] == 1)
				$currentUser_IsSuperAdmin = 1;
	
	} else {
		$currentUser_IsAdmin = 0;
	}
		
		
	if($action == "ADDNEW"){
		$sqlQuery	= "SELECT UserID "
		            . "FROM AppUser "
		            . "ORDER BY UserID DESC";
		$rs = $db->Execute($sqlQuery);
		$row = $rs->FetchRow();
		
		$requested_userID = $row['UserID'] + 1; //Get next UserID by getting the current and adding 1 ;
	}
		
	echo "<table class=\"SearchAndAlert\"><tr><td class=\"Action\">".$pageTitle."</td></tr></table>\n"
		//."<form id=\"AdminUserDetails\" name=\"AdminUserDetails\" method=\"Post\" action=\"AdminUserDetails.php\" onsubmit=\"return validateTheForm(this)\">\n"
		."<form action=\"AdminUserDetails.php?userID=".$requested_userID."&action=".$action."&status=".$submitStatus."\" method=\"post\" name=\"AdminUserDetails\" id=\"AdminUserDetails\" onsubmit=\"return ".(($action=='EDIT')?'validateUpdateForm(this)':'validateTheForm(this)')."\" autocomplete=\"off\">"
		."<table cellspacing=\"0\" border=\"0\" width=\"100%\" id=\"userTable\">\n"
		."<thead>\n<tr><th>Item</th><th>Description</th><th><span style=\"float:right;\">";

	if ($requested_userID != '')	echo "User ID #".$requested_userID."<input type=\"hidden\" name=\"fUserID\" id=\"fUserID\" value=\"".$requested_userID."\" />\n";
	
	echo "</span></th></tr>\n</thead>\n<tbody>\n";



	// EDIT
	
	if (($requested_userID != '') && ($action == "EDIT")) 
	{
		$sqlQuery	= "SELECT * "
		            . "FROM AppUser "
		            . "WHERE UserID = " . $requested_userID . "";
		$rs = $db->Execute($sqlQuery);

		//if the rowcount is not zero it returned rows
		if ($rs->RowCount()) 
		{

			//loop through and output all the rows in the $rs object
			while ($row = $rs->FetchRow()) 
			{	
				
				if($row['DateofBirth'] == "1900-01-01 00:00:00.000"){
					$DateofBirth = "";
				} else {
					$DateofBirth = date("m/d/Y", strtotime($row['DateofBirth']));
				}
				
				$VolunteerSince = date("m/d/Y", strtotime($row['VolunteerSince']));
			
				echo "<tr>\n"
					."<td class=\"Title\">First Name: </td><td colspan=\"2\"><input type=\"text\" name=\"fFirstName\" id=\"fFirstName\" value=\"".$row['FirstName']."\" onblur=\"RandomLoginGenerator(document.AdminUserDetails.fUsername)\" /> Note: <strong>Capitalized upon Submit. (name -> Name)</strong></td>\n"
					."</tr>\n"
					."<tr>\n"
					."<td class=\"Title\">Last Name: </td><td colspan=\"2\"><input type=\"text\" name=\"fLastName\" id=\"fLastName\" value=\"".$row['LastName']."\" onblur=\"RandomLoginGenerator(document.AdminUserDetails.fUsername)\" /> Note: <strong>Capitalized upon Submit. (name -> Name)</strong></td>\n"
					."</tr>\n"
                    ."<tr><td class=\"Title\">Division: </td><td colspan=\"2\">\n";

                    drawDivisionSelect("fDivision", $row['DivisionId']);

                    echo "</td></tr>\n<tr>\n"
					."<tr>\n"
					."<td class=\"Title\">Username: </td><td colspan=\"2\"><input type=\"text\" name=\"fUsername\" id=\"fUsername\" value=\"".$row['Username']."\" /></td>\n"
					."</tr>\n"
					."<tr>\n"
					."<td class=\"Title\">Password: </td><td colspan=\"2\"><input type=\"password\" name=\"fPassword\" id=\"fPassword\" /></td>\n"
					//."<td class=\"Title\">Password: </td><td colspan=\"2\"><input type=\"password\" name=\"fPassword\" id=\"fPassword\" /> Note: <strong>Changes to this form will require you to reset the password.</strong></td>\n"
					//."<td class=\"Title\">Password: </td><td colspan=\"2\"><input type=\"password\" name=\"fPassword\" id=\"fPassword\" value=\"".$row['Password']."\" /></td>\n"
					."</tr>\n"
					."<tr>\n"
					."<td class=\"Title\">Address 1: </td><td colspan=\"2\"><input type=\"text\" name=\"fAddress1\" id=\"fAddress1\" value=\"".$row['Address1']."\" /></td>\n"
					."</tr>\n"
					."<tr>\n"
					."<td class=\"Title\">Address 2: </td><td colspan=\"2\"><input type=\"text\" name=\"fAddress2\" id=\"fAddress2\" value=\"".$row['Address2']."\" /></td>\n"
					."</tr>\n"
					."<tr>\n"
					."<td class=\"Title\">City: </td><td colspan=\"2\"><input type=\"text\" name=\"fCity\" id=\"fCity\" value=\"".$row['City']."\" /></td>\n"
					."</tr>\n"
					."<tr>\n"
					."<td class=\"Title\">Province: </td><td colspan=\"2\"><input type=\"text\" name=\"fProvince\" id=\"fProvince\" value=\"".$row['Province']."\" /></td>\n"
					."</tr>\n"
					."<tr>\n"
					."<td class=\"Title\">Postal Code: </td><td colspan=\"2\"><input type=\"text\" name=\"fPostalCode\" id=\"fPostalCode\" value=\"".$row['PostalCode']."\" /> Note: <strong>Capitalized upon Submit. (b3m4n8 -> B3M4N8)</td>\n"
					."</tr>\n"
					."<tr>\n"
					."<td class=\"Title\">Phone: </td><td colspan=\"2\"><input type=\"text\" name=\"fPrimaryPhone\" id=\"fPrimaryPhone\" value=\"".$row['Phone1']."\" /> Example: <strong>(902) 555-5555</strong></td>\n"
					."</tr>\n"
					//."<tr>\n"
					//."<td class=\"Title\">Secondary Phone: </td><td colspan=\"2\"><input type=\"text\" name=\"fSecondaryPhone\" id=\"fSecondaryPhone\" value=\"".$row['Phone2']."\" /></td>\n"
					//."</tr>\n"
					//."<tr>\n"
					//."<td class=\"Title\">Mobile: </td><td colspan=\"2\"><input type=\"text\" name=\"fMobile\" id=\"fMobile\" value=\"".$row['PhoneCell']."\" /></td>\n"
					//."</tr>\n"
					."<tr>\n"
					."<td class=\"Title\">Email: </td><td colspan=\"2\"><input type=\"text\" name=\"fPrimaryEmail\" id=\"fPrimaryEmail\" value=\"".$row['Email1']."\" /> Example: <strong>you@halifax.ca</strong></td>\n"
					."</tr>\n"
					."<tr>\n"
					."<td class=\"Title\">Date of Birth: </td><td colspan=\"2\"><input class=\"datesel\" type=\"text\" name=\"fDateofBirth\" readonly value=\"" . $DateofBirth . "\" />\n"
					."<img src=\"images/icon_cal.gif\" border=\"0\" onclick=\"displayCalendar(document.forms[0].fDateofBirth, 'mm/dd/yyyy', this)\" onmouseover=\"this.style.cursor='pointer';\">\n"
					."<img src=\"images/refresh.png\" border=\"0\" title=\"Clear\" onclick=\"document.forms[0].fDateofBirth.value = '';\" onmouseover=\"this.style.cursor='pointer';\" />\n"
					."</td>\n"
					."<tr>\n"
					."<td class=\"Title\">Volunteer Since: </td><td colspan=\"2\"><input class=\"datesel\" type=\"text\" name=\"fVolunteerSince\" readonly value=\"" . $VolunteerSince . "\" />\n"
					."<img src=\"images/icon_cal.gif\" border=\"0\" onclick=\"displayCalendar(document.forms[0].fVolunteerSince, 'mm/dd/yyyy', this)\" onmouseover=\"this.style.cursor='pointer';\">\n"
					."<img src=\"images/refresh.png\" border=\"0\" title=\"Clear\" onclick=\"document.forms[0].fVolunteerSince.value = '';\" onmouseover=\"this.style.cursor='pointer';\" />\n"
					."</td>\n"
					//."<tr>\n"
					//."<td class=\"Title\">Secondary Email: </td><td colspan=\"2\"><input type=\"text\" name=\"fSecondaryEmail\" id=\"fSecondaryEmail\" value=\"".$row['Email2']."\" /></td>\n"
					//."</tr>\n"
					//."<tr>\n"
					//."<td class=\"Title\">Text Messaging: </td><td colspan=\"2\"><input type=\"text\" name=\"fTextMessaging\" id=\"fTextMessaging\" value=\"".$row['TextMessage']."\" /></td>\n"
					//."</tr>\n"
					."<tr>\n"
					."<td class=\"Title\">Comment: </td><td colspan=\"2\"><textarea rows=\"5\" name=\"fComments\" id=\"fComments\">".$row['Comment']."</textarea></td>\n"
					."</tr>\n";
					
					if($currentUser_IsSuperAdmin == 1) {
						echo "<tr>\n"
						."<td class=\"Title\">Super Admin Comment: </td><td colspan=\"2\"><textarea rows=\"5\" name=\"fSuperAdminComments\" id=\"fSuperAdminComments\">".$row['SuperAdminComment']."</textarea></td>\n"
						."</tr>\n";
					}


					// Check if user being modified is a Super Admin
					if(userIsSuperAdmin($requested_userID)){
						echo "<tr><td class=\"Title\">Section & Admin Access: </td><td colspan=\"2\">This user is a Super Admin and therefore has admin access to all section.\n";
					
					} else {
						
						if($currentUser_IsAdmin == 1)
						{					
							echo "<tr><td class=\"Title\">Section & Admin Access: </td><td colspan=\"2\">\n";
							echo "<input type=\"hidden\" name=\"fIsAdmin\" id=\"fIsAdmin\" value=\"1\" />";
							userSections($requested_userID);
						} else {
							echo "<input type=\"hidden\" name=\"fIsAdmin\" id=\"fIsAdmin\" value=\"0\" />";
						}
						
					}

					echo "</td>\n"
					."</tr>\n"
					."<tr>\n"
					."<td class=\"Title\">Is Active: </td>\n";
					
					if($requested_userID == $current_userID)
					{
						echo "<td>"
						    ."<img src=\"images\delete.png\" /> WARNING:<br />If you Deactive yourself you will be unable to Reactive yourself or Login once you Logout.<br />You will need to contact an Administrator to Reactive you."
						    ."</td>";
					}
					
					echo "<td>";
					
					if ($row['IsActive'] == 0) {
						echo "<input class=\"RadioBoxes\" type=\"radio\" name=\"fIsActive\" id=\"fIsActive\" value=\"1\" />Yes<br />\n"
							."<input class=\"RadioBoxes\" type=\"radio\" name=\"fIsActive\" id=\"fIsActive\" value=\"0\" CHECKED />No\n";
					}
					else {
						echo "<input class=\"RadioBoxes\" type=\"radio\" name=\"fIsActive\" id=\"fIsActive\" value=\"1\" CHECKED />Yes<br />\n"
							."<input class=\"RadioBoxes\" type=\"radio\" name=\"fIsActive\" id=\"fIsActive\" value=\"0\" />No\n";
					}
					
					//echo "<tr><td class=\"Title\">Is Active: </td><td colspan=\"2\"><input type=\"text\" name=\"fIsActive\" id=\"fIsActive\" value=\"".$row['IsActive']."\" /></td></tr>\n";			
					echo "</td>\n"
					."</tr>\n";			

				//echo "<pre>";
				//print_r($row);
				//echo "</pre>";

			}
		}

	}
	
	
	// ADD NEW
	
	elseif($action == "ADDNEW") 
	{		
	
		$DateofBirth = "";
		$VolunteerSince = date("m/d/Y");

		echo "<tr>\n"
			."<td class=\"Title\">First Name: </td><td colspan=\"2\"><input type=\"text\" name=\"fFirstName\" id=\"fFirstName\" value=\"\" onblur=\"RandomLoginGenerator(document.AdminUserDetails.fUsername)\" /> Note: <strong>Capitalized upon Submit. (name -> Name)</strong><br /><div id=\"fFirstName_Error\"></div></td>\n"
			."</tr>\n"
			."<tr>\n"
			."<td class=\"Title\">Last Name: </td><td colspan=\"2\"><input type=\"text\" name=\"fLastName\" id=\"fLastName\" value=\"\" onblur=\"RandomLoginGenerator(document.AdminUserDetails.fUsername)\" /> Note: <strong>Capitalized upon Submit. (name -> Name)</strong></td>\n"
			."</tr>\n"
            ."<tr><td class=\"Title\">Division: </td><td colspan=\"2\">\n";

		    drawDivisionSelect("fDivision", null);

            echo "</td></tr>\n<tr>\n"
			."<tr>\n"
			."<td class=\"Title\">Username: </td><td colspan=\"2\"><input type=\"text\" name=\"fUsername\" id=\"fUsername\" value=\"\" /></td>\n"
			."</tr>\n"
			."<tr>\n"
			."<td class=\"Title\">Password: </td><td colspan=\"2\"><input type=\"password\" name=\"fPassword\" id=\"fPassword\" value=\"\" /></td>\n"
			."</tr>\n"
			."<tr>\n"
			."<td class=\"Title\">Address 1: </td><td colspan=\"2\"><input type=\"text\" name=\"fAddress1\" id=\"fAddress1\" value=\"\" /></td>\n"
			."</tr>\n"
			."<tr>\n"
			."<td class=\"Title\">Address 2: </td><td colspan=\"2\"><input type=\"text\" name=\"fAddress2\" id=\"fAddress2\" value=\"\" /></td>\n"
			."</tr>\n"
			."<tr>\n"
			."<td class=\"Title\">City: </td><td colspan=\"2\"><input type=\"text\" name=\"fCity\" id=\"fCity\" value=\"Halifax\" /></td>\n"
			."</tr>\n"
			."<tr>\n"
			."<td class=\"Title\">Province: </td><td colspan=\"2\"><input type=\"text\" name=\"fProvince\" id=\"fProvince\" value=\"Nova Scotia\" readonly /></td>\n"
			."</tr>\n"
			."<tr>\n"
			."<td class=\"Title\">Postal Code: </td><td colspan=\"2\"><input type=\"text\" name=\"fPostalCode\" id=\"fPostalCode\" value=\"\" /> Note: <strong>Capitalized upon Submit. (b3m4n8 -> B3M4N8)</td>\n"
			."</tr>\n"
			."<tr>\n"
			."<td class=\"Title\">Phone: </td><td colspan=\"2\"><input type=\"text\" name=\"fPrimaryPhone\" id=\"fPrimaryPhone\" value=\"\" /> Example: <strong>(902) 555-5555</strong></td>\n"
			."</tr>\n"
			//."<tr>\n"
			//."<td class=\"Title\">Secondary Phone: </td><td colspan=\"2\"><input type=\"text\" name=\"fSecondaryPhone\" id=\"fSecondaryPhone\" value=\"".$row['Phone2']."\" /></td>\n"
			//."</tr>\n"
			//."<tr>\n"
			//."<td class=\"Title\">Mobile: </td><td colspan=\"2\"><input type=\"text\" name=\"fMobile\" id=\"fMobile\" value=\"".$row['PhoneCell']."\" /></td>\n"
			//."</tr>\n"
			."<tr>\n"
			."<td class=\"Title\">Email: </td><td colspan=\"2\"><input type=\"text\" name=\"fPrimaryEmail\" id=\"fPrimaryEmail\" value=\"\" /> Example: <strong>you@halifax.ca</strong></td>\n"
			."</tr>\n"
			."<tr>\n"
			."<td class=\"Title\">Date of Birth: </td><td colspan=\"2\"><input class=\"datesel\" type=\"text\" name=\"fDateofBirth\" readonly value=\"" . $DateofBirth . "\" />\n"
			."<img src=\"images/icon_cal.gif\" border=\"0\" onclick=\"displayCalendar(document.forms[0].fDateofBirth, 'mm/dd/yyyy', this)\" onmouseover=\"this.style.cursor='pointer';\">\n"
			."<img src=\"images/refresh.png\" border=\"0\" title=\"Clear\" onclick=\"document.forms[0].fDateofBirth.value = '';\" onmouseover=\"this.style.cursor='pointer';\" />\n"
			."</td>\n"
			."<tr>\n"
			."<td class=\"Title\">Volunteer Since: </td><td colspan=\"2\"><input class=\"datesel\" type=\"text\" name=\"fVolunteerSince\" readonly value=\"" . $VolunteerSince . "\" />\n"
			."<img src=\"images/icon_cal.gif\" border=\"0\" onclick=\"displayCalendar(document.forms[0].fVolunteerSince, 'mm/dd/yyyy', this)\" onmouseover=\"this.style.cursor='pointer';\">\n"
			."<img src=\"images/refresh.png\" border=\"0\" title=\"Clear\" onclick=\"document.forms[0].fVolunteerSince.value = '';\" onmouseover=\"this.style.cursor='pointer';\" />\n"
			."</td>\n"
			//."<tr>\n"
			//."<td class=\"Title\">Secondary Email: </td><td colspan=\"2\"><input type=\"text\" name=\"fSecondaryEmail\" id=\"fSecondaryEmail\" value=\"".$row['Email2']."\" /></td>\n"
			//."</tr>\n"
			//."<tr>\n"
			//."<td class=\"Title\">Text Messaging: </td><td colspan=\"2\"><input type=\"text\" name=\"fTextMessaging\" id=\"fTextMessaging\" value=\"".$row['TextMessage']."\" /></td>\n"
			//."</tr>\n"
			."<tr>\n"
			."<td class=\"Title\">Comment: </td><td colspan=\"2\"><textarea rows=\"5\" name=\"fComments\" id=\"fComments\"></textarea></td>\n"
			."</tr>\n";
			
			if($currentUser_IsSuperAdmin == 1) {
				echo "<tr>\n"
				."<td class=\"Title\">Super Admin Comment: </td><td colspan=\"2\"><textarea rows=\"5\" name=\"fSuperAdminComments\" id=\"fSuperAdminComments\">".$row['SuperAdminComment']."</textarea></td>\n"
				."</tr>\n";
			}
			
			
			echo "<tr><td class=\"Title\">Section & Admin Access: </td><td colspan=\"2\">\n";
			echo "<input type=\"hidden\" name=\"fIsAdmin\" id=\"fIsAdmin\" value=\"1\" />";
			userSections($requested_userID);
			
			//userCategories();
			//userSections($requested_userID);
	
			echo "</td></tr>\n<tr><td class=\"Title\">Is Active: </td><td colspan=\"2\">";
			
				/*
				Added by Brandon
				Replaced a text box showing the 'Active' (ie: 1 or 0) status, all other pages have radio boxs, so I added them here to. 
				
				if($_POST['fIsActive'] = 0)
				{
					echo "<input class=\"RadioBoxes\" type=\"radio\" name=\"fIsActive\" id=\"fIsActive\" value=\"1\" />Yes<br />\n"
						."<input class=\"RadioBoxes\" type=\"radio\" name=\"fIsActive\" id=\"fIsActive\" value=\"0\" CHECKED />No\n";
				}
				else //check if false is returned -Brandon
				{*/
					echo "<input class=\"RadioBoxes\" type=\"radio\" name=\"fIsActive\" id=\"fIsActive\" value=\"1\" CHECKED />Yes<br />\n"
						."<input class=\"RadioBoxes\" type=\"radio\" name=\"fIsActive\" id=\"fIsActive\" value=\"0\" />No\n";
				//}
				
			echo "</td></tr>\n";
				//."<input type=\"hidden\" name=\"fPassword\" id=\"fPassword\" />\n";

	}
	
	
	else {
	
	//$DateofBirth = "";
	//$VolunteerSince = date("m/d/Y");

	echo "<tr>\n"
		."<td class=\"Title\">test First Name: </td><td colspan=\"2\"><input type=\"text\" name=\"fFirstName\" id=\"fFirstName\" value=\"".$row['FirstName']."\" onblur=\"RandomLoginGenerator(document.AdminUserDetails.fUsername)\" /> Note: <strong>Capitalized upon Submit. (name -> Name)</strong></td>\n"
		."</tr>\n"
		."<tr>\n"
		."<td class=\"Title\">Last Name: </td><td colspan=\"2\"><input type=\"text\" name=\"fLastName\" id=\"fLastName\" value=\"".$row['LastName']."\" onblur=\"RandomLoginGenerator(document.AdminUserDetails.fUsername)\" /> Note: <strong>Capitalized upon Submit. (name -> Name)</strong></td>\n"
		."</tr>\n"
		."<tr>\n"
		."<td class=\"Title\">Username: </td><td colspan=\"2\"><input type=\"text\" name=\"fUsername\" id=\"fUsername\" value=\"".$row['Username']."\" /></td>\n"
		."</tr>\n"
		."<tr>\n"
		."<td class=\"Title\">Password: </td><td colspan=\"2\"><input type=\"password\" name=\"fPassword\" id=\"fPassword\" value=\"".$row['Password']."\" /></td>\n"
		."</tr>\n"
		."<tr>\n"
		."<td class=\"Title\">Address 1: </td><td colspan=\"2\"><input type=\"text\" name=\"fAddress1\" id=\"fAddress1\" value=\"".$row['Address1']."\" /></td>\n"
		."</tr>\n"
		."<tr>\n"
		."<td class=\"Title\">Address 2: </td><td colspan=\"2\"><input type=\"text\" name=\"fAddress2\" id=\"fAddress2\" value=\"".$row['Address2']."\" /></td>\n"
		."</tr>\n"
		."<tr>\n"
		."<td class=\"Title\">City: </td><td colspan=\"2\"><input type=\"text\" name=\"fCity\" id=\"fCity\" value=\"".$row['City']."\" /></td>\n"
		."</tr>\n"
		."<tr>\n"
		."<td class=\"Title\">Province: </td><td colspan=\"2\"><input type=\"text\" name=\"fProvince\" id=\"fProvince\" value=\"".$row['Province']."\" /></td>\n"
		."</tr>\n"
		."<tr>\n"
		."<td class=\"Title\">Postal Code: </td><td colspan=\"2\"><input type=\"text\" name=\"fPostalCode\" id=\"fPostalCode\" value=\"".$row['PostalCode']."\" /> Note: <strong>Capitalized upon Submit. (b3m4n8 -> B3M4N8)</td>\n"
		."</tr>\n"
		."<tr>\n"
		."<td class=\"Title\">Phone: </td><td colspan=\"2\"><input type=\"text\" name=\"fPrimaryPhone\" id=\"fPrimaryPhone\" value=\"".$row['Phone1']."\" /> Example: <strong>(902) 555-5555</strong></td>\n"
		."</tr>\n"
		//."<tr>\n"
		//."<td class=\"Title\">Secondary Phone: </td><td colspan=\"2\"><input type=\"text\" name=\"fSecondaryPhone\" id=\"fSecondaryPhone\" value=\"".$row['Phone2']."\" /></td>\n"
		//."</tr>\n"
		//."<tr>\n"
		//."<td class=\"Title\">Mobile: </td><td colspan=\"2\"><input type=\"text\" name=\"fMobile\" id=\"fMobile\" value=\"".$row['PhoneCell']."\" /></td>\n"
		//."</tr>\n"
		."<tr>\n"
		."<td class=\"Title\">Email: </td><td colspan=\"2\"><input type=\"text\" name=\"fPrimaryEmail\" id=\"fPrimaryEmail\" value=\"".$row['Email1']."\" /> Example: <strong>you@halifax.ca</strong></td>\n"
		."</tr>\n"
		."<tr>\n"
		."<td class=\"Title\">Date of Birth: </td><td colspan=\"2\"><input class=\"datesel\" type=\"text\" name=\"fDateofBirth\" readonly value=\"" . $row['DateofBirth'] . "\" />\n"
		."<img src=\"images/icon_cal.gif\" border=\"0\" onclick=\"displayCalendar(document.forms[0].fDateofBirth, 'mm/dd/yyyy', this)\" onmouseover=\"this.style.cursor='pointer';\">\n"
		."<img src=\"images/refresh.png\" border=\"0\" title=\"Clear\" onclick=\"document.forms[0].fDateofBirth.value = '';\" onmouseover=\"this.style.cursor='pointer';\" />\n"
		."</td>\n"
		."<tr>\n"
		."<td class=\"Title\">Volunteer Since: </td><td colspan=\"2\"><input class=\"datesel\" type=\"text\" name=\"fVolunteerSince\" readonly value=\"" . $row['VolunteerSince'] . "\" />\n"
		."<img src=\"images/icon_cal.gif\" border=\"0\" onclick=\"displayCalendar(document.forms[0].fVolunteerSince, 'mm/dd/yyyy', this)\" onmouseover=\"this.style.cursor='pointer';\">\n"
		."<img src=\"images/refresh.png\" border=\"0\" title=\"Clear\" onclick=\"document.forms[0].fVolunteerSince.value = '';\" onmouseover=\"this.style.cursor='pointer';\" />\n"
		."</td>\n"
		//."<tr>\n"
		//."<td class=\"Title\">Secondary Email: </td><td colspan=\"2\"><input type=\"text\" name=\"fSecondaryEmail\" id=\"fSecondaryEmail\" value=\"".$row['Email2']."\" /></td>\n"
		//."</tr>\n"
		//."<tr>\n"
		//."<td class=\"Title\">Text Messaging: </td><td colspan=\"2\"><input type=\"text\" name=\"fTextMessaging\" id=\"fTextMessaging\" value=\"".$row['TextMessage']."\" /></td>\n"
		//."</tr>\n"
		."<tr>\n"
		."<td class=\"Title\">Comment: </td><td colspan=\"2\"><textarea rows=\"5\" name=\"fComments\" id=\"fComments\">".$row['Comment']."</textarea></td>\n"
		."</tr>\n";
		
		
		if($currentUser_IsSuperAdmin == 1) {
			echo "<tr>\n"
			."<td class=\"Title\">Super Admin Comment: </td><td colspan=\"2\"><textarea rows=\"5\" name=\"fSuperComments\" id=\"fSuperComments\">".$row['SuperAdminComment']."</textarea></td>\n"
			."</tr>\n";
		}
		
		echo "<tr><td class=\"Title\">Section & Admin Access: </td><td colspan=\"2\">\n";
			
		//userCategories();				
		userSections($requested_userID);
		
		echo "</td></tr>\n<tr><td class=\"Title\">Is Active: </td><td colspan=\"2\">"
			."<input class=\"RadioBoxes\" type=\"radio\" name=\"fIsActive\" id=\"fIsActive\" value=\"1\" CHECKED />Yes<br />\n"
			."<input class=\"RadioBoxes\" type=\"radio\" name=\"fIsActive\" id=\"fIsActive\" value=\"0\" />No\n"
			."</td></tr>\n";
			//."<input type=\"hidden\" name=\"fPassword\" id=\"fPassword\" />\n";
	}

	echo "<tr>\n"
	   . "<td class=\"Title\">&nbsp;</td>\n"
	   . "<td colspan=\"2\">\n"
	   . "<input class=\"Buttons\" type=\"button\" onclick=\"history.back();\" value=\"&laquo; Cancel\" /><input class=\"Buttons\" type=\"submit\" value=\"Submit &raquo;\" name=\"submitform\"/></td>\n"
	   . "</tr>\n";
	 /* 
	if($action != "DELETE" && $action != "ADDNEW")
	{
		if($userID == 100)
		{		
			echo "<tr>\n"
	           . "<td class=\"Title\">&nbsp;</td>\n"
			   . "<td><img src=\"images/delete.png\" title=\"Cannot Delete\" />This User cannot be Deleted.</td>\n"
			   . "<td align=\"center\"><input class=\"Buttons\" type=\"delete\" value=\"Delete &raquo;\" disabled=\"disabled\"  /></td>\n";
		}
		else
		{
			echo "<tr>\n"
	           . "<td class=\"Title\">&nbsp;</td>\n" 
			   . "<td><img src=\"images/delete.png\" title=\"WARNING\" />WARNING:<br>Deleting a User will <u><strong>permanently</strong></u> delete them from the Database. This process cannot be undone.</td>\n"
			   . "<td align=\"center\"><input class=\"Buttons\" type=\"delete\" value=\"Delete &raquo;\" onclick=\"location.href='adminUserDetails.php?userID=$userID&action=DELETE';return false\"/></td>\n";
		}
	}
	*/
	echo "</tr>\n"
	   . "</tbody></table></form>\n";

	$db->Close();
}



function newsDetails($userID, $newsID, $sectionID, $action, $post, $postingDate, $postingID)
{
	global $db;
	connectDB();
	
	$currentDate = date("m/d/Y");

	if (sizeof($post) > 0) {
        $sqlQuery = "SELECT * "
            . "FROM vAppUser "
            . "WHERE UserID = " . $post['fPostedBy'];

        $rs = $db->Execute($sqlQuery);
        $row = $rs->FetchRow();
    }
	
	switch($action) {
	
		case "ADDNEW":
			$pageTitle = "Add News Post";
			$submitStatus = "Added";
			break;
			
		case "EDIT":
			$pageTitle = "Edit News Post";
			$submitStatus = "Updated";
			break;
	}
	
	if($action == "ADDNEW")
	{
		echo "<table class=\"SearchAndAlert\">\n"
		   . "<tr>\n"
		   . "<td class=\"Action\">".$pageTitle."</td>\n"
		   . "</tr>\n"
		   . "</table>\n"
		   . "<form action=\"news.php?action=".$action."&status=".$submitStatus."\" method=\"post\" name=\"AdminUserDetails\" id=\"AdminUserDetails\" onsubmit=\"return validateNewPostForm(this);\">"
		   . "<input name=\"fuserID\" type=\"hidden\" value=\"".$userID."\" />"
		   . "<table cellspacing=\"0\" border=\"0\" width=\"100%\" id=\"userTable\">\n"
		   . "<thead>\n"
		   . "<tr>\n"
		   . "<th>Item</th>\n"
		   . "<th>Description</th>\n"
		   . "<th><span style=\"float:right;\">\n"
		   . "</span></th></tr>\n</thead>\n<tbody>\n";
		
		echo "<tr>\n"
		   . "<td class=\"Title\">Content: </td><td colspan=\"2\"><textarea rows=\"5\" name=\"fContent\" id=\"fContent\"></textarea></td>\n"
		   . "</tr>\n"
		   . "<tr>\n"
		   . "<td class=\"Title\">Post By: </td>\n"
           . "<td>";
		    drawUserSelect($userID, "fPostedBy", $userID);
		    echo "</td>\n"
           . "</td>\n"
		   . "</tr>\n"
		   . "<tr>\n"
           . "<td class=\"Title\">Post On: </td>\n"
           . "<td>\n"
           . "<input class=\"datesel\" type=\"text\" name=\"fPostedOn\" id=\"fPostedOn\" readonly value=\"$currentDate\">\n"
           . "<img src=\"images/icon_cal.gif\" border=\"0\" title=\"Calendar\" onclick=\"displayCalendar(document.forms[0].fPostedOn, 'mm/dd/yyyy', this);\" onmouseover=\"this.style.cursor='pointer';\" />\n"
           . "<img src=\"images/refresh.png\" border=\"0\" title=\"Clear Date\" onclick=\"document.forms[0].fPostedOn.value = '';\" onmouseover=\"this.style.cursor='pointer';\" />\n"
           . "</td>\n"
		   . "</tr>\n"
		   . "<tr><td class=\"Title\">Post In: </td><td colspan=\"2\">\n";
		   
		   userNewsSection($userID, $action, $sectionID,null,null, null);
			
		echo "</td></tr>\n<tr>\n"
	       . "<td class=\"Title\">&nbsp;</td>\n"
	       . "<td colspan=\"2\">\n"
	       . "<input class=\"Buttons\" type=\"button\" onclick=\"history.back();\" value=\"&laquo; Cancel\" /><input class=\"Buttons\" type=\"submit\" value=\"Submit &raquo;\" name=\"submitform\" /></td>\n"
	       . "</tr>\n" 
		   . "</tr>\n"
		   . "</tbody></table></form>\n";
		   
		
		//postNews();
	}
	elseif($action == "PREVIEW")
	{		
		echo "<table class=\"SearchAndAlert\">\n"
		   . "<tr>\n"
		   . "<td class=\"Action\">News Posted!</td>\n"
		   . "</tr>\n"
		   . "</table>\n"
		   . "<div class=\"newsItem\" style=\"min-width: 282px\">\n"
		   . "<div class=\"dateContainer\"><div class=\"month\">". date("M", strtotime($post['fPostedOn'])) ."</div>\n"
		   . "<div class=\"day\">". date("d", strtotime($post['fPostedOn'])) ."</div>\n"
		   . "</div>\n"
		   . "<div class=\"slugline\">Posted in News Post Preview by ". $row['FirstName'] . " " . $row['LastName'] ."</div>\n"
		   . "<div class=\"newsText\">". $post['fContent'] ."</div>\n"
		   . "</div>\n";
		   
	} elseif($action == "PREVIEW-UPDATE") {		
	
		echo "<table class=\"SearchAndAlert\">\n"
		   . "<tr>\n"
		   . "<td class=\"Action\">News Post Updated!</td>\n"
		   . "</tr>\n"
		   . "</table>\n"
		   . "<div class=\"newsItem\" style=\"min-width: 282px\">\n"
		   . "<div class=\"dateContainer\"><div class=\"month\">". date("M", strtotime($post['fPostedOn'])) ."</div>\n"
		   . "<div class=\"day\">". date("d", strtotime($post['fPostedOn'])) ."</div>\n"
		   . "</div>\n"
		   . "<div class=\"slugline\">Posted in News Post Update Preview by ". $row['FirstName'] . " " . $row['LastName'] ."</div>\n"
		   . "<div class=\"newsText\">". $post['fContent'] ."</div>\n"
		   . "</div>\n";

	
	} elseif($action == "EDIT") {
	
    	$sqlQuery = "SELECT * "
		          . "FROM vAppNews "
                  . "WHERE NewsID = '" . $newsID[0] ."'";

		$rs = $db->Execute($sqlQuery);
		$row = $rs->FetchRow();
		
		
		$postDate = new DateTime($row['PostingDate']);
        $formattedDate = $postDate->format('m/d/Y');

        $newsID_url = base64_encode(implode(",", $newsID));

		echo "<table class=\"SearchAndAlert\">\n"
		   . "<tr>\n"
		   . "<td class=\"Action\">".$pageTitle."</td>\n"
		   . "</tr>\n"
		   . "</table>\n"
		   . "<form action=\"news.php?action=".$action."&status=".$submitStatus."&newsid=" . $newsID_url . "&sectionid=" . $sectionID_url . "&postedbyid=" . $row['PostedByID'] . "&postingdate=". $postedDate_url ."\" method=\"post\" name=\"AdminUserDetails\" id=\"AdminUserDetails\" onsubmit=\"return validateNewPostForm(this);\">"
		   . "<input name=\"fuserID\" type=\"hidden\" value=\"".$userID."\" />"
		   . "<table cellspacing=\"0\" border=\"0\" width=\"100%\" id=\"userTable\">\n"
		   . "<thead>\n"
		   . "<tr>\n"
		   . "<th>Item</th>\n"
		   . "<th>Description</th>\n"
		   . "<th><span style=\"float:right;\">\n"
		   . "</span></th></tr>\n</thead>\n<tbody>\n";
		

		echo "<tr>\n"
		   . "<td class=\"Title\">Content: </td><td colspan=\"2\"><textarea rows=\"5\" name=\"fContent\" id=\"fContent\">" . $row['NewsText'] . "</textarea></td>\n"
		   . "</tr>\n"
		   . "<tr>\n"
           . "<td class=\"Title\">Post By: </td>\n"
           . "<td>";
            drawUserSelect($userID, "fPostedBy", $row['PostedByID']);
        echo "</td>\n"
		   . "</tr>\n"
		   . "<tr>\n"
            . "<td class=\"Title\">Post On: </td>\n"
            . "<td>\n"
            . "<input class=\"datesel\" type=\"text\" name=\"fPostedOn\" id=\"fPostedOn\" readonly value=\"" . $formattedDate . "\">\n"
            . "<img src=\"images/icon_cal.gif\" border=\"0\" title=\"Calendar\" onclick=\"displayCalendar(document.forms[0].fPostedOn, 'mm/dd/yyyy', this);\" onmouseover=\"this.style.cursor='pointer';\" />\n"
            . "<img src=\"images/refresh.png\" border=\"0\" title=\"Clear Date\" onclick=\"document.forms[0].fPostedOn.value = '';\" onmouseover=\"this.style.cursor='pointer';\" />\n"
            . "</td>\n"
		   . "</tr>\n"
		   . "<tr><td class=\"Title\">Post In: </td><td colspan=\"2\">\n";
		   
		   userNewsSection($row['PostedByID'], $action, $sectionID, $postingDate, $postingID, $newsID);
			
		echo "</td></tr>\n<tr>\n"
	       . "<td class=\"Title\">&nbsp;</td>\n"
	       . "<td colspan=\"2\">\n"
	       . "<input class=\"Buttons\" type=\"button\" onclick=\"history.back();\" value=\"&laquo; Cancel\" /><input class=\"Buttons\" type=\"submit\" value=\"Update &raquo;\" name=\"submitform\" /></td>\n"
	       . "</tr>\n";	
		   	   
		echo "</tr>\n"
		   . "</tbody></table></form>\n";
	}
/*
	elseif($action == "EDIT")
	{
		$sqlQuery = "SELECT * "
		          . "FROM vAppNews "
		          . "WHERE NewsID = $newsID[0]";
		$rs = $db->Execute($sqlQuery);
		$row = $rs->FetchRow();
		
		$newsID_url = implode(", ", $newsID);
		$sectionID_url = implode(", ", $sectionID);
		$postedDate_url = $row['PostingDate'];

		echo "<table class=\"SearchAndAlert\">\n"
		   . "<tr>\n"
		   . "<td class=\"Action\">".$pageTitle."</td>\n"
		   . "</tr>\n"
		   . "</table>\n"
		   . "<form action=\"news.php?action=".$action."&status=".$submitStatus."&newsid=" . base64_encode($newsID_url) . "&sectionid=" . base64_encode($sectionID_url) . "&postedbyid=".base64_encode($row['PostedByID'])."&postingdate=".base64_encode($postedDate_url)."\" method=\"post\" name=\"AdminUserDetails\" id=\"AdminUserDetails\" onsubmit=\"return validateTheForm(this);\">"
		   . "<input name=\"fuserID\" type=\"hidden\" value=\"".$userID."\" />"
		   . "<table cellspacing=\"0\" border=\"0\" width=\"100%\" id=\"userTable\">\n"
		   . "<thead>\n"
		   . "<tr>\n"
		   . "<th>Item</th>\n"
		   . "<th>Description</th>\n"
		   . "<th><span style=\"float:right;\">\n"
		   . "</span></th></tr>\n</thead>\n<tbody>\n";
		
	  //echo "<tr>\n"
		 //. "<td class=\"Title\">Post Title: </td><td colspan=\"2\"><input type=\"text\" name=\"fPostTitle\" id=\"fPostTitle\" value=\"\"/></td>\n"
		 //. "</tr>\n";
		echo "<tr>\n"
		   . "<td class=\"Title\">Content: </td><td colspan=\"2\"><textarea rows=\"5\" name=\"fContent\" id=\"fContent\">" . $row['NewsText'] . "</textarea></td>\n"
		   . "</tr>\n"
		   . "<tr>\n"
		   . "<td class=\"Title\">Post By: </td><td colspan=\"2\"><input type=\"text\" name=\"fPostedBy\" id=\"fPostedBy\" value=\"" . $row['FirstName'] ." ". $row['LastName'] ."\" readonly/></td>\n"
		   . "</tr>\n"
		   . "<tr>\n"
		   . "<td class=\"Title\">Post On: </td><td colspan=\"2\"><input type=\"text\" name=\"fPostedOn\" id=\"fPostedOn\" value=\"" . date("m/d/Y", strtotime($row['PostingDate'])) . "\" readonly/></td>\n"
		   . "</tr>\n"
		   . "<tr><td class=\"Title\">Post In: </td><td colspan=\"2\">\n";
		   
		   userNewsSection($userID, $action, $sectionID);
			
		echo "</td></tr>\n<tr>\n"
	       . "<td class=\"Title\">&nbsp;</td>\n"
	       . "<td colspan=\"2\">\n"
	       . "<input class=\"Buttons\" type=\"button\" onclick=\"history.back();\" value=\"&laquo; Cancel\" /><input class=\"Buttons\" type=\"submit\" value=\"Update &raquo;\" name=\"submitform\" /></td>\n"
	       . "</tr>\n";	
		   	   
		echo "</tr>\n"
		   . "</tbody></table></form>\n";
	}
	*/
	
	$db->Close();
}

/*

//ouput the first field in the row
$db->SetFetchMode(ADODB_FETCH_ASSOC);
echo $row['UserID'];

//set the fetch mode to numeric
$db->SetFetchMode(ADODB_FETCH_NUM);
echo $row[0];

*/

function getUserFormData() 
{
	//initialize
	$FirstName		= "";
	$LastName		= "";
	$UserName		= "";
	$Password		= "";
	$Address1		= "";

	$Address2		= "";
	$City			= "";
	$Province		= "";
	$PostalCode		= "";
	$PrimaryPhone	= "";

	$SecondaryPhone	= "";
	$Mobile			= "";
	$PrimaryEmail	= "";
	$SecondaryEmail	= "";
	$TextMessaging	= "";

	$Comments		= "";
	$SuperAdminComments	= "";
	
	$Section		= "";
	$IsActive		= "";
	$UserID			= "";
	
	$DateofBirth	= "";
	$VolunteerSince	= "";
	$DivisionId = "";
	
		
	//load data
	$FirstName		= str_replace("'", "", strip_tags(ucwords($_POST['fFirstName'])));
	$LastName		= str_replace("'", "", strip_tags(ucwords($_POST['fLastName'])));
	$UserName		= str_replace("'", "", strip_tags($_POST['fUsername']));
	$Password		= $_POST['fPassword'];
	$Address1		= $_POST['fAddress1'];

	$Address2		= $_POST['fAddress2'];
	$City			= $_POST['fCity'];
	$Province		= $_POST['fProvince'];
	$PostalCode		= strtoupper($_POST['fPostalCode']);
	$PrimaryPhone	= $_POST['fPrimaryPhone'];

	$SecondaryPhone	= $_POST['fSecondaryPhone'];
	$Mobile			= $_POST['fMobile'];
	$PrimaryEmail	= $_POST['fPrimaryEmail'];
	$SecondaryEmail	= $_POST['fSecondaryEmail'];
	$TextMessaging	= $_POST['fTextMessaging'];

	$Comments		= str_replace("'", "", strip_tags($_POST['fComments']));
	$SuperAdminComments	= str_replace("'", "", strip_tags($_POST['fSuperAdminComments']));
	
	$Section		= $_POST['fSection'];
	$IsActive		= $_POST['fIsActive'];
	$UserID			= $_POST['fUserID'];
	
	if($_POST['fDateofBirth'] == "12/31/1969") {
		$DateofBirth = "";
	} else {
		$DateofBirth	= $_POST['fDateofBirth'];
	}
	$VolunteerSince	= $_POST['fVolunteerSince'];

	$DivisionId = $_POST['fDivision'];

	if (empty($DivisionId)) {
	    $DivisionId = "NULL";
    }

	$formValues = array($FirstName, $LastName, $UserName, $Password, $Address1, $Address2, $City, $Province, $PostalCode, $PrimaryPhone, $SecondaryPhone, $Mobile, $PrimaryEmail, $SecondaryEmail, $TextMessaging, $Comments, $SuperAdminComments, $IsActive, $UserID, $DateofBirth, $VolunteerSince, $DivisionId);
	return $formValues;
}






function getUserSectionAndAdminData($current_userID, $requested_userID, $post, $action, $status, $status2) 
{
	global $db;
	connectDB();
	$sqlQuery = "SELECT * "
			  . "FROM vAppAdmin "
			  . "WHERE UserID = " . $current_userID . " "
			  . "AND IsAdmin = 1";
	$rs = $db->Execute($sqlQuery);
	$row = $rs->FetchRow();
	
	if ($rs->RowCount()) 
	{
		$sectionStatus	= array();
		$adminStatus	= array();
		
		$sqlQuery = "SELECT * "
				  . "FROM AppSection";
		$rs = $db->Execute($sqlQuery);
	
		while($row = $rs->FetchRow())
		{		
			$sectionName = str_replace(" ", "", $row['SectionName']);
//			$sectionName = str_replace("(", "", $sectionName);			
			foreach($post as $name => $value)
			{	
				if($name == "f" . $sectionName)
				{			
					$sectionStatus[$row['SectionID']] = $row['SectionID'];
				}
					
				if($name == "f" . $sectionName . "IsAdmin")
				{			
					$adminStatus[$row['SectionID']] = $value;
				}

			}
		}
		
		
		
		
		if($action == "EDIT" && $status = "Updated" && !userIsSuperAdmin($requested_userID)) 
		{
			$sqlQuery = "DELETE FROM AppAdmin "
					  . "WHERE UserID = " . $requested_userID; 
			$rs2 = $db->Execute($sqlQuery);
		}
	
	
	
	
		for($i = 1; $i <= $rs->RowCount(); $i++)
		{
		
			if(empty($sectionStatus[$i]))
			{
				$sectionStatus[$i] = 0;
			}
			
			if(empty($adminStatus[$i]))
			{
				$adminStatus[$i] = 0;
			}
			
			if($action == "ADDNEW")
			{
				if($sectionStatus[$i] != 0)
				{
					$sqlQuery = "INSERT INTO AppAdmin "
							  . "(UserID, SectionID, IsAdmin) "
							  . "VALUES (" . $requested_userID . ", " . $sectionStatus[$i] . ", " . $adminStatus[$i] . ")";
					$rs3 = $db->Execute($sqlQuery);
				}
			}	
			elseif($action == "EDIT" && !userIsSuperAdmin($requested_userID))
			{	
				if($sectionStatus[$i] != 0)
				{  
					$sqlQuery = "INSERT INTO AppAdmin "
							  . "(UserID, SectionID, IsAdmin) "
							  . "VALUES (" . $requested_userID . ", " . $sectionStatus[$i] . ", " . $adminStatus[$i] . ")";
					$rs3 = $db->Execute($sqlQuery);  
				}	
			}
		}
	}
	$db->Close();
}





function getNewsPostData()
{	
	$PostByID 		= $_POST['fPostedBy'];
	$Content 		= $_POST['fContent'];
	$postingOn      = $_POST['fPostedOn'];
	
	$formValues = array($PostByID, $Content, $postingOn);
	return $formValues;
}


function checkIfUserExists($firstName, $lastName, $email) {

	global $db;
	connectDB();
	$userExists = false;

	$sqlQuery	= "SELECT * FROM AppUser WHERE (Email1='$email') OR ((FirstName='$firstName') AND (LastName='$lastName'))";
	$rs = $db->Execute($sqlQuery);

	//if the rowcount is not zero it returned rows
	if ($rs->RowCount()) {
		$userExists = true;
	}
	
	$db->Close();
	return $userExists;
		
}

function updateUsers($action, $formValues) {
	
	global $db, $currentUserID;

	$sqlQuery = "";
	
	switch ($action) {
		
	case "ADDNEW":
		if(checkIfUserExists($formValues[0], $formValues[1], $formValues[12]) === false) 
		{
			
			$sqlQuery = "INSERT INTO AppUser "
				      . "(
						  FirstName, 
						  LastName, 
						  Username, 
						  Password, 
						  Address1, 
						  Address2, 
						  City, 
						  Province, 
						  PostalCode, 
						  Phone1, 
						  Phone2, 
						  PhoneCell, 
						  Email1, 
						  Email2, 
						  TextMessage, 
						  Comment, 
						  SuperAdminComment, 
						  IsActive, 
						  DateofBirth, 
						  VolunteerSince,
						  DivisionId) "
				      . "VALUES (
						  '$formValues[0]',  
						  '$formValues[1]', 
						  '$formValues[2]', 
						  '$formValues[3]', 
						  '$formValues[4]', 
						  '$formValues[5]', 
						  '$formValues[6]', 
						  '$formValues[7]', 
						  '$formValues[8]', 
						  '$formValues[9]',
						  '$formValues[10]', 
						  '$formValues[11]', 
						  '$formValues[12]',
						  '$formValues[13]', 
						  '$formValues[14]', 
						  '$formValues[15]', 
						  '$formValues[16]', 
						  $formValues[17], 
						  '$formValues[19]', 
						  '$formValues[20]',
						  $formValues[21]
						  )";

			$output = $formValues[0]." ".$formValues[1]." has been added to the system !";

		}
		else
		{	
			$output = "Oops! Either '<u>".$formValues[0]." ".$formValues[1]."</u>' already exists or someone is already using '<u>".$formValues[12]."</u>' as their primary email address !";
		}
		break;

	case "EDIT":

		if( userIsSuperAdmin($currentUserID) ) {

			$sqlQuery = "UPDATE AppUser "
			          . "SET FirstName='$formValues[0]', LastName='$formValues[1]', Username='$formValues[2]', ".(empty($formValues[3]) ? "": "Password='$formValues[3]', ")."Address1='$formValues[4]', Address2='$formValues[5]', City='$formValues[6]', Province='$formValues[7]', PostalCode='$formValues[8]', Phone1='$formValues[9]', Phone2='$formValues[10]', PhoneCell='$formValues[11]', Email1='$formValues[12]', Email2='$formValues[13]', TextMessage='$formValues[14]', Comment='$formValues[15]', SuperAdminComment='$formValues[16]', IsActive=$formValues[17], DateofBirth='$formValues[19]', VolunteerSince='$formValues[20]', DivisionId=$formValues[21] "
				      . "WHERE UserID = $formValues[18]";

		} else {
		
			$sqlQuery = "UPDATE AppUser "
			          . "SET FirstName='$formValues[0]', LastName='$formValues[1]', Username='$formValues[2]', ".(empty($formValues[3]) ? "": "Password='$formValues[3]', ")."Address1='$formValues[4]', Address2='$formValues[5]', City='$formValues[6]', Province='$formValues[7]', PostalCode='$formValues[8]', Phone1='$formValues[9]', Phone2='$formValues[10]', PhoneCell='$formValues[11]', Email1='$formValues[12]', Email2='$formValues[13]', TextMessage='$formValues[14]', Comment='$formValues[15]', IsActive=$formValues[17], DateofBirth='$formValues[19]', VolunteerSince='$formValues[20]',DivisionId=$formValues[21] "
				      . "WHERE UserID = $formValues[18]";
		
		}

		$output = $formValues[0]." ".$formValues[1]."'s profile has been updated !";
		break;
	
	case "DELETE":
		$sqlQuery = "DELETE FROM AppUser WHERE UserID = $formValues[17]";
		
		$output = "user deleted";
		break;
	}
	
	if($sqlQuery != "") {
		connectDB();
		$rs1 = $db->Execute($sqlQuery);
		$db->Close();
	}

	return $output;
}





function updateNews($action, $post, $formValues, $newsID, $sectionID, $postedbyID, $postingDate) 
{
	global $db;
	connectDB();
	
	$sectionStatus	= array();
	
	$sqlQuery = "SELECT * "
	          . "FROM AppSection";
	$rs = $db->Execute($sqlQuery);
	$sectionCount = $rs->RowCount();

	while($row = $rs->FetchRow())
	{		
		$sectionName = str_replace(" ", "", $row['SectionName']);
//		$sectionName = str_replace("(", "", $sectionName);
//		$sectionName = str_replace(")", "", $sectionName);
		
		foreach($post as $name => $value)
		{	
			if($name == "f" . $sectionName)
			{			
				$sectionStatus[$row['SectionID']] = $row['SectionID'];
			}
		}
	}

	if($action == "ADDNEW")
	{
		for($i = 1; $i <= $sectionCount; $i++)
		{		
			if(empty($sectionStatus[$i]))
			{
				$sectionStatus[$i] = 0;
			}
			
			if($sectionStatus[$i] != 0)
			{
				$sqlQuery = "INSERT INTO AppNews "
						  . "(SectionID, PostedByID, NewsText, PostingDate) "
						  . "VALUES (" . $sectionStatus[$i] . ", " . $formValues[0] . ", '" . str_replace("'", "", strip_tags($formValues[1])) . "', '" . date('Y-m-d', strtotime($formValues[2])) . "')";
				$rs2 = $db->Execute($sqlQuery);
			}
		}
		
		$output = "PREVIEW";
	}
	elseif($action == "DELETE")
	{
		$count = count($formValues);

		for($i = 0; $i <= $count; $i++)
		{
			$sqlQuery = "DELETE "
			          . "FROM AppNews "
			          . "WHERE (NewsID = " . $formValues[$i] . ") ";
			$rs = $db->Execute($sqlQuery);
			
			header("Location: index.php");
		}
	}
	elseif($action == "EDIT")
	{
        $newsArray = explode(',',$newsID[0]);
		$count = count($newsArray);

		for($i = 0; $i < $count; $i++) {	
			$sqlQuery = "DELETE "
			          . "FROM AppNews "
				      . "WHERE (NewsID = " . $newsArray[$i] . ")";
			$rs = $db->Execute($sqlQuery);
			
		} 
		
		for($i = 1; $i <= $sectionCount; $i++) {

			if(empty($sectionStatus[$i])) {

				$sectionStatus[$i] = 0;

			} if($sectionStatus[$i] != 0) {

				$sqlQuery = "INSERT INTO AppNews "
						  . "(SectionID, PostedByID, PostingDate, NewsText) "
						  . "VALUES (" . $sectionStatus[$i] . ", " .  $formValues[0] . ", '" . date('Y-m-d', strtotime($formValues[2])) . "', '" . str_replace("'", "", strip_tags($formValues[1])) . "')";
				$rs = $db->Execute($sqlQuery);

			}

		}
		
		$output = "PREVIEW-UPDATE";

	}
	
	$db->Close();
	return $output;
	
}

function userCategories() {

	global $db;
	connectDB();

	$sqlQuery = "SELECT * FROM AppSection "
	          . "ORDER BY SectionName ASC";

	$rs = $db->Execute($sqlQuery);

	if ($rs->RowCount()) {

		echo "<select name=\"fSection\" id=\"fSection\"><option value=\"0\">Assign this user a section...</option>\n<option value=\"0\">-------------------</option>\n";

		while ($row = $rs->FetchRow()) {
			//if ($currentCat == $row['SectionID']) {
			//	echo "<option SELECTED value=\"". $row['SectionID'] ."\">". $row['SectionName'] ."</option>\n";
			//} else {
				echo "<option value=\"". $row['SectionID'] ."\">". $row['SectionName'] ."</option>\n";
			//}
		}

		echo "</select>";
	
	}
	
	$db->Close();

}



function userSections($userID) {

	global $db;
	connectDB();
	$checkIt = "";

	$sqlQuery = "SELECT * FROM AppSection ORDER BY SectionID ASC";

	$rs = $db->Execute($sqlQuery);

	if ($rs->RowCount()) {

		echo "<table class=\"SectionList\">\n";
		$results = $rs->getAll();

		// echo "<pre>";
		// print_r($results);
		// echo "</pre>";

		foreach ($results as $row) {
		// while ($row = $rs->FetchRow()) {
			//echo $i;
			
			$sectionName = str_replace(" ","",$row['SectionName']);
			$sectionName = "f".$sectionName;

			$checkIt = getUserSectionAccess($userID, $row['SectionID'], "SECTION");
			echo "<tr>\n<td style=\"width:350px; border: 0px; border-color: #FFFFFF\"><input type=\"checkbox\" class=\"CheckBoxes\" name=\"". $sectionName ."\"  id=\"". $sectionName ."\" value=\"1\" $checkIt border=\"0\" /><span>" . $row['SectionName'] . "</span></td>\n";

			$sectionNameAdmin = $sectionName . "IsAdmin";
			
			$checkIt = getUserSectionAccess($userID, $row['SectionID'], "ADMIN");
			echo "<td><input type=\"checkbox\" class=\"CheckBoxes\" name=\"". $sectionNameAdmin ."\" id=\"". $sectionNameAdmin ."\" value=\"1\" $checkIt border=\"0\" /><span>Is Admin</span></td>\n</tr>\n";

			$checkIt = "";
			$i++;	
			
		}
		
		echo "</table>";
	
		//echo "</div>";
	
	}
	
	$db->Close();

}

function userNewsSection($userID, $action, $sectionID, $postingDate, $postingID, $newsID)
{
	global $db;
	connectDB();
	//$checkIt = "";
	
//	if($action != "EDIT")
//	{
		$sqlQuery = "SELECT * "
				  . "FROM AppSection "
				  . "INNER JOIN AppAdmin "
				  . "ON AppSection.SectionID = AppAdmin.SectionID "
				  . "WHERE AppAdmin.UserID = $userID "
				  . "ORDER BY AppSection.SectionName ASC";
/*				  
		$sqlQuery = "SELECT * "
				  . "FROM AppSection ";
*/				  
		$rs = $db->Execute($sqlQuery);
	
		if ($rs->RowCount()) {
	
			echo "<table id=\"SectionList\" class=\"SectionList\">\n";
	
			while ($row = $rs->FetchRow()) {
			
				$sectionName = str_replace(" ","",$row['SectionName']);
				$sectionName = "f".$sectionName;
	
				//$checkIt = getUserSectionAccess($userID, $row['SectionID'], "SECTION");

                $count = 0;
                if ($newsID != null) {

                    $count = count($newsID);
                    $formattedNewsID = '';

                    for($i = 0; $i < $count; $i++)
                    {
                        $formattedNewsID .= "'" . $newsID[$i] . "',";
                    }

                    //remove the extra comma
                    $formattedNewsID = rtrim($formattedNewsID,",");

                    $sqlQuery2 = "SELECT SectionName "
                        . "FROM vAppNews "
                        . "WHERE NewsID in (" . $formattedNewsID . ")";

                    $rs2 = $db->Execute($sqlQuery2);
                    //$sectionCount = $rs2->RowCount();

                    $count = 0;


                    while($row2 = $rs2->FetchRow()) {

                        if($row2['SectionName'] == $row['SectionName']) {
                            echo "<tr>\n<td style=\"width:250px;\"><input type=\"checkbox\" class=\"CheckBoxes\" name=\"". $sectionName ."\"  id=\"". $sectionName ."\" value=\"1\" CHECKED /><span>" . $row['SectionName'] . "</span></td>\n";
                            $count = 1;
                        }

                    }
                }
				
				if($count == 0){
					echo "<tr>\n<td style=\"width:250px;\"><input type=\"checkbox\" class=\"CheckBoxes\" name=\"". $sectionName ."\"  id=\"". $sectionName ."\" value=\"1\" /><span>" . $row['SectionName'] . "</span></td>\n";
				}
							
			}
			
			echo "</table>";
			
			
		    echo "<input type=\"hidden\" id=\"hdnNewsId\" value=\"" . $formattedNewsID . "\" />\n";
		
		}
		

		
		
				
	/*}
	elseif($action == "EDIT")
	{
		$count = count($sectionID);
		
		$sqlQuery = "SELECT * "
				  . "FROM AppSection "
				  . "INNER JOIN AppAdmin "
				  . "ON AppSection.SectionID = AppAdmin.SectionID "
				  . "WHERE AppAdmin.UserID = $userID AND ";
				  
		for($i = 0; $i < $count; $i++)
		{  
			if($i < 1)
			{
				$sqlQuery .= "((AppSection.SectionID = " . $sectionID[$i] . ") ";
			}
			else
			{
				$sqlQuery .= "OR (AppSection.SectionID = " . $sectionID[$i] . ") ";
			}
		}	  
		$sqlQuery .= ") "	  
	               . "ORDER BY AppSection.SectionName ASC";
		$rs = $db->Execute($sqlQuery);
		
		echo "<table class=\"SectionList\">\n";
		
		while($row = $rs->FetchRow()) 
		{
			$sectionName = str_replace(" ","",$row['SectionName']);
			$sectionName = str_replace("(","",$sectionName);
			$sectionName = str_replace(")","",$sectionName);
			$sectionName = "f".$sectionName;
				
			echo "<tr>\n<td style=\"width:250px;\"><input type=\"checkbox\" class=\"CheckBoxes\" name=\"". $sectionName ."\"  id=\"". $sectionName ."\" value=\"1\" checked=\"checked\" /><span>" . $row['SectionName'] . "</span></td>\n";
		}
		
		$sqlQuery = "SELECT * "
				  . "FROM AppSection "
				  . "INNER JOIN AppAdmin "
				  . "ON AppSection.SectionID = AppAdmin.SectionID "
				  . "WHERE AppAdmin.UserID = $userID AND ";
				  
		for($i = 0; $i < $count; $i++)
		{  
			if($i < 1)
			{
				$sqlQuery .= "((AppSection.SectionID != " . $sectionID[$i] . ") ";
			}
			else
			{
				$sqlQuery .= "AND (AppSection.SectionID != " . $sectionID[$i] . ") ";
			}
		}	  
		$sqlQuery .= ") "	  
	               . "ORDER BY AppSection.SectionName ASC";
		$rs = $db->Execute($sqlQuery);
		
		while($row = $rs->FetchRow()) 
		{
			$sectionName = str_replace(" ","",$row['SectionName']);
			$sectionName = str_replace("(","",$sectionName);
			$sectionName = str_replace(")","",$sectionName);
			$sectionName = "f".$sectionName;
			
			echo "<tr>\n<td style=\"width:250px;\"><input type=\"checkbox\" class=\"CheckBoxes\" name=\"". $sectionName ."\"  id=\"". $sectionName ."\" value=\"1\" /><span>" . $row['SectionName'] . "</span></td>\n";
		}
		
		echo "</table>";
	}*/
	
	$db->Close();
}




function getUserSectionAccess($userID, $sectionID, $lookAt) {

	global $db;
	connectDB();
	$output = "";

	$sqlQuery = "SELECT SectionID, IsAdmin FROM AppAdmin WHERE UserID = $userID";
	$rs = $db->Execute($sqlQuery);

	if ($rs->RowCount()) {
		while ($row = $rs->FetchRow()) {
			
			switch ($lookAt) {

				case "SECTION":
					if($row['SectionID'] == $sectionID)	$output = "checked=\"checked\"";
					break;

				case "ADMIN":
					if($row['SectionID'] == $sectionID && $row['IsAdmin'] == 1)	$output = "checked=\"checked\"";
					break;
			}

		}
	}
	
	$db->Close();

	return $output;

}







?>

