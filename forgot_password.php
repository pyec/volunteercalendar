<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');

require("header.php");

$returnVal = "Please enter your username and email address to verifie and resend your password";
if (array_key_exists('fUsername', $_POST)) $returnVal = userLogin($_POST['fUsername'], $_POST['fPassword']);
?>
		<div id="login">
			<form action="login.php" method="post" id="loginform">
				<h3>HRP Volunteers</h3>
				<table border="0" cellpadding="0" cellspacing="0" align="Center">
				<tr>
					<td colspan="2"><div class="loginmsg"><?= $returnVal ?></div></td>
				</tr>
				<tr>
					<td><label for="fUsername">Username</label></td>
					<td><input type="text" name="fUsername" id="fUsername" tabindex="1" /></td>
				</tr>
				<tr>
					<td><label for="fPassword">Password</label></td>
					<td><input type="password" name="fPassword" id="fPassword" tabindex="2" autocomplete="off" /></td>
				</tr>
				<tr>
					<td colspan="2">
						<a href="forgot_password.php" class="loginforgot">Forgot password?</a>
						<input type="submit" value="Sign in" class="loginbutton" tabindex="3" />
					</td>
				</tr>
				</table>
			</form>
		</div>

<?php require("footer.php"); ?>
