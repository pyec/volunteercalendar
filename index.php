<?php 
 /*---------------------------------------------------------------
   * 
   * 	MODULE:		index.php
   * 	AUTHOR:		Gian
   * 	Created:	Unknown
   * 
   * --------------------------------------------------------------
   * 
   * 	
   * 	MODIFICATION HISTORY
   * 	20170521 PRSC	Cleanup paths, code and PHP tabs fixed.
   *    * 
   *---------------------------------------------------------------
   */


//    $basepath = $_SERVER['DOCUMENT_ROOT']."/police/VolunteerCalendar";
    $basepath = "C:\inetpub\wwwroot\VolunteerCalendar";
    
	require ($basepath . '\header.php');


//   require("header.php");

?>

<?php
$action = $_REQUEST['action'];
//print "ACTION "  . $action;



if($action == "DELETE")
{
	$newsID = explode(", ", base64_decode($_REQUEST['newsid']));
	updateNews($action, '', $newsID, null, null, null, null, null);
}

?>
    <table>
    <tr>
        <td width="40%" valign="top" class="newscolumn">
            <h1>Latest News</h1>
            <? //printNewsSP("BRIEF", 0, $currentUserID); ?>
            <?php printNews(10, 0, $currentUserID); ?>
        </td>
        <td valign="top">
            <h1>Upcoming Shifts (Next 7 Days)</h1>
            <? printShifts($currentUserID); ?>
            <div class="sectionfooter"><a href="shifts.php">View All</a></div>
    
            <!--
            <h1>Newest Messages</h1>
            <? //printMessages(20, $currentUserID) ?>
            <div class="sectionfooter"><a href="shifts.php">View All</a></div>
            -->
        </td>
    </tr>
    </table>

<?php require("footer.php"); ?>