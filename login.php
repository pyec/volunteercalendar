<?php
/*---------------------------------------------------------------
   * 
   * 	MODULE:		index.php
   * 	AUTHOR:		Gian
   * 	Created:	Unknown
   * 
   * --------------------------------------------------------------
   * 
   * 	
   * 	MODIFICATION HISTORY
   * 	20170521 PRSC	Cleanup paths, code and PHP tabs fixed.
   *    * 
   *---------------------------------------------------------------
   */


 //   $basepath = $_SERVER['DOCUMENT_ROOT']."/police/VolunteerCalendar";
    $basepath = "C:\inetpub\wwwroot\VolunteerCalendar";
    
	require ($basepath . '\header.php');

//include("header.php");

if ($_GET['action'] == "FORGOT")
{	
	if($_POST['fEmail'])
	{
		// echo $_POST['fEmail'];
		userForgetPassword($_POST['fEmail']);
	}
	
	if($_REQUEST['status'] == "OK")
	{
		?>
			<div id="login">
				<form action="login.php?action=FORGOT" method="post" id="loginform">
					<h3>HRP Volunteers</h3>
					<h5>Forgot Password</h5>
					<table border="0" cellpadding="0" cellspacing="0" align="Center">
						<tr>
							<td colspan="2"><div class="loginmsg"><font color="green"><strong>Account Verified!</strong><br/> Please check your email.</font></div></td>
						</tr>
						<tr>
							<td><label for="fEmail">Email</label></td>
							<td><input type="text" name="fEmail" id="fEmail" tabindex="2" autocomplete="off" /></td>
						</tr>
						<tr>
							<td colspan="2">
								<a href="login.php" class="loginforgot">Return to Login</a>
								<input type="submit" value="Resend" class="loginbutton" tabindex="3" />
							</td>
						</tr>
					</table>
				</form>
                <p style="border-top:1px solid #ccc; padding-top:10px;margin-top:12px">If you are not a current volunteer, please visit our 
                <a href="http://www.halifax.ca/Police/Programs/volunteering.php">volunteer opportunities page</a>.</p>
			</div>
		<?php 
	}
	elseif($_REQUEST['status'] == "FAIL")
	{
		?>
			<div id="login">
				<form action="login.php?action=FORGOT" method="post" id="loginform">
					<h3>HRP Volunteers</h3>
					<h5>Forgot Password</h5>
					<table border="0" cellpadding="0" cellspacing="0" align="Center">
						<tr>
							<td colspan="2"><div class="loginmsg"><font color="red"><strong>Account Verification Failed!</strong><br/> Please try again.</font></div></td>
						</tr>
						<tr>
							<td><label for="fEmail">Email</label></td>
							<td><input type="text" name="fEmail" id="fEmail" tabindex="2" autocomplete="off" /></td>
						</tr>
						<tr>
							<td colspan="2">
								<a href="login.php" class="loginforgot">Return to Login</a>
								<input type="submit" value="Resend" class="loginbutton" tabindex="3" />
							</td>
						</tr>
					</table>
				</form>
                <p style="border-top:1px solid #ccc; padding-top:10px;margin-top:12px">If you are not a current volunteer, please visit our <a href="http://www.halifax.ca/Police/Programs/volunteering.php">volunteer opportunities page</a>.</p>
			</div>
		<?php 
	}
	else
	{
		?>
			<div id="login">
				<form action="login.php?action=FORGOT" method="post" id="loginform">
					<h3>HRP Volunteers</h3>
					<h5>Forgot Password</h5>
					<table border="0" cellpadding="0" cellspacing="0" align="Center">
						<tr>
							<td colspan="2"><div class="loginmsg">Please enter your username and email address to verify and resend your password.</div></td>
						</tr>
						<tr>
							<td><label for="fEmail">Email</label></td>
							<td><input type="text" name="fEmail" id="fEmail" tabindex="2" autocomplete="off" /></td>
						</tr>
						<tr>
							<td colspan="2">
								<a href="login.php" class="loginforgot">Return to Login</a>
								<input type="submit" value="Resend" class="loginbutton" tabindex="3" />
							</td>
						</tr>
					</table>
				</form>
                <p style="border-top:1px solid #ccc; padding-top:10px;margin-top:12px">If you are not a current volunteer, please visit our <a href="http://www.halifax.ca/Police/Programs/volunteering.php">volunteer opportunities page</a>.</p>
			</div>
		<?php 
	}
}
else
{
	$returnVal = "Please enter your username and password to sign in to the HRP Volunteer application.";
	// if (array_key_exists('fUsername', $_POST)) $returnVal = userLogin($_POST['fUsername'], $_POST['fPassword']);
	if ($_POST['fUsername'] && $_POST['fPassword'])
	{
		// echo "here";
		$returnVal = userLogin($_POST['fUsername'], $_POST['fPassword']);
	}
	
	?>
			<div id="login">
				<form action="login.php" method="post" id="loginform">
					<h3>HRP Volunteers</h3>
                    <h5>Login</h5>
					<table border="0" cellpadding="0" cellspacing="0" align="Center">
                        <tr>
                            <td colspan="2"><div class="loginmsg"><?= $returnVal ?></div></td>
                        </tr>
                        <tr>
                            <td><label for="fUsername">Username</label></td>
                            <td><input type="text" name="fUsername" id="fUsername" tabindex="1" maxlength="21" /></td>
                        </tr>
                        <tr>
                            <td><label for="fPassword">Password</label></td>
                            <td><input type="password" name="fPassword" id="fPassword" tabindex="2" autocomplete="off" maxlength="21"/></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <a href="login.php?action=FORGOT" class="loginforgot">Forgot password?</a>
                                <input type="submit" value="Sign in" class="loginbutton" tabindex="3" />
                            </td>
                        </tr>
					</table>
				</form>
                <p style="border-top:1px solid #ccc; padding-top:10px;margin-top:12px">If you are not a current volunteer, please visit our <a href="http://www.halifax.ca/Police/Programs/volunteering.php">volunteer opportunities page</a>.</p>
			</div>
	<?php 
}

if ($_GET['action'] == "LOGOUT") userLogout();

require("footer.php"); 
?>