<?php 
  /*---------------------------------------------------------------
   * 
   * 	MODULE:		AdminUserDetails.php
   * 	AUTHOR:		Gian
   * 	Created:	Unknown
   * 
   * --------------------------------------------------------------
   * 
   * 	
   * 	MODIFICATION HISTORY
   * 	20170521 PRSC	Cleanup paths, code and PHP tabs fixed.
   *    * 
   *---------------------------------------------------------------
   */


    $basepath = $_SERVER['DOCUMENT_ROOT']."/VolunteerCalendar";

	require($basepath . "/header.php");

	$userID = $_GET['userID'];
	$action = $_GET['action'];
	$status = $_GET['status'];
	$status2 = $_GET['status2'];

	if(userIsAdmin($currentUserID) || $currentUserID == $userID) {

	if ($_POST) 
	{
		//echo "<pre>";
		//print_r(getUserFormData());
		//echo "</pre>";
		
		$status2 = updateUsers($action, getUserFormData());
		
		getUserSectionAndAdminData($currentUserID, $userID, $_POST, $action, $status, $status2);
	
	}
	
	/*echo "<br/><table>";
	foreach($_POST as $name => $value)
	{
		echo "<tr><td>$name: </td><td>$value</td></tr>";
	}
	echo "<table>";*/
	
	userDetails($currentUserID, $userID, $action, $status, $status2);

} else {

	echo "<p style='margin-top:93px;color:red;'><b>You do not have permission to view/edit this users information. </b><br />Please choose an option in the menu to continue.</p>";

}

require($basepath . "/footer.php"); 

?>