<a href="http://www.halifax.ca/" title="Halifax.ca"><img src="images/logo.gif" border="0" alt="Halifax Regional Municipality" width="169" height="85" /></a>
<?php
/*
Added by Brandon
Replaces the oringal HTML with a PHP function that checks if the logged in user currently has admin rights
*/
printNavigation($currentUserID);

if (strpos($curURL, "cal.php")) 
{ 
?>
	<div class="navMenu">
		<div class="subMenu">Colour Legend</div>
		<?php printCategories("SELECT", $currentCat, $currentUserID, true); ?>
        <?php printCategories("LEGEND", 0, $currentUserID, false); ?>
	</div>
<?php 
} 
?>

<div class="navMenu">
	<h1>Current User</h1>
	<p>
		<?php echo $currentName ?>
        <br />
        <?php echo $currentPhone ?>
        <br />
		<?php echo $currentEmail ?>
        <br />
	</p>
</div>