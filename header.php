<?php
  /*---------------------------------------------------------------
   * 
   * 	MODULE:		header.php
   * 	AUTHOR:		Gian
   * 	Created:	Unknown
   * 
   * --------------------------------------------------------------
   * 
   * 	
   * 	MODIFICATION HISTORY
   * 	20170521 PRSC	Cleanup paths, code and PHP tabs fixed.
   *    * 
   *---------------------------------------------------------------
   */


//    $basepath = $_SERVER['DOCUMENT_ROOT']."/police/VolunteerCalendar";
    $basepath = "C:\inetpub\wwwroot\VolunteerCalendar";
    
	require ($basepath . '\init.php');
	require ($basepath . '\includes\adminFunctions.php');
	require ($basepath . '\includes\functions.php');
	
?>

<!DOCTYPE HTML>
<html lang="en-ca">
<head>
	<title>HRP Volunteers </title>

	<link rel="stylesheet" href="css/default.css" media="all" type="text/css" />   
    <link rel="stylesheet" href="css/dhtmlgoodies_calendar.css" media="screen,projection" type="text/css" />
	<link rel="stylesheet" href="css/modalbox.css" type="text/css" media="screen" />

    <script type="text/javascript" src="js/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
	<script type="text/javascript" src="js/dhtmlgoodies_calendar.js"></script>
    <script type="text/javascript" src="js/ajax.js"></script>


	</script>
    
    <style type="text/css">
		#SectionFlag {
			display:none;
		}
	</style>

</head>

<body>

	<?php
//    print "CUR  [" . $curURL;							//DBG PRSC
	if (!strpos($curURL, "login.php")) { ?>
    <div class="leftColumn">
		<?php
			$mstr_loadnext = $basepath . '\navigation.php';
//			print "LOADNEXT ["  . $mstr_loadnext;		//DBG PRSC
			require ($mstr_loadnext); ?>
    </div>
	<div class="rightColumn">
	<?php } 
	?>
	
	