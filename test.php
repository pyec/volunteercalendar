<?php

//error_reporting(E_ALL);
//ini_set('display_errors', '1');

if (connectDB()) drawMonth(8,2007);

function connectDB() {
	global $db;
	require_once("./adodb5/adodb.inc.php");

	$dbhost = "localhost";
	$dbuname = "HRPCalendar";
	$dbpass = "DaB0mb";
	$dbname = "HRPCalendar";

	$db = ADONewConnection('odbc_mssql');
	$db->SetFetchMode(ADODB_FETCH_ASSOC);
	//$db->debug = true;
	$dsn = "Driver={SQL Server};Server=localhost;Database=$dbname;";
	$result = $db->Connect($dsn, $dbuname, $dbpass);

	return $result;
}



function drawMonth($month, $year, $day=1) {
	$date = mktime(12, 0, 0, $month, 1, $year);
	$daysInMonth = date("t", $date);

	//retrieve all the month's events from the database
	global $db;

	$sqlQuery = "SELECT * FROM hrpcal_events "
		."WHERE event_date BETWEEN '$month/1/$year' AND '$month/$daysInMonth/$year' "
		."ORDER BY event_date ASC";

	$rs = $db->Execute($sqlQuery);
	$arrEvents = $rs->GetRows();

	//echo "<pre>";
	//print_r($arrEvents);
	//echo "</pre>";


	//*********************************
	// Draw the Month
	//*********************************

	// calculate the position of the first day in the calendar (sunday = 1st column, etc)
	$offset = date("w", $date);
	$rows = 1;

	//start the table
	echo "<div id=\"pageTitle\">Month View: ". date("F", $month) . " $year</div>"
			. "<table id=\"monthTable\" border=\"1\" cellpadding=\"2\" cellspacing=\"0\">\n<tr class=\"monthHeader\">";

	//print the row with the days of the week
	echo "<tr>\n";
	$daysOfWeek = array("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat");
	foreach ($daysOfWeek as $dayIndex => $dayName) {
		echo "<td>$dayName</td>\n";
	}
	echo "</tr>\n";

	//fill in the spacers for the beginning of the month
	echo "<tr>\n";
	for ($i = 1; $i <= $offset; $i++) echo "<td class=\"daySpacer\">&nbsp;</td>";

	for ($day = 1; $day <= $daysInMonth; $day++) {
		//if it's sunday - end the row and start a new one
		if (($day + $offset - 1) % 7 == 0 && $day != 1) {
			echo "</tr>\n\t<tr>";
			$rows++;
		}

		//if the day is today's date, style appropriately
		if (strtotime("$year-$month-$day") == strtotime(date("Y-m-d"))) {
			echo "<td class=\"dayToday\"><div class=\"dayLabel\"><a href=\"day.php?m=$month&y=$year&d=$day\">$day</a></div>";
		} else {
			//check to see if day is a weekend, style appropriately
			if (date("N", strtotime("$year-$month-$day")) >= 6) {
				echo "<td class=\"dayWeekend\"><div class=\"dayLabel\"><a href=\"day.php?m=$month&y=$year&d=$day\">$day</a></div>";
			} else {
				echo "<td class=\"dayNormal\"><div class=\"dayLabel\"><a href=\"day.php?m=$month&y=$year&d=$day\">$day</a></div>";
			}
		}

		//loop through the recordset and print the items
		foreach ($arrEvents as &$rsRow) {
			if (strtotime(substr($rsRow['event_date'],0,10)) == strtotime("$year-$month-$day")) {
				//an item exists for this day - print it
				printEvent("MONTH", $rsRow);
			};
		}

		echo "</td>";
	}

	//fill in the spacers for the end of the month
	while (($day + $offset) <= $rows * 7) {
		echo "<td class=\"daySpacer\">&nbsp;</td>";
		$day++;
	}

	echo "</tr>\n";
	echo "</table>\n";
}



function printEvent($type, $rsRow) {
	//echo "<div class=\"monthEvent\" style=\"background-color: #". $row['cat_hexcolor'] ."; border: 1px solid #" .hexDarker($row['cat_hexcolor']) ."\">"
	//	."<a href=# onclick=\"calPopup('viewEntry.php?id=" .$row['event_id']. "')\">$iconAttach" .getSimpleTime($row['event_timestart']) ." ". $row['event_name'] ."</a>"
	//	."</div>";
	echo "<div class=\"monthEvent\">". $rsRow['event_timestart'] ."-". $rsRow['event_timeend'] ."<br />". $rsRow['event_name'] ."</div>";
}

?>