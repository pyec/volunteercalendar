
<?php
/*---------------------------------------------------------------
 *
 * 	MODULE:		retrieveVolunteersBySectionAJAX.php
 * 	AUTHOR:		Clay Pye
 * 	Created:	2019-07-12
 *
 * --------------------------------------------------------------
 *
 *
 * 	MODIFICATION HISTORY
 *
 *---------------------------------------------------------------
 */
$basepath = "C:\inetpub\wwwroot\VolunteerCalendar";

require ($basepath . '\includes\functions.php');

$leadId = $_REQUEST["leadId"];

global $db;
connectDB();

if(isset($leadId)) {
    $sqlQuery = "SELECT * "
        . "FROM AppLeads "
        . "WHERE LeadId = ". $leadId;

    $rs = $db->Execute($sqlQuery);



    if ($rs->RowCount()) {
        while ($row = $rs->FetchRow()) {
            $output .= $row['UserId'] . ",";
        }

        $output = rtrim($output,",");
    }

    $db->Close();

    echo $output;

}


