
function popDetails(shiftID, mode, shiftDate) {
	switch(mode) {
	case "EDIT":
		theURL = "shiftDetail.php?action=EDIT&shiftid=" + shiftID;
		break;
	case "ADDNEW":
		theURL = "shiftDetail.php?action=ADDNEW";
		break;
	default:
		theURL = "shiftDetail.php?shiftid=" + shiftID + "&shiftDate=" + shiftDate;
	}

	newWindow = window.open(theURL, "shiftDetail", "status=1, height=450, width=600, resizable=1, scrollbars=yes");
	newWindow.focus();
}

function deleteShift(shiftID) {
	var answer = confirm("Are you sure you want to delete this shift?\nDeletions CANNOT be undone.");

	if (answer)	window.location = "shiftDetail.php?action=DELETE&shiftid=" + shiftID;
}

function deleteNews(newsID, sectionID) {
	var answer = confirm("Are you sure you want to delete this news post?\nDeletions CANNOT be undone.");

	if (answer)	window.location = "index.php?action=DELETE&newsid=" + newsID + "&sectionid=" + sectionID;
}

String.prototype.fulltrim = function() {

return this.replace(/(?:(?:^|\n)\s+|\s+(?:$|\n))/g,"").replace(/\s+/g," ");

};

function validateTheForm(theForm) {

	var output			= "";
	var highlightColor	= "#fee8e5";
	var count 		= 0;

	// reset form elements to white
	for (i=0; i<theForm.elements.length; i++) {
		if (theForm.elements[i].type=="text" || theForm.elements[i].type=="textarea" || theForm.elements[i].type=="select-one") {
			theForm.elements[i].style.background='#fff';
			// trim form elements removing unnecessary white space - unless the comment or special admin comment
			if (theForm.elements[i].name !== "fComments" && theForm.elements[i].name !== "fSuperAdminComments") {
				theForm.elements[i].value = theForm.elements[i].value.fulltrim();
			}
		}
	}

	// begin validating form elements
	if (theForm.fFirstName.value == "") {
		output += "- First Name\n";
		theForm.fFirstName.style.background=highlightColor;
	}
	if (theForm.fLastName.value == "") {
		output += "- Last Name\n";
		theForm.fLastName.style.background=highlightColor;
	}
	
	if (theForm.fUsername.value == "") {
		output += "- Username\n";
		theForm.fUsername.style.background=highlightColor;
	}
	
	if (theForm.fPassword.value == "") {
		output += "- Password\n";
		theForm.fPassword.style.background=highlightColor;
	}

	if (theForm.fPrimaryPhone.value == "") {
		output += "- Primary Phone\n";
		theForm.fPrimaryPhone.style.background=highlightColor;
	}

	if (theForm.fPrimaryEmail.value == "") {
		output += "- Primary Email\n";
		theForm.fPrimaryEmail.style.background=highlightColor;
	}
	else if (theForm.fPrimaryEmail.value.indexOf("@") < 0) {
		output += "- Primary Email (invalid format)\n";
		theForm.fPrimaryEmail.style.background=highlightColor;
	}
	
	if (theForm.fIsAdmin.value == 1) {

		for (i=0; i<theForm.elements.length; i++) {
			if (theForm.elements[i].type=="checkbox") {
				if(theForm.elements[i].checked) {
					count += 1;	
				} else {
						theForm.elements[i].style.backgroundColor=highlightColor;
				}
			}
		}
	
		if(count <= 0) {
			output += "- Please select at least one Section\n";
		}
	
	}

	// Format output (pass or fail validation)
	if (output != "") {
		output = "\nOops! The fields below are required!\n\nWe will attempt to fill in missing information automatically.\nIf you do not know the required field's information type \"n/a\" to continue.\n\n" + output;
		alert(output);
		return false;
	} 
	
	return true;	
	

}

function validateVolunteerForm(theForm) {

	let output			= "";
	let highlightColor	= "#fee8e5";
	let slSection = document.getElementById('fCategory');
	let selectedSelection = slSection.options[slSection.selectedIndex].value;


	// reset form elements to white
	for (i=0; i<theForm.elements.length; i++) {
		if (theForm.elements[i].type=="text" || theForm.elements[i].type=="textarea" || theForm.elements[i].type=="select-one") {
			theForm.elements[i].style.background='#fff';
		}
	}

	// begin validating form elements
	if (theForm.fShiftDate.value === "") {
		output += "- Start Date\n";
		theForm.fShiftDate.style.background=highlightColor;
	}
	if (selectedSelection === "0") {
		output += "- Section\n";
		theForm.fCategory.style.background=highlightColor;
	}

    // Format output (pass or fail validation)
	if (output !== "") {
		output = "\nThe fields below are required.\n\n" + output;
		alert(output);
		return false;
	}

	return true;


}

// Same as new user validation form but without password validation
function validateUpdateForm(theForm) {

	var output			= "";
	var highlightColor	= "#fee8e5";
	var count 		= 0;

	// reset form elements to white
	for (i=0; i<theForm.elements.length; i++) {
		if (theForm.elements[i].type=="text" || theForm.elements[i].type=="textarea" || theForm.elements[i].type=="select-one") {
			theForm.elements[i].style.background='#fff';
			// trim form elements removing unnecessary white space - unless the comment or special admin comment
			if (theForm.elements[i].name !== "fComments" && theForm.elements[i].name !== "fSuperAdminComments") {
				theForm.elements[i].value = theForm.elements[i].value.fulltrim();
			}
		}
	}

	// begin validating form elements
	if (theForm.fFirstName.value == "") {
		output += "- First Name\n";
		theForm.fFirstName.style.background=highlightColor;
	}
	if (theForm.fLastName.value == "") {
		output += "- Last Name\n";
		theForm.fLastName.style.background=highlightColor;
	}
	
	if (theForm.fUsername.value == "") {
		output += "- Username\n";
		theForm.fUsername.style.background=highlightColor;
	}
	
	//if (theForm.fPassword.value == "") {
	//	output += "- Password\n";
	//	theForm.fPassword.style.background=highlightColor;
	//}

	if (theForm.fPrimaryPhone.value == "") {
		output += "- Primary Phone\n";
		theForm.fPrimaryPhone.style.background=highlightColor;
	}

	if (theForm.fPrimaryEmail.value == "") {
		output += "- Primary Email\n";
		theForm.fPrimaryEmail.style.background=highlightColor;
	}
	else if (theForm.fPrimaryEmail.value.indexOf("@") < 0) {
		output += "- Primary Email (invalid format)\n";
		theForm.fPrimaryEmail.style.background=highlightColor;
	}
	
	if (theForm.fIsAdmin.value == 1) {
	
		for (i=0; i<theForm.elements.length; i++) {
			if (theForm.elements[i].type=="checkbox") {
				if(theForm.elements[i].checked) {
					count += 1;	
				} else {
						theForm.elements[i].style.backgroundColor=highlightColor;
				}
			}
		}
	
		if(count <= 0) {
			output += "- Please select at least one Section\n";
		}
	
	}

	// Format output (pass or fail validation)
	if (output != "") {
		output = "\nOops! The fields below are required!\n\nWe will attempt to fill in missing information automatically.\nIf you do not know the required field's information type \"n/a\" to continue.\n\n" + output;
		alert(output);
		return false;
	} 
	
	return true;	
	

}


function validateNewPostForm(theForm) {

	var output			= "";
	var highlightColor	= "#fee8e5";
	var count 		= 0;

	// reset form elements to white
	for (i=0; i<theForm.elements.length; i++) {
		if (theForm.elements[i].type=="text" || theForm.elements[i].type=="textarea" || theForm.elements[i].type=="select-one") {
			theForm.elements[i].style.background='#fff';
			// trim form elements removing unnecesarry white space
			theForm.elements[i].value = theForm.elements[i].value.fulltrim();
		}
	}

	// begin validating form elements
	if (theForm.fContent.value == "") {
		output += "- Content\n";
		theForm.fContent.style.background=highlightColor;
	}
	if (theForm.fPostedOn.value == "") {
		output += "- Post On\n";
		theForm.fPostedOn.style.background=highlightColor;
	}

	for (i=0; i<theForm.elements.length; i++) {
		if (theForm.elements[i].type=="checkbox") {
			if(theForm.elements[i].checked) {
				count += 1;
			} else {
				theForm.elements[i].style.backgroundColor=highlightColor;
			}
		}
	}

	if(count <= 0) {
		output += "- Please select at least one Post In\n";
	}

	// Format output (pass or fail validation)
	if (output != "") {
		output = "\nThe fields below are required.\n\n" + output;
		alert(output);
		return false;
	}

	return true;


}

function RandomLoginGenerator(x) {
	
	var a;
	var b;
	var output;
	
	a = document.AdminUserDetails.fLastName.value;
	if (a.length > 6) a = a.substring(0,6);
	
	b = document.AdminUserDetails.fFirstName.value;
	b = b.substring(0,1);

	output = a + b
	x.value = output.toLowerCase();
	
	return true;
	
}


function RandomPasswordGenerator(x) {
	
	var theNumber;
	theNumber = Math.floor(Math.random() * 1000000);
	
	x.value = theNumber;
	
	return true;
	
}

/*
function highlightNewUser() {
	newUser = document.getElementById("1159");
	newUser.style.backgroundColor = "#fff8d0";
}
*/



function postedByOnChange() {
	var xmlhttp = new XMLHttpRequest();

	xmlhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			var sectionTable = document.getElementById("SectionList");
			sectionTable.innerHTML = this.responseText;
		}
	};
	//+theForm.fPostedBy.value
    var postForm = document.getElementById("AdminUserDetails");
    var newsId = document.getElementById("hdnNewsId").value;
	var e = document.getElementById("AdminUserDetails").fPostedBy;
	var strUserId = e.options[e.selectedIndex].value;

	xmlhttp.open("GET","retrievePostBySections.php?userId="+strUserId+"&newsId="+newsId);
	xmlhttp.send();
}

function volunteerOnChange() {
	var xmlhttp = new XMLHttpRequest();

	xmlhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			let categorySelect = document.getElementById("fCategory");
			categorySelect.innerHTML = this.responseText;
		}
	};

	let type = 'SELECT';
	let currentCat = document.getElementById('hdnShiftCat').value;
	let volunteerSelect = document.getElementById('fVolunteer');
	let currentUserID = volunteerSelect.options[volunteerSelect.selectedIndex].value;
	let calselect = false;

	xmlhttp.open("GET","retrieveSectionsByVolunteerAJAX.php?type="+type+"&currentCat="+currentCat+"&currentUserID="+currentUserID+"&calselect="+calselect);
	xmlhttp.send();
}

function slSectionOnChange() {
	var xmlhttp = new XMLHttpRequest();

	xmlhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			var leadSelect = document.getElementById("slLeadList");
			leadSelect.innerHTML = this.responseText;
		}
	};

	//reset the option controls
	let volunteerList = document.getElementById("opVolunteersList");
	let leadList = document.getElementById("opLeadList");
	volunteerList.options.length = 0;
	leadList.options.length = 0;

	var e = document.getElementById("slSection");
	var sectionId = e.options[e.selectedIndex].value;

	xmlhttp.open("GET","retrieveVolunteersBySectionAJAX.php?sectionId="+sectionId);
	xmlhttp.send();
}

function slLeadListOnChange() {

	let leadList = document.getElementById("slLeadList");
	let leadId = leadList.options[leadList.selectedIndex].value;

	let volunteerList = document.getElementById("opVolunteersList");
	let volunteerLeadList = document.getElementById('opLeadList');
	let xmlhttp = new XMLHttpRequest();

	xmlhttp.onreadystatechange = function() {
		if (this.readyState === 4 && this.status === 200) {

			let volunteersUnderLead = this.responseText;

			// clear out the lists
			volunteerList.options.length = 0;
			volunteerLeadList.options.length = 0;

			for (let i = 0; i < leadList.length; i++){
				if (leadList.options[i].value !== '0' &&  leadList.options[i].value !== leadId &&  volunteersUnderLead.indexOf(leadList.options[i].value === -1)) {
					let opt = document.createElement('option');

					opt.value = leadList.options[i].value;
					opt.text = leadList.options[i].text;
					volunteerList.appendChild(opt);
				}
			}

			//add the volunteers under lead to the lead list option control
			let volunteerArr = volunteersUnderLead.split(',');

			for (let i = 0; i < volunteerArr.length; i++) {
				if (volunteerArr[i].trim() !== '') {

					let optVolunteer = document.createElement('option');
					optVolunteer.value = volunteerArr[i].trim();

					//find the volunteer name
					let volunteerName = '';
					for (let x = 0; x < leadList.options.length; x++) {
						if (leadList.options[x].value === optVolunteer.value) {
							volunteerName = leadList.options[x].text;
							break;
						}
					}

					optVolunteer.text = volunteerName;

					volunteerLeadList.appendChild(optVolunteer);
				}
			}
		}
	};

	xmlhttp.open("GET","retrieveVolunteersByLeadAJAX.php?leadId="+leadId);
	xmlhttp.send();

}

function leadLeftTransferClicked() {
	var list = $('#opLeadList');

	var selectedItem = list[0].selectedIndex;

	if (selectedItem === -1) {
		alert("No Volunteer Selected");
	} else{
		$('#opLeadList > option:selected').appendTo('#opVolunteersList');
	}
}

function leadRightTransferClicked() {
	var list = $('#opVolunteersList');

	var selectedItem = list[0].selectedIndex;

	if (selectedItem === -1) {
		alert("No Volunteer Selected");
	} else{
		$('#opVolunteersList > option:selected').appendTo('#opLeadList');
	}
}

function btnSaveOnClick() {
	let xmlhttp = new XMLHttpRequest();

	// only save if a lead is selected
	let leadSelect = $('#slLeadList');
	$('#lblInfo').text('');

	if (leadSelect[0].selectedIndex <= 0) {
		//alert("No Lead Selected");
		$('#lblInfo').text('No Lead Selected');
	}
	else {
		let sectionSelect = $('#slSection');
		let sectionId = sectionSelect[0].options[sectionSelect[0].selectedIndex].value;

		let selectedLead = leadSelect[0].options[leadSelect[0].selectedIndex].value;

		//get the volunteers under lead list
		let leadVolunteersList = [];

		$('#opLeadList option').each(function (name, val) {
			leadVolunteersList.push(val.value);
		});

		xmlhttp.onreadystatechange = function () {
			if (this.readyState == 4 && this.status == 200) {
				$('#lblInfo').text(this.responseText);
			}
		};
		let delimitedList = "";

		if (leadVolunteersList.length > 0) {
			delimitedList = leadVolunteersList.join(',');
		}

		xmlhttp.open("POST","saveLeadVolunteers.php?volunteerList="+delimitedList+"&sectionId="+sectionId+"&leadId="+selectedLead);
		xmlhttp.send();
	}
}


function fRepeatsChanged() {
	var repeatsChecked = document.getElementById("fShiftRepeats").checked;
	var trRepeatInterval = document.getElementById("trRepeatInterval");
    var trRepeatMonthly = document.getElementById("trRepeatMonthly");
	var trEndDate = document.getElementById("trEndDate");
	var repeatEvery = document.getElementById("fRepeatEvery");
	var trRepeatWeekly = document.getElementById("trRepeatWeekly");
	var repeatOn = document.getElementById("fRepeatOn");
	var fshiftDate = document.getElementById("fShiftDate").value;

	//determine the day of the week
	var shiftDate = new Date(fshiftDate);
	var dayOfWeek = getDayOfWeekInteger(shiftDate);

	if (repeatsChecked === true) {
		trRepeatInterval.style.visibility = 'visible';
		//trRepeatWeekly.style.visibility = 'visible';
		trEndDate.style.visibility = 'visible';
		repeatEvery.value = "Week";
        repeatOn.value = dayOfWeek;
	}else{
		trRepeatInterval.style.visibility = 'collapse';
		trRepeatWeekly.style.visibility = 'collapse';
		trEndDate.style.visibility = 'collapse';
        trRepeatMonthly.style.visibility = 'collapse';
	}
}

function fRepeatEveryChanged() {
	var repeatEvery = document.getElementById("fRepeatEvery");
	var trRepeatWeekly = document.getElementById("trRepeatWeekly");
	var trEndDate = document.getElementById("trEndDate");
	var repeatOn = document.getElementById("fRepeatOn");
	var fshiftDate = document.getElementById("fShiftDate").value;
	var trRepeatMonthly = document.getElementById("trRepeatMonthly");
	var repeatMonthly= document.getElementById("fRepeatMonthly");

	//determine the day of the week
	var shiftDate = new Date(fshiftDate);
	var dayOfWeek = getDayOfWeekInteger(shiftDate);
	var dayOfWeekString = getDayOfWeek(shiftDate);
	var dayOfMonth = shiftDate.getDate();
	var weekOfMonth = getWeekOfMonth(shiftDate);

	switch(repeatEvery.value) {
		case "Week":
			//trRepeatWeekly.style.visibility = 'visible';
			repeatOn.value = dayOfWeek;
			trRepeatMonthly.style.visibility = 'collapse';
			break;
		case "Day":
			trRepeatWeekly.style.visibility = 'collapse';
			trRepeatMonthly.style.visibility = 'collapse';
			break;
		case "Month":
			trRepeatWeekly.style.visibility = 'collapse';
			trRepeatMonthly.style.visibility = 'visible';

			//remove all options
			repeatMonthly.options.length = 0;

			//option 1
			var opt1 = document.createElement('option');
			opt1.value = "1";
			opt1.text = "Monthly on Day " + dayOfMonth;
			repeatMonthly.add(opt1);

			//option 2
			var opt2 = document.createElement('option');
			opt2.text = "Monthly on the " + weekOfMonth + " " + dayOfWeekString;
			opt2.value = "2";
			repeatMonthly.add(opt2);
			break;
		case "Year":
			trRepeatWeekly.style.visibility = 'collapse';
			trRepeatMonthly.style.visibility = 'collapse';
			break;
		default:
	}
}

function getWeekOfMonth(date) {
	prefixes = ['First', 'Second', 'Third', 'Fourth', 'Fifth'];

	return prefixes[Math.floor(date.getDate() / 7)];
}

function getDayOfWeek(date) {

	var days = ['Sunday','Monday','Tuesday','Wednesday',
			'Thursday','Friday','Saturday'];
	return days[date.getDay()];
}

function getDayOfWeekInteger(date) {

    return date.getDay();
}

$(document).ready(function(){
    var fRepeats = $('#hdnShiftRepeats').val();

    if (fRepeats === '1') {
        var repeatInterval = $('#hdnRepeatInterval').val();
        var shiftRepeats = $('#hdnShiftRepeats').val();
        var repeatWeek = $('#hdnRepeatWeek').val();
        var repeatWeekday = $('#hdnRepeatWeekday').val();
        var repeatMonth = $('#hdnRepeatMonth').val();
        var repeatYear = $('#hdnRepeatYear').val();
        var repeatDay = $('#hdnRepeatDay').val();

        $('#trEndDate').css('visibility','visible');

        if (repeatInterval === '0' && repeatYear === '*' && shiftRepeats) {
            $('#fShiftRepeats').prop('checked', true);
            var fshiftDate = document.getElementById("fShiftDate").value;
            var shiftDate = new Date(fshiftDate);
            var dayOfWeekString = getDayOfWeek(shiftDate);
            var dayOfMonth = shiftDate.getDate();
            var weekOfMonth = getWeekOfMonth(shiftDate);

            $('#trRepeatInterval').css('visibility','visible');

            // monthly
            if (repeatMonth === '*') {

                //$('#trRepeatWeekly').css('visibility','visible');
                $('#trRepeatMonthly').css('visibility', 'visible');
                $('#trEndDate').css('visibility','visible');
                $('#fRepeatEvery').val('Month');

                var repeatMonthly = $('#fRepeatMonthly');

                repeatMonthly.children('option').remove();
                //remove all options
                //repeatMonthly.options.length = 0;

                //option 1
                var opt1 = document.createElement('option');
                opt1.value = "1";
                opt1.text = "Monthly on Day " + dayOfMonth;
                repeatMonthly.append(opt1);

                //option 2
                var opt2 = document.createElement('option');
                opt2.text = "Monthly on the " + weekOfMonth + " " + dayOfWeekString;
                opt2.value = "2";
                repeatMonthly.append(opt2);

                if (repeatWeekday !== '*' && repeatWeek !== '*') {
                    repeatMonthly.val(2);
                } else{
                    repeatMonthly.val(1);
                }
                //$('#fRepeatEvery').val('Week');
                //$('#fRepeatOn').val(repeatWeekday);
            }
            // yearly
            else if (repeatYear === '*' && repeatMonth !== '*') {
                $('#fRepeatEvery').val('Year');
            }
        }
        else if (repeatInterval !== '0' && (repeatYear === '' || repeatYear === 'NULL') && shiftRepeats) {
            $('#trRepeatInterval').css('visibility','visible');
            $('#fShiftRepeats').prop('checked', true);

            if (repeatInterval === '7') {
                $('#fRepeatEvery').val('Week');
            }
            else {
                $('#fRepeatEvery').val('Day');
            }
        }
    }
});

// document.onload = function loadShift() {
// 	var fDoesNotRepeat = document.getElementById("fDoesNotRepeat");
//
// 	if(fDoesNotRepeat != null) {
// 		alert ("on edit shift");
// 	}
// };