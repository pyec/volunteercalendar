<?php require("header.php"); ?>
        
        <table width="100%">
        <tr>
        <td>
		<?php 
		printUserControls($currentUserID);
		printAdvancedFilter($currentUserID, $_REQUEST['filter'], $_REQUEST['selected_section'], $_REQUEST['selected_user'], $_REQUEST['start_date'], $_REQUEST['end_date']);
		?>
        </td>
        </tr>
        </table>
        
		<?php
		if($_REQUEST['type'] == "category")
		{
			printShiftsBySection($currentUserID, $_REQUEST['shiftid'], $_REQUEST['type']);
		}
		elseif($_REQUEST['type'] == "person")
		{
			printShiftsByUser($currentUserID, $_REQUEST['userid'], 0);
		}
		elseif($_REQUEST['type'] == "me")
		{
			printShiftsBySection($currentUserID, $_REQUEST['shiftid'], $_REQUEST['type']);
		}
		elseif($_REQUEST['type'] == "date")
		{
			printShiftByDateFilter($currentUserID, $_REQUEST['selected_section'], $_REQUEST['selected_user'], $_REQUEST['start_date'], $_REQUEST['end_date']);
		}
		elseif(isset($_REQUEST['filter']))
		{
			printShiftByNameFilter($currentUserID, $_REQUEST['filter']);
		}
		else
		{
			printShiftsByUser($currentUserID, $currentUserID, 0);
		}
		?>

<?php require("footer.php"); ?>