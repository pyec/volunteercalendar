<?php
/*---------------------------------------------------------------
 *
 * 	MODULE:		Milestones.php
 * 	AUTHOR:		Clay Pye
 * 	Created:	2019-06-20
 *
 * --------------------------------------------------------------
 *
 *
 * 	MODIFICATION HISTORY
 *
 *    *
 *---------------------------------------------------------------
 */


$basepath = $_SERVER['DOCUMENT_ROOT']."/VolunteerCalendar";

require($basepath . "/header.php");

echo "<div style=\"padding-top:70px;\">";

if(userIsSuperAdmin($currentUserID)) {
    global $db;
    connectDB();

    $currentMonth = date("m");
    $loopMonth = 0;

    $sqlQuery = "SELECT FirstName, LastName, Convert(Date, VolunteerSince, 101) AS VolunteerSince, YearsOfService "
    . "FROM vAppUser WHERE YearsOfService IN (5,10,15,20,25,30,35,40,45,50) ORDER BY YearsOfService";
    $rs = $db->Execute($sqlQuery);

    //If Rows are Returned
    if ($rs->RowCount()) {

        echo "<table width=\"460px\">\n"
            . "<tr>\n"
            . "<td><h1>Volunteer Milestones</h1></td>\n"
            . "</tr>\n"
            . "</table>\n"
            . "<table cellspacing=\"0\" border=\"0\" width=\"460px\" id=\"milestonesTable\">\n"
            . "<thead>\n"
            . "<tr>\n"
            . "<th width=\"220px\">Volunteer</th>\n"
            . "<th width=\"120px\">Volunteer Since</th>\n"
            . "<th width=\"120px\">Years of Service</th>\n"
            . "</tr>\n"
            . "</thead>\n"
            . "<tbody>\n";

        while ($row = $rs->FetchRow()) {
            echo "<tr>";
            echo "<td width=\"220px\"><strong>" . $row['FirstName'] . " " . $row['LastName'] . "</strong></td>";
            echo "<td width=\"120px\"><strong>" . $row['VolunteerSince'] . "</strong></td>";
            echo "<td width=\"120px\"><strong>" . $row['YearsOfService'] . "</strong></td>";
            echo "</tr>";
        }

    }
    else //No Rows Returned
    {
        echo "<p style='margin-top:93px;color:red;'><b>There are no milestones to display. </b><br />Please choose an option in the menu to continue.</p>";
    }

    $db->Close();
}
else {
    echo "<p style='margin-top:93px;color:red;'><b>You do not have permission to view this information. </b><br />Please choose an option in the menu to continue.</p>";

}
echo "</div>";

require($basepath . "/footer.php");

?>